import React from 'react';
import { MantineProvider, Global, ColorScheme } from '@mantine/core';
import { addDecorator } from '@storybook/react';
import { SiskaThemeProvider } from '../packages/react/src/contexts';
import { CSSReset } from '../packages/react/src/components';
import { PDFProvider } from '../packages/pdf';

addDecorator((story) => {
  const colorScheme =
    localStorage.getItem('storybook-siska-ui-theme') || 'light';

  return (
    <SiskaThemeProvider
      roleType="MAHASISWA"
      colorScheme={colorScheme as ColorScheme}
    >
      <PDFProvider fontPath="/fonts" theme={{ fontFamily: 'Times New Roman' }}>
        <CSSReset />
        {story()}
      </PDFProvider>
    </SiskaThemeProvider>
  );
});

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/
    }
  }
};

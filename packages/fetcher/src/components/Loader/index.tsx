import React from 'react';
import { Center, Text } from '@mantine/core';
import { Loading } from '@siska-up-ui/react';
import { AxiosError } from 'axios';
import { getErrorMessage } from '../../utils';
import { ApiResponseError } from '../../types';

export type LoaderProps = {
  isLoading: boolean;
  isRefetching?: boolean;
  spinnerOnRefetch?: boolean;
  isEmpty?: boolean;
  error?: AxiosError<ApiResponseError> | null;
  children: React.ReactNode;
  placeholderHeight?: number;
};

export const Loader: React.FC<LoaderProps> = ({
  isLoading,
  isRefetching,
  isEmpty,
  error,
  children,
  spinnerOnRefetch = true,
  placeholderHeight = 400
}) => {
  return (
    <>
      {isLoading || (isRefetching && spinnerOnRefetch) ? (
        <Center sx={{ height: placeholderHeight }}>
          <Loading />
        </Center>
      ) : error ? (
        <Center sx={{ height: placeholderHeight }}>
          <Text align="center">{getErrorMessage(error)}</Text>
        </Center>
      ) : !isLoading && !isRefetching && isEmpty ? (
        <Center>
          <Text align="center">Belum ada data</Text>
        </Center>
      ) : (
        <>{children}</>
      )}
    </>
  );
};

export default Loader;

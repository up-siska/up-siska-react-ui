import React, { createContext, useContext } from 'react';
import { LoaderProps } from '../../components';

type LoaderCtx = Omit<LoaderProps, 'children'>;

export const LoaderContext = createContext<LoaderCtx>({
  isLoading: false
});

type LoaderProviderProps = {
  children: React.ReactNode;
} & LoaderCtx;

export const LoaderProvider: React.FC<LoaderProviderProps> = ({
  children,
  ...props
}) => {
  return (
    <LoaderContext.Provider value={props}>{children}</LoaderContext.Provider>
  );
};

export const useLoaderContext = () => useContext(LoaderContext);

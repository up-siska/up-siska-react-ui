import { useMutation, useQueryClient } from 'react-query';
import { AxiosResponse, AxiosError } from 'axios';
import { NotificationProps, useNotifications } from '@mantine/notifications';
import { getErrorMessage } from '../../utils';
import {
  ApiResponse,
  ApiResponseError,
  ErrorValidationArray,
  ErrorValidation,
  ErrorObject,
  ActionOptions,
  ErrorValidationLaravel,
  ErrorValidation2
} from '../../types';
import React, { useCallback } from 'react';

const wrapMessage = (message: string) => {
  return (
    <div
      dangerouslySetInnerHTML={{
        __html: message
      }}
    />
  );
};

export const useAction = <T extends any = any, P = any, R = ApiResponse<T>>(
  queryKey: string | string[] | null,
  apiFn: (params: P) => Promise<AxiosResponse<R>>,
  options?: ActionOptions<T, R>
) => {
  const { showNotification } = useNotifications();
  const queryClient = useQueryClient();

  const showErrorNotification = useCallback(
    (props: Omit<NotificationProps, 'message'> & { message: string }) => {
      const { message, ...rest } = props;
      showNotification({
        title: 'Error',
        message: wrapMessage(message),
        color: 'red',
        autoClose: 5000,
        ...rest,
        ...(options?.errorNotificationProps || {})
      });
    },
    [options?.errorNotificationProps, showNotification]
  );

  return useMutation<
    AxiosResponse<R>,
    AxiosError<ApiResponseError, AxiosResponse<R>>,
    P
  >(apiFn, {
    onSuccess: (response) => {
      if (options?.successMessage) {
        showNotification({
          title: 'Berhasil',
          message: options.successMessage,
          color: 'green',
          ...(options?.successNotificationProps || {})
        });
      }

      if (options?.onDone) {
        options.onDone(response.data);
      }
    },
    onError: (error) => {
      if (!options?.hideError) {
        if (options?.errorMessage) {
          const message =
            options.errorMessage instanceof Function
              ? options.errorMessage(error)
              : options.errorMessage;

          showErrorNotification({ message });
          return;
        }

        const errors = error.response?.data.error;
        const commonError = error as Error;
        const laravelError = error.response
          ?.data as unknown as ErrorValidationLaravel;

        if (errors) {
          if (errors instanceof Array) {
            errors.forEach((err, i) => {
              const validationArray = err as ErrorValidationArray;
              const validationObj = err as ErrorValidation;
              const validationObj2 = err as ErrorValidation2;

              if (validationArray.children) {
                const _messages: string[] = [];

                validationArray.children.forEach((child) => {
                  child.children.forEach((c) => {
                    Object.values(c.constraints).forEach((message) => {
                      _messages.push(message);
                    });
                  });
                });

                _messages.map((message, idx) => {
                  return setTimeout(
                    () => showErrorNotification({ message }),
                    i * idx * 200
                  );
                });
              }

              if (validationObj.constraints?.length) {
                return setTimeout(
                  () =>
                    showErrorNotification({
                      message:
                        typeof validationObj === 'string'
                          ? err.toString()
                          : validationObj.constraints.join(', ')
                    }),
                  i * 200
                );
              }

              if (validationObj2.key && validationObj2.msg) {
                return setTimeout(
                  () =>
                    showErrorNotification({
                      title: `Error: ${validationObj2.key}`,
                      message: validationObj2.msg
                    }),
                  i * 200
                );
              }

              if (typeof err === 'string') {
                return setTimeout(
                  () => showErrorNotification({ message: err }),
                  i * 200
                );
              }
            });
          } else if (errors instanceof Object && !errors.message) {
            const messages = getErrorMessage(error);
            if (messages instanceof Array) {
              messages.map((err, i) => {
                return setTimeout(
                  () =>
                    showErrorNotification({
                      message: typeof err === 'string' ? err : err.message,
                      title: typeof err === 'string' ? err : err.data
                    }),
                  i * 200
                );
              });
            }
          } else if (errors instanceof Object) {
            if (errors.message) {
              showErrorNotification({ message: errors.message });
            }
          } else if (laravelError.errors) {
            Object.entries(laravelError.errors).forEach(([key, value], i) => {
              return setTimeout(
                () =>
                  showErrorNotification({
                    title: `Error: ${key}`,
                    message: value.join(',')
                  }),
                i * 200
              );
            });
          } else {
            showErrorNotification({
              message:
                errors?.toString() === '[object Object]'
                  ? 'Terjadi kesalahan'
                  : errors?.toString()
            });
          }
        } else {
          showErrorNotification({ message: commonError.message });
        }
      }
      options?.onError?.(error);
    },
    onSettled: () => {
      if (queryKey) {
        if (queryKey instanceof Array) {
          queryKey.forEach((key) => {
            queryClient.refetchQueries(key);
          });
        } else {
          queryClient.refetchQueries(queryKey);
        }
      }
    }
  });
};

export default useAction;

import { AxiosResponse } from 'axios';
import { useCallback } from 'react';
import { useDidUpdate } from '@mantine/hooks';
import {
  useQuery,
  QueryKey,
  QueryFunctionContext,
  useQueryClient
} from 'react-query';
import { ApiResponse, UseFetchOptions } from '../../types';

export const useFetch = <T, D extends object = {}, R = ApiResponse<T, D>>(
  queryKey: QueryKey,
  apiFn: (ctx?: QueryFunctionContext<QueryKey>) => Promise<AxiosResponse<R>>,
  options?: UseFetchOptions<T, D, R>
) => {
  const queryClient = useQueryClient();
  const cancel = useCallback(() => {
    queryClient.cancelQueries([queryKey]);
  }, [queryClient, queryKey]);

  useDidUpdate(() => {
    if (options?.cancellation) {
      queryClient.cancelQueries([queryKey]);
    }
  }, [...(options?.cancellation || []), queryClient, queryKey]);

  const query = useQuery(queryKey, apiFn, options);

  return {
    ...query,
    cancel
  } as typeof query & { cancel: () => void };
};

export default useFetch;

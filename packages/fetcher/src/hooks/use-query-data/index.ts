import { useQueryClient, QueryKey } from 'react-query';
import { QueryFilters } from 'react-query/types/core/utils';
import { AxiosError, AxiosResponse } from 'axios';
import { ApiResponse, ApiResponseError } from '../../types';

export const useQueryData = <
  T = any,
  D extends object = {},
  R = AxiosResponse<ApiResponse<T, D>>,
  S = R
>(
  key: QueryKey,
  filters?: QueryFilters
) => {
  const queryClient = useQueryClient();
  const queryData = queryClient.getQueryData<R>(key, filters);
  const queryState = queryClient.getQueryState<S, AxiosError<ApiResponseError>>(
    key,
    filters
  );

  return {
    queryData,
    queryState,
    isLoading: queryState?.status === 'loading',
    isError: queryState?.status === 'error'
  };
};

export default useQueryData;

export type ApiResponse<T extends any, D extends object = {}> = {
  ok: boolean;
  message: string;
  code: number;
  data: T;
} & D;

export type ErrorImport = {
  data: string[];
  message: string;
};

export type ErrorValidationArrayChild = {
  target: any;
  value: any;
  property: string;
  children: Array<
    ErrorValidationArrayChild & { constraints: Record<string, string> }
  >;
};

export type ErrorValidationArray<T = string> = {
  property: T;
  children: ErrorValidationArrayChild[];
};

export type ErrorValidation<T = string> = {
  constraints: string[];
  property: T;
};

export type ErrorValidation2 = {
  key: string;
  msg: string;
};

export type ErrorValidationLaravel<T extends string = string> = {
  message: string;
  errors: Record<T, string[]>;
};

export type ErrorObject = {
  data?: string;
  message: string;
};

export interface ApiResponseError<V = string> {
  ok: boolean;
  path: string;
  statusCode: number;
  error:
    | string
    | ErrorImport
    | ErrorValidation<V>[]
    | ErrorValidationArray<V>[]
    | ErrorValidation2[]
    | string[]
    | ErrorObject;
}

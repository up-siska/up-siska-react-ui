import { NotificationProps } from '@mantine/notifications';
import { AxiosError, AxiosResponse } from 'axios';
import { QueryKey, UseQueryOptions } from 'react-query';
import { ApiResponse, ApiResponseError, ErrorValidationLaravel } from './api';

export interface IModifiedDataInfo<I = number> {
  readonly id: I;
  readonly created_at: string | null;
  readonly created_by?: string | null;
  readonly updated_at?: string | null;
  readonly updated_by?: string | null;
  readonly deleted_at?: string | null;
  readonly deleted_by?: string | null;
}

export interface IPostData<T> {
  formValues: T;
}

export interface IUpdateData<T, I = number> {
  formValues: T;
  id: I;
}

export interface IDeleteData<I = number> {
  id: I;
}

export interface IGetDatas {
  urlParams?: string;
}

export interface IGetData<I = number> {
  id: I;
}

export type OnDoneAction<T, R = ApiResponse<T>> = (response?: R) => void;
export type OnErrorAction<V = string> = (
  response?: AxiosError<ApiResponseError<V>>
) => void;

export interface ActionOptions<T, R = ApiResponse<T>, V = string> {
  onDone?: OnDoneAction<T, R>;
  successMessage?: string | null;
  errorMessage?:
    | (<E = any>(
        res: AxiosError<ApiResponseError<E> | ErrorValidationLaravel> | Error
      ) => string)
    | string;
  hideError?: boolean;
  onError?: OnErrorAction<V>;
  successNotificationProps?: NotificationProps;
  errorNotificationProps?: NotificationProps;
}

export type UseFetchOptions<
  T = any,
  D extends object = {},
  R = ApiResponse<T, D>
> = {
  cancellation?: any[];
} & UseQueryOptions<
  Promise<AxiosResponse<R>>,
  AxiosError<ApiResponseError>,
  AxiosResponse<R>,
  QueryKey
>;

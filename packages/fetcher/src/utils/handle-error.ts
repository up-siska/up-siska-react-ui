import { AxiosError } from 'axios';
import {
  ApiResponseError,
  ErrorImport,
  ErrorObject,
  ErrorValidation,
  ErrorValidation2,
  ErrorValidationArray,
  ErrorValidationLaravel
} from '../types';

type Output = 'string' | 'default';

export const getErrorMessage = <V = string>(
  error?: AxiosError<ApiResponseError<V> | null> | Error,
  outputType?: Output
) => {
  const axiosError = error as AxiosError<ApiResponseError<V> | null>;
  const errorResponse = axiosError.response?.data?.error;
  const commonError = error as Error;

  const errorsImport = errorResponse as ErrorImport;
  const errorsValidation = errorResponse as ErrorValidation<V>[];
  const errorsValidation2 = errorResponse as ErrorValidation2[];
  const errorsValidationArray = errorResponse as ErrorValidationArray<V>[];
  const errorObj = errorResponse as ErrorObject;
  const errorsLaravel = errorResponse as ErrorValidationLaravel;

  if (error) {
    if (errorResponse) {
      if (typeof errorResponse === 'string') {
        return errorResponse;
      } else {
        if (
          errorsImport.data &&
          errorsImport.data instanceof Array &&
          typeof errorsImport.data !== 'string'
        ) {
          if (outputType === 'string') {
            return errorsImport.data.map((d) => errorsImport.message);
          } else {
            return errorsImport.data.map((d) => ({
              data: d,
              message: errorsImport.message
            }));
          }
        }

        if (errorObj.data && typeof errorObj.data === 'string') {
          return `${errorObj.data}. ${errorObj.message}`;
        }

        if (errorsValidation.length > 0 && errorsValidation[0].constraints) {
          return errorsValidation.map((err) => err.constraints.join(', '));
        }

        if (
          errorsValidationArray.length > 0 &&
          errorsValidationArray[0].children
        ) {
          const _errors: string[] = [];

          errorsValidationArray.forEach((err) => {
            err.children.forEach((child) => {
              child.children.forEach((c) => {
                Object.values(c.constraints).forEach((message) => {
                  _errors.push(message);
                });
              });
            });
          });

          return _errors;
        }

        if (
          errorsValidation2.length > 0 &&
          errorsValidation2[0]?.key &&
          errorsValidation2[0]?.msg
        ) {
          const _errors: string[] = [];

          errorsValidation2.forEach((err) => {
            _errors.push(err.msg || '');
          });

          return _errors;
        }

        if (errorObj.message && !errorsLaravel.errors) {
          return errorObj.message;
        }

        if (errorsLaravel.errors) {
          const _errors: string[] = [];
          Object.values(errorsLaravel.errors).forEach((err) => {
            err.forEach((msg) => {
              _errors.push(msg);
            });
          });
          return _errors;
        }

        return 'Terjadi Kesalahan';
      }
    } else {
      return commonError.message;
    }
  }
};

import React from 'react';
import ReactPDF, { View } from '@react-pdf/renderer';
import { mergeStyles } from '../../utils';

export type ContentProps = {
  children?: React.ReactNode;
} & ReactPDF.ViewProps;

export const Content: React.FC<ContentProps> = ({
  children,
  style,
  ...props
}) => {
  return (
    <View style={mergeStyles({ flex: 1 }, style)} {...props}>
      {children}
    </View>
  );
};

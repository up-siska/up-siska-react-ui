import React from 'react';
import ReactPDF, { PDFViewer as ReactPDFViewer } from '@react-pdf/renderer';
import { MantineProvider } from '@mantine/core';
import { mergeStyles } from '../../utils';
import { defaultProps, lightTheme, defaultStyles } from '@siska-up-ui/react';

export type PDFViewerProps = {} & ReactPDF.PDFViewerProps;

export const PDFViewer: React.FC<PDFViewerProps> = ({
  style,
  children,
  ...props
}) => {
  return (
    <MantineProvider
      defaultProps={defaultProps}
      theme={lightTheme}
      styles={defaultStyles}
      withNormalizeCSS
      withGlobalStyles
    >
      <ReactPDFViewer
        style={mergeStyles({ width: '100vw', height: '100vh' }, style)}
        {...props}
      >
        {children}
      </ReactPDFViewer>
    </MantineProvider>
  );
};

import React from 'react';
import ReactPDF, { Page as PDFPage } from '@react-pdf/renderer';
import { mergeStyles } from '../../utils';

export type PageProps = {
  children?: React.ReactNode;
} & ReactPDF.PageProps;

export const Page: React.FC<PageProps> = ({
  size = 'A4',
  children,
  style,
  ...props
}) => {
  return (
    <PDFPage size={size} style={mergeStyles({ padding: 16 }, style)} {...props}>
      {children}
    </PDFPage>
  );
};

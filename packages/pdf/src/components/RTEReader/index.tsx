import React from 'react';
import { Html } from 'react-pdf-html';
import { defaultRTEStyles } from '../../constants';

export type RTEReaderProps = {
  children: string;
  resetCSS?: boolean;
};

export const RTEReader: React.FC<RTEReaderProps> = ({
  children,
  resetCSS = true
}) => {
  return (
    <Html
      stylesheet={resetCSS ? defaultRTEStyles : undefined}
    >{`<div>${children}</div>`}</Html>
  );
};

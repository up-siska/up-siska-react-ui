import React from 'react';
import ReactPDF, { View } from '@react-pdf/renderer';
import { Typography, TypographyProps } from '../../components';
import { mergeStyles } from '../../utils';

export type SectionTitleProps = {} & TypographyProps;

const Title: React.FC<SectionTitleProps> = ({ style, children, ...props }) => {
  return (
    <Typography
      style={mergeStyles({ fontWeight: 500, marginBottom: 6 }, style)}
      {...props}
    >
      {children}
    </Typography>
  );
};

export type SectionProps = {
  children?: React.ReactNode;
} & ReactPDF.ViewProps;

export const Section: React.FC<SectionProps> & { Title: typeof Title } = ({
  children,
  style,
  ...props
}) => {
  return (
    <View style={mergeStyles({ marginBottom: 16 }, style)} {...props}>
      {children}
    </View>
  );
};

Section.Title = Title;

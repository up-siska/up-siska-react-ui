import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { PDFViewer, Document, Page } from '@react-pdf/renderer';
import { Html } from 'react-pdf-html';
import { Table } from './index';
import { Typography } from '../';
import { PDFProvider } from '../../contexts';

export default {
  title: 'Example/Table',
  component: Table
} as ComponentMeta<typeof Table>;

const Example = () => {
  return (
    <PDFProvider fontPath="/fonts" theme={{ fontFamily: 'Times New Roman' }}>
      <PDFViewer style={{ width: '100vw', height: '100vh' }}>
        <Document>
          <Page size="A4">
            <Typography style={{ fontWeight: 'bold' }}>Hello</Typography>

            {/* <Html>
              {renderToStaticMarkup(
                <Table>
                  <thead>
                    <Table.TrHead>
                      <Table.Th>Hello</Table.Th>
                    </Table.TrHead>
                  </thead>
                  <tbody>
                    <Table.TrBody>
                      <Table.Td>World</Table.Td>
                    </Table.TrBody>
                  </tbody>
                </Table>
              )}
            </Html> */}
          </Page>
        </Document>
      </PDFViewer>
    </PDFProvider>
  );
};

const Template: ComponentStory<typeof Table> = (args) => {
  return <Example />;
};

const DocumentPDF = Template.bind({});

export { DocumentPDF };

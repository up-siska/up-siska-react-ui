import React from 'react';
import { usePDFTheme } from '../../hooks';

type TableElementProps<C extends keyof JSX.IntrinsicElements> = {
  component: C;
} & JSX.IntrinsicElements[C];

const TableElement = <C extends keyof JSX.IntrinsicElements>({
  component,
  children,
  ...props
}: TableElementProps<C>): JSX.Element => {
  const Element = (component || 'div') as any;

  return <Element {...props}>{children}</Element>;
};

const createTableElement = (
  elementName: keyof JSX.IntrinsicElements,
  defaultStyle: React.CSSProperties
) => {
  const Element: React.FC<JSX.IntrinsicElements[typeof elementName]> = ({
    style,
    children,
    ...props
  }) => {
    const theme = usePDFTheme();

    return (
      <TableElement
        component={elementName}
        style={{
          fontFamily: theme.fontFamily,
          ...defaultStyle,
          ...style
        }}
        {...props}
      >
        {children}
      </TableElement>
    );
  };

  return Element;
};

export const TrHead = createTableElement('tr', {
  borderTop: '1px solid #000',
  borderLeft: '1px solid #000'
});

export const TrBody = createTableElement('tr', {
  borderLeft: '1px solid #000'
});

export const Th = createTableElement('th', {
  borderRight: '1px solid #000',
  borderBottom: '1px solid #000',
  fontSize: 10,
  padding: `4px 6px`
});

export const Td = createTableElement('td', {
  borderRight: '1px solid #000',
  borderBottom: '1px solid #000',
  fontSize: 10,
  padding: `4px 6px`
});

export type TableComponent = {
  TrHead: typeof TrHead;
  TrBody: typeof TrBody;
  Th: typeof Th;
  Td: typeof Td;
} & React.FC<JSX.IntrinsicElements['table']>;

export const Table = createTableElement('table', {}) as TableComponent;

Table.Td = Td;
Table.Th = Th;
Table.TrHead = TrHead;
Table.TrBody = TrBody;

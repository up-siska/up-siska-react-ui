import React from 'react';
import ReactPDF, { Text } from '@react-pdf/renderer';
import { FontWeight, SizeType } from '../../interfaces';
import { usePDFTheme } from '../../hooks';
import { mergeStyles } from '../../utils';

export type TypographyProps = {
  size?: SizeType;
  weight?: FontWeight;
  children?: React.ReactNode;
} & ReactPDF.TextProps;

export const Typography: React.FC<TypographyProps> = ({
  size = 'sm',
  weight = 400,
  style,
  children,
  ...props
}) => {
  const theme = usePDFTheme();

  return (
    <Text
      style={mergeStyles(
        {
          fontSize: theme?.fontSize[size],
          fontWeight: weight,
          fontFamily: theme?.fontFamily
        },
        style
      )}
      {...props}
    >
      {children}
    </Text>
  );
};

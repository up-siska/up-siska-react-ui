import { PDFTheme } from '../interfaces';

export const defaultTheme: PDFTheme = {
  spacing: {
    xs: 8,
    sm: 12,
    md: 16,
    lg: 20,
    xl: 24
  },
  fontSize: {
    xs: 6,
    sm: 10,
    md: 14,
    lg: 18,
    xl: 22
  },
  fontFamily: 'Roboto'
};

export const defaultRTEStyles = {
  '*': {
    margin: 0,
    padding: 0,
    fontSize: 10
  },
  'ol, ul': {
    margin: '6px 0px'
  },
  li: {
    margin: '2px 0px'
  }
};

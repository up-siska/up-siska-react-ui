import React, { createContext, useEffect, useMemo } from 'react';
import merge from 'ts-deepmerge';
import { defaultTheme } from '../../constants';
import { PDFTheme } from '../../interfaces';
import { registerFonts } from '../../utils';

export type ThemeCtx = {
  theme: PDFTheme | null;
  fontPath: string;
};

export const ThemeContext = createContext<ThemeCtx>({
  theme: null,
  fontPath: ''
});

export type PDFProviderProps = {
  children: React.ReactNode;
  fontPath: string;
  theme?: Partial<PDFTheme>;
  registerFont?: boolean;
};

export const PDFProvider: React.FC<PDFProviderProps> = ({
  children,
  theme,
  fontPath,
  registerFont = true
}) => {
  const mergedTheme = useMemo(() => {
    return merge(defaultTheme, theme || {}) as PDFTheme;
  }, [theme]);

  useEffect(() => {
    if (registerFont) {
      registerFonts(fontPath);
    }
  }, [fontPath, registerFont]);

  const _window = window as any;
  _window.defaultPdfTheme = mergedTheme;

  return (
    <ThemeContext.Provider value={{ theme: mergedTheme, fontPath }}>
      {children}
    </ThemeContext.Provider>
  );
};

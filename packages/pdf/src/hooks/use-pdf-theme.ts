import { PDFTheme } from '../interfaces';

export const usePDFTheme = () => {
  const _window = window as any;
  const theme = _window.defaultPdfTheme as PDFTheme;

  return theme;
};

export type SizeType = 'xs' | 'sm' | 'md' | 'lg' | 'xl';

export type Sizes = Record<SizeType, number>;

export type FontWeight = 100 | 300 | 400 | 500 | 700;

export type FontName = 'Roboto' | 'Times New Roman';

export interface PDFTheme {
  spacing: Sizes;
  fontSize: Sizes;
  fontFamily: FontName;
}

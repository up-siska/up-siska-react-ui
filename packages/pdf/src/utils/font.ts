import { Font } from '@react-pdf/renderer';
import { FontName } from '../interfaces';

export const registerFonts = (fontPath: string) => {
  const fontUrl = (fileName: string, fontType: FontName) => {
    return `${fontPath}/${fontType}/${fileName}`;
  };

  Font.register({
    family: 'Roboto',
    fonts: [
      {
        src: fontUrl('Roboto-Bold.ttf', 'Roboto'),
        fontWeight: 700
      },
      {
        src: fontUrl('Roboto-Regular.ttf', 'Roboto'),
        fontWeight: 400
      },
      {
        src: fontUrl('Roboto-Medium.ttf', 'Roboto'),
        fontWeight: 500
      },
      {
        src: fontUrl('Roboto-Thin.ttf', 'Roboto'),
        fontWeight: 100
      },
      {
        src: fontUrl('Roboto-Light.ttf', 'Roboto'),
        fontWeight: 300
      },
      {
        src: fontUrl('Roboto-BoldItalic.ttf', 'Roboto'),
        fontStyle: 'italic',
        fontWeight: 700
      },
      {
        src: fontUrl('Roboto-RegularItalic.ttf', 'Roboto'),
        fontStyle: 'italic',
        fontWeight: 400
      },
      {
        src: fontUrl('Roboto-MediumItalic.ttf', 'Roboto'),
        fontStyle: 'italic',
        fontWeight: 500
      },
      {
        src: fontUrl('Roboto-ThinItalic.ttf', 'Roboto'),
        fontStyle: 'italic',
        fontWeight: 100
      },
      {
        src: fontUrl('Roboto-LightItalic.ttf', 'Roboto'),
        fontStyle: 'italic',
        fontWeight: 300
      }
    ]
  });

  Font.register({
    family: 'Times New Roman',
    fonts: [
      {
        src: fontUrl('TimesNewRoman-Regular.ttf', 'Times New Roman'),
        fontWeight: 400
      },
      {
        src: fontUrl('TimesNewRoman-RegularItalic.ttf', 'Times New Roman'),
        fontStyle: 'italic',
        fontWeight: 400
      },
      {
        src: fontUrl('TimesNewRoman-Bold.ttf', 'Times New Roman'),
        fontWeight: 700
      },
      {
        src: fontUrl('TimesNewRoman-BoldItalic.ttf', 'Times New Roman'),
        fontStyle: 'italic',
        fontWeight: 700
      }
    ]
  });
};

import ReactPDF, { StyleSheet } from '@react-pdf/renderer';
import { usePDFTheme } from '../hooks';
import { PDFTheme } from '../interfaces';

type Style = ReactPDF.Styles[''];

export const createPDFStyles = <T extends ReactPDF.Styles>(
  style: ((theme: PDFTheme) => T) | T
) => {
  const useStyles = () => {
    const theme = usePDFTheme();
    const stylesheet = style instanceof Function ? style(theme) : style;

    return StyleSheet.create(stylesheet);
  };

  return useStyles;
};

export const mergeStyles = (defaultStyle: Style, style?: Style | Style[]) => {
  if (style instanceof Array) {
    return [defaultStyle, ...style];
  }

  return { ...defaultStyle, ...style };
};

import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { ModalsProvider } from '@mantine/modals';
import { Button } from '@mantine/core';
import { AlertDialog } from './index';
import { useAlertDialog } from '../../hooks';

export default {
  title: 'Example/AlertDialog',
  component: AlertDialog
} as ComponentMeta<typeof AlertDialog>;

const Template: ComponentStory<typeof AlertDialog> = (args) => {
  const ButtonAction = () => {
    const alertDialog = useAlertDialog();

    const handleShow = () => {
      alertDialog.show({
        title: 'Testing',
        highlightedText: '"Yohanes Joshua Christiani"',
        message:
          'Apakah anda yakin ingin menghapus user "Yohanes Joshua Christiani"? Anda tidak akan dapat mengembalikan aksi ini.',
        onConfirm: (onClose, endLoading) => {
          setTimeout(() => {
            endLoading();
            onClose();
          }, 3000);
        },
        ...args
      });
    };

    return <Button onClick={() => handleShow()}>Show Dialog</Button>;
  };

  return (
    <ModalsProvider
      modalProps={{
        shadow: 'none',
        centered: true
      }}
    >
      <ButtonAction />
    </ModalsProvider>
  );
};

const Delete = Template.bind({});

Delete.args = {
  onClose: () => {},
  type: 'delete'
};

const Confirm = Template.bind({});

Confirm.args = {
  onClose: () => {},
  type: 'confirm'
};

const Custom = Template.bind({});

Custom.args = {
  onClose: () => {}
};

export { Delete, Confirm, Custom };

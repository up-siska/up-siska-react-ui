import React, { useCallback, useMemo, useState } from 'react';
import {
  Alert,
  Button,
  Group,
  ButtonProps,
  Highlight,
  useMantineTheme
} from '@mantine/core';
import { CheckIcon, TrashIcon } from '@heroicons/react/solid';
import { ExclamationCircleIcon } from '@heroicons/react/outline';
import { Icon } from '../Icon';
import useStyles from './styles';

export type AlertDialogProps = {
  onClose: () => void;
  confirmButton?: ButtonProps<'button'> & { isShow?: boolean };
  cancelButton?: ButtonProps<'button'> & { isShow?: boolean };
  alertColorScheme?: string;
  onConfirm?: (onClose: () => void, callback?: () => void) => void;
  highlightedText?: string | string[];
  withExtraInfos?: boolean;
  extraInfos?: React.ReactNode;
  children: string;
  type?: 'confirm' | 'delete';
};

type DefaultProps = Omit<AlertDialogProps, 'onClose' | 'children'>;

export const AlertDialog: React.FC<AlertDialogProps> = ({
  onClose,
  onConfirm,
  type,
  highlightedText,
  children,
  ...props
}) => {
  const { classes } = useStyles();
  const theme = useMantineTheme();
  const [isLoading, setIsLoading] = useState(false);

  const deleteProps: DefaultProps = {
    confirmButton: {
      isShow: true,
      color: 'red',
      children: 'Hapus',
      leftIcon: <Icon icon={TrashIcon} />,
      ...props.confirmButton
    },
    cancelButton: {
      isShow: true,
      color: 'primary',
      children: 'Batal',
      ...props.cancelButton
    },
    alertColorScheme: 'red',
    withExtraInfos: true,
    extraInfos:
      'Data yang telah dihapus tidak akan dapat dikembalikan mohon untuk memastikan lagi sebelum melakukan hapus data, jika sudah yakin lanjutkan konfirmasi.'
  };

  const confirmProps: DefaultProps = {
    confirmButton: {
      ...props.confirmButton,
      isShow: true,
      color: 'primary',
      children: 'Ya',
      leftIcon: <Icon icon={CheckIcon} />
    },
    cancelButton: {
      ...props.cancelButton,
      isShow: true,
      color: 'red',
      children: 'Batal'
    },
    alertColorScheme: theme.primaryColor,
    withExtraInfos: false
  };

  const initialProps: DefaultProps = {
    confirmButton: {
      ...props.confirmButton,
      isShow: true,
      color: 'primary',
      children: 'Ya'
    },
    cancelButton: {
      ...props.cancelButton,
      isShow: true,
      color: 'red',
      children: 'Batal'
    },
    ...props
  };

  const getPropsByType = () => {
    const actionProps = {
      confirm: confirmProps,
      delete: deleteProps,
      initial: initialProps
    };

    return actionProps[type || 'initial'];
  };

  const defaultProps = getPropsByType();

  const {
    alertColorScheme,
    confirmButton,
    cancelButton,
    withExtraInfos,
    extraInfos
  } = defaultProps;

  return (
    <>
      <Highlight
        align="center"
        highlight={highlightedText || ''}
        highlightStyles={(theme) => ({
          backgroundColor:
            theme.colors[alertColorScheme || theme.primaryColor][5],
          fontWeight: 700,
          WebkitBackgroundClip: 'text',
          WebkitTextFillColor: 'transparent'
        })}
      >
        {children}
      </Highlight>

      {withExtraInfos && (
        <Alert
          className={classes.alertInfos}
          icon={<Icon icon={ExclamationCircleIcon} size={30} />}
          color={alertColorScheme}
          title="Peringatan"
        >
          {extraInfos}
        </Alert>
      )}

      <Group className={classes.buttonGroup} position="right" spacing="xs">
        {cancelButton?.isShow && <Button onClick={onClose} {...cancelButton} />}
        {confirmButton?.isShow && (
          <Button
            {...confirmButton}
            loading={isLoading}
            onClick={() => {
              setIsLoading(true);
              onConfirm?.(onClose, () => setIsLoading(false));
            }}
          />
        )}
      </Group>
    </>
  );
};

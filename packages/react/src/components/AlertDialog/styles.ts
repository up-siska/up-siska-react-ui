import { createStyles } from '@mantine/core';

const useStyles = createStyles((theme) => ({
  buttonGroup: {
    marginTop: theme.spacing.sm
  },
  alertInfos: {
    marginTop: theme.spacing.sm
  }
}));

export default useStyles;

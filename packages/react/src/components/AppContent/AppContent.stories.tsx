import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { AppContent } from './index';

export default {
  title: 'Example/AppContent',
  component: AppContent
} as ComponentMeta<typeof AppContent>;

const Template: ComponentStory<typeof AppContent> = (props) => (
  <AppContent {...props} />
);

const Content = Template.bind({});

Content.args = {
  navbarDims: {
    width: '100vw',
    height: 60
  },
  children: 'Content',
  isSidebarOpen: true,
  isOpenPermanent: true
};

export { Content };

import React from 'react';
import { Box, Text } from '@mantine/core';
import { useElementSize } from '@mantine/hooks';
import useStyles from './styles';
import { useDeviceView, Devices } from '../../hooks';

export type AppContentProps = {
  navbarDims: ReturnType<typeof useElementSize>;
  children: React.ReactNode;
  isSidebarOpen: boolean;
  isOpenPermanent: boolean;
  ignoreSidebarWidth: Devices;
};

export const AppContent: React.FC<AppContentProps> = ({
  navbarDims,
  children,
  isSidebarOpen,
  isOpenPermanent,
  ignoreSidebarWidth
}) => {
  const { classes } = useStyles();
  const isTablet = useDeviceView(ignoreSidebarWidth);

  return (
    <Box className={classes.contentWrapper}>
      <Box
        id="siska-up-content"
        className={classes.content}
        sx={(theme) => ({
          paddingTop: `calc(${theme.spacing.md}px + ${navbarDims.height.toFixed(
            1
          )}px)`,
          marginLeft: isTablet ? 0 : isSidebarOpen && isOpenPermanent ? 245 : 65
        })}
      >
        {children}
      </Box>
      <Box component="footer" className={classes.footer}>
        <Box className={classes.innerFooter}>
          <Text component="p" className={classes.copyrightText}>
            Copyright &copy; 2022
          </Text>
        </Box>
      </Box>
    </Box>
  );
};

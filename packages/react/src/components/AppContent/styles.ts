import { createStyles } from '@mantine/core';

const useStyles = createStyles((theme) => ({
  root: {
    backgroundColor: theme.colors.body[0],
    minHeight: '100vh'
  },
  contentWrapper: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh'
  },
  content: {
    padding: theme.spacing.md,
    flex: 1
  },
  footer: {
    zIndex: 98
  },
  innerFooter: {
    padding: theme.spacing.sm,
    backgroundColor: theme.colors.paper[0],
    textAlign: 'end',
    borderTop: '1px solid',
    borderTopColor: theme.colors.gray[theme.colorScheme === 'dark' ? 8 : 2]
  },
  copyrightText: {
    fontSize: 10,
    color: theme.colors.text[1]
  }
}));

export default useStyles;

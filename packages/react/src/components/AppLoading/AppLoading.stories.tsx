import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { AppLoading } from './index';

export default {
  title: 'Example/AppLoading',
  component: AppLoading
} as ComponentMeta<typeof AppLoading>;

const Template: ComponentStory<typeof AppLoading> = (props) => (
  <AppLoading {...props} />
);

const Visible = Template.bind({});

Visible.args = {
  isLoading: true
};

const NotVisible = Template.bind({});

NotVisible.args = {
  isLoading: false
};

export { Visible, NotVisible };

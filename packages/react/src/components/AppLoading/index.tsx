import React, { useEffect } from 'react';
import { Box, Transition, Portal } from '@mantine/core';
import { Loading } from '../Loading';
import useStyles from './styles';

export type AppLoadingProps = {
  isLoading: boolean;
};

export const AppLoading: React.FC<AppLoadingProps> = ({ isLoading }) => {
  const { classes } = useStyles();

  useEffect(() => {
    if (isLoading) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = '';
    }

    return () => {
      document.body.style.overflow = '';
    };
  }, [isLoading]);

  return (
    <Portal zIndex={1999}>
      <Transition mounted={isLoading} transition="fade" duration={300}>
        {(styles) => (
          <Box className={classes.loadingWrapper} sx={{ ...(styles as any) }}>
            <Loading size={80} />
          </Box>
        )}
      </Transition>
    </Portal>
  );
};

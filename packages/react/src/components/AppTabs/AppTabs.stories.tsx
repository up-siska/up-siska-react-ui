import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { AppTabs } from './index';
import { AppTabsContent } from '../AppTabsContent';
import { UsersIcon, UserAddIcon } from '@heroicons/react/solid';
import {
  PresentationChartLineIcon,
  LinkIcon,
  ViewListIcon
} from '@heroicons/react/outline';
import { TabMenu } from '../../types';

export default {
  title: 'Example/AppTabs',
  component: AppTabs
} as ComponentMeta<typeof AppTabs>;

const tabsMenu: TabMenu[] = [
  {
    label: 'Dashboard',
    icon: PresentationChartLineIcon,
    path: '/',
    pathnames: ['/']
  },
  {
    label: 'User Management',
    icon: UsersIcon,
    path: '/user',
    pathnames: ['/user']
  }
  // {
  //   label: 'Role Management',
  //   icon: UserAddIcon,
  //   path: '/role',
  //   pathnames: ['/role']
  // },
  // {
  //   label: 'App Access',
  //   icon: ViewListIcon,
  //   path: '/app-access',
  //   pathnames: ['/app-access']
  // },
  // {
  //   label: 'SSO Client',
  //   icon: LinkIcon,
  //   path: '/sso-client',
  //   pathnames: ['/sso-client']
  // },
  // {
  //   label: 'Dashboard',
  //   icon: PresentationChartLineIcon,
  //   path: '/',
  //   pathnames: ['/']
  // },
  // {
  //   label: 'User Management',
  //   icon: UsersIcon,
  //   path: '/user',
  //   pathnames: ['/user']
  // },
  // {
  //   label: 'Role Management',
  //   icon: UserAddIcon,
  //   path: '/role',
  //   pathnames: ['/role']
  // },
  // {
  //   label: 'App Access',
  //   icon: ViewListIcon,
  //   path: '/app-access',
  //   pathnames: ['/app-access']
  // },
  // {
  //   label: 'SSO Client',
  //   icon: LinkIcon,
  //   path: '/sso-client',
  //   pathnames: ['/sso-client']
  // }
];

const Template: ComponentStory<typeof AppTabs> = (props) => (
  <>
    <AppTabs {...props} />
    <AppTabsContent>
      <div style={{ height: 500, padding: 100 }}>Hello</div>
    </AppTabsContent>
  </>
);

const Tabs = Template.bind({}) as typeof Template;

Tabs.args = {
  tabsMenu,
  renderLink: (path, child) => (
    <a
      href={path}
      target="_blank"
      rel="noreferrer"
      style={{ textDecoration: 'none' }}
    >
      {child}
    </a>
  )
};

export { Tabs };

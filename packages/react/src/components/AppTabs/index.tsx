import React, { useEffect, useMemo, useRef, useState } from 'react';
import {
  Tabs,
  TabsProps,
  ThemeIcon,
  Text,
  Box,
  useMantineTheme
} from '@mantine/core';
import { Icon, IconType } from '../';
import { TabMenu } from '../../types';
import { useSiskaTheme } from '../../hooks';
import useStyles from './styles';

export type TabLabelProps = {
  icon?: IconType;
  label: string;
};

export const TabLabel: React.FC<TabLabelProps> = ({ icon, label }) => {
  const { classes } = useStyles();

  return (
    <Box className={classes.labelContainer}>
      {icon && (
        <ThemeIcon
          className={classes.labelIcon}
          size={38}
          color="primary"
          variant="light"
        >
          <Icon icon={icon} size={24} />
        </ThemeIcon>
      )}

      <Text className={classes.labelText}>{label}</Text>
    </Box>
  );
};

export type AppTabsProps = {
  tabsMenu: TabMenu[];
  renderLink: (path: string, children: React.ReactNode) => React.ReactNode;
} & Omit<TabsProps, 'children'>;

export const AppTabs: React.FC<AppTabsProps> = ({
  tabsMenu,
  renderLink,
  ...props
}) => {
  const tabsRef = useRef<HTMLDivElement | null>(null);
  const theme = useMantineTheme();
  const { classes } = useStyles();
  const { isStudent } = useSiskaTheme();
  const borderColor = useMemo(() => {
    return theme.colorScheme === 'dark'
      ? theme.colors.gray[8]
      : theme.colors.gray[2];
  }, [theme.colorScheme, theme.colors.gray]);
  const [windowWidth, setWindowWidth] = useState(0);

  const isOverflown = useMemo(() => {
    const tabsEl = tabsRef.current;
    const el = tabsEl?.getElementsByClassName(
      'mantine-Tabs-tabsList'
    )[0] as HTMLDivElement;

    if (el) {
      return el.scrollWidth > el.getBoundingClientRect().width;
    }
    return false;
    // eslint-disable-next-line
  }, [windowWidth]);

  useEffect(() => {
    const contentEl = document?.querySelector(
      '#app-tabs-content'
    ) as HTMLDivElement | null;

    if (contentEl) {
      if (!isOverflown) {
        contentEl.style.borderTopRightRadius = `${theme.spacing.sm}px`;
      } else {
        contentEl.style.borderTopRightRadius = `0px`;
      }
    }
  }, [isOverflown, theme.spacing.sm]);

  useEffect(() => {
    setWindowWidth(window.innerWidth);
    window.addEventListener('resize', () => {
      setWindowWidth(window.innerWidth);
    });

    return () => {
      window.removeEventListener('resize', () => {
        setWindowWidth(window.innerWidth);
      });
    };
  }, []);

  return (
    <>
      <Box>
        <Tabs
          ref={tabsRef}
          variant="unstyled"
          classNames={{
            tabControl: classes.tabControl,
            tabActive: classes.tabActive,
            tabsList: classes.tabList
          }}
          styles={(theme) => ({
            tabControl: {
              border: isStudent ? `1px solid ${borderColor}` : '0',
              boxShadow: isStudent ? 'none' : theme.shadows.xs,
              '&:focus:not(:focus-visible)': {
                boxShadow: isStudent ? 'none' : theme.shadows.xs
              }
            },
            tabActive: {
              borderColor: isStudent ? borderColor : theme.colors.blue[7],
              '&:after': {
                height: isStudent ? 0 : 6
              }
            },
            tabsList: {
              boxShadow: !isStudent
                ? isOverflown
                  ? '0px 10px 8px 0px rgb(0 103 212 / 25%)'
                  : '-8px 5px 8px -6px rgb(0 103 212 / 25%)'
                : 'none'
            }
          })}
          {...props}
        >
          {tabsMenu.map((tab, index) => {
            const { label, icon, path } = tab;

            return (
              <Tabs.Tab
                label={renderLink(
                  path || '/',
                  <TabLabel label={label} icon={icon} />
                )}
                key={index}
              />
            );
          })}
        </Tabs>
      </Box>
    </>
  );
};

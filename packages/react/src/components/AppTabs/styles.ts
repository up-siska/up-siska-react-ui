import { createStyles } from '@mantine/core';

const useStyles = createStyles((theme) => ({
  tabControl: {
    height: 'auto',
    backgroundColor: theme.colors.paper[1],
    padding: 0,
    color:
      theme.colorScheme === 'dark'
        ? theme.colors.dark[1]
        : theme.colors.gray[7],
    borderTopLeftRadius: theme.spacing.sm,
    borderTopRightRadius: theme.spacing.sm,
    marginRight: 12,
    '&:nth-last-child(1)': {
      marginRight: 0
    },
    marginTop: 6
  },
  tabList: {
    '&::-webkit-scrollbar': {
      width: '6px',
      height: '6px',
      borderRadius: '2px',
      backgroundColor: theme.colors.gray[theme.colorScheme === 'dark' ? 7 : 4],
      margin: `0px ${theme.spacing.xs}px`
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: theme.colors.gray[theme.colorScheme === 'dark' ? 8 : 5],
      margin: `0px ${theme.spacing.xs}px`,
      borderRadius: '2px'
    },
    overflowX: 'auto',
    overflowY: 'hidden',
    flexWrap: 'nowrap'
  },
  tabActive: {
    backgroundColor: theme.colors.paper[0],
    color:
      theme.colorScheme === 'dark'
        ? theme.colors.dark[0]
        : theme.colors.gray[9],
    zIndex: 101,
    position: 'relative',
    '&:after': {
      content: "''",
      position: 'absolute',
      left: 0,
      right: 0,
      background: theme.colors.paper[0]
    }
  },
  labelContainer: {
    display: 'flex',
    alignItems: 'center',
    fontWeight: 'bold',
    padding: `${theme.spacing.xs}px ${theme.spacing.md * 2}px`
  },
  labelIcon: {
    marginRight: 0,
    [`@media (min-width: ${theme.breakpoints.lg}px)`]: {
      marginRight: theme.spacing.sm
    }
  },
  labelText: {
    whiteSpace: 'nowrap',
    fontSize: 14,
    [`@media (min-width: ${theme.breakpoints.xl}px)`]: {
      fontSize: 16
    }
  }
}));

export default useStyles;

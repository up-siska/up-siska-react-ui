import React from 'react';
import { AppTabsContent } from './index';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { MantineProvider } from '@mantine/core';
import { darkTheme, defaultTheme, lightTheme } from '../../theme';

export default {
  title: 'Example/AppTabsContent',
  component: AppTabsContent,
  argTypes: {
    children: {
      options: ['Normal', 'Heading', 'Code'],
      control: { type: 'radio' },
      mapping: {
        Heading: <h1>Heading</h1>,
        Code: <code>Code</code>
      }
    }
  },
  args: {
    children: 'Normal'
  }
} as ComponentMeta<typeof AppTabsContent>;

const LightTemplate: ComponentStory<typeof AppTabsContent> = (args) => {
  return (
    <MantineProvider
      withGlobalStyles
      withNormalizeCSS
      theme={{
        ...lightTheme
      }}
    >
      <AppTabsContent {...args} />
    </MantineProvider>
  );
};

const LightTheme = LightTemplate.bind({});

LightTheme.args = {
  ...LightTheme.args
};

const DarkTemplate: ComponentStory<typeof AppTabsContent> = (args) => {
  return (
    <MantineProvider
      withGlobalStyles
      withNormalizeCSS
      theme={{
        ...darkTheme
      }}
    >
      <AppTabsContent {...args} />
    </MantineProvider>
  );
};

const DarkTheme = DarkTemplate.bind({});

DarkTheme.args = {
  ...DarkTheme.args
};

export { LightTheme, DarkTheme };

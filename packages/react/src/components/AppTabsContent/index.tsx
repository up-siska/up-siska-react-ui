import React, { useMemo } from 'react';
import { Box, useMantineTheme } from '@mantine/core';
import useStyles from './styles';
import { useSiskaTheme } from '../../hooks';

export type AppTabsContentProps = {
  children: React.ReactNode;
};

export const AppTabsContent: React.FC<AppTabsContentProps> = ({ children }) => {
  const { classes } = useStyles();
  const { isStudent } = useSiskaTheme();
  const theme = useMantineTheme();
  const borderColor = useMemo(() => {
    return theme.colorScheme === 'dark'
      ? theme.colors.gray[8]
      : theme.colors.gray[2];
  }, [theme.colorScheme, theme.colors.gray]);

  return (
    <Box
      id="app-tabs-content"
      className={classes.root}
      sx={(theme) => ({
        boxShadow: !isStudent ? theme.shadows.sm : 'none',
        border: isStudent ? `1px solid ${borderColor}` : 0
      })}
    >
      {children}
    </Box>
  );
};

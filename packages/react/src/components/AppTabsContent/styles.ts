import { createStyles } from '@mantine/core';

const useStyles = createStyles((theme) => ({
  root: {
    backgroundColor: theme.colors.paper[0],
    zIndex: 100,
    position: 'relative',
    borderBottomLeftRadius: theme.spacing.sm,
    borderBottomRightRadius: theme.spacing.sm
  }
}));

export default useStyles;

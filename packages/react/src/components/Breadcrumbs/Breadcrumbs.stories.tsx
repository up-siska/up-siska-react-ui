import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Breadcrumbs, BreadcrumbItem } from './index';

export default {
  title: 'Example/Breadcrumbs',
  component: Breadcrumbs
} as ComponentMeta<typeof Breadcrumbs>;

const Template: ComponentStory<typeof Breadcrumbs> = (args) => {
  const items: BreadcrumbItem[] = [
    {
      label: 'Home',
      path: '/',
      isActive: false
    },
    {
      label: 'About',
      path: '/about',
      isActive: false
    },
    {
      label: 'Hello',
      path: '/about/mantap',
      isActive: true
    }
  ];

  return (
    <Breadcrumbs {...args} items={items} backAction={() => alert('back')} />
  );
};

const Breadcrumb = Template.bind({});

export { Breadcrumb };

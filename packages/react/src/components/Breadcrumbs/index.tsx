import React, { useMemo } from 'react';
import { useRouter } from 'next/router';
import { Badge, Group, Text, useMantineTheme } from '@mantine/core';
import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/outline';
import { ArrowNarrowRightIcon } from '@heroicons/react/solid';
import { Icon } from '../Icon';
import { useSiskaTheme } from '../../hooks';

export type BreadcrumbItem = {
  label: string;
  path: string;
  isActive?: boolean;
};

export type BreadcrumbsProps = {
  items: BreadcrumbItem[];
  backAction?: () => void;
  renderLink?: (child: React.ReactNode, path: string) => React.ReactNode;
};

export const Breadcrumbs: React.FC<BreadcrumbsProps> = ({
  items,
  backAction,
  renderLink
}) => {
  const { back } = useRouter();

  const theme = useMantineTheme();
  const { isStudent, roleType } = useSiskaTheme();
  const notActiveColor = useMemo(() => {
    return theme.colorScheme === 'dark'
      ? theme.colors.gray[5]
      : theme.colors.gray[7];
  }, [theme.colorScheme, theme.colors.gray]);

  const content = (
    <Group spacing="xs">
      {items.map((item, index) => {
        return (
          <Group
            key={index}
            spacing="xs"
            sx={(theme) => ({
              fontSize: theme.fontSizes.sm
            })}
          >
            {item.isActive ? (
              <Text
                size="sm"
                sx={{
                  color: isStudent ? theme.colors.text[1] : 'inherit'
                }}
              >
                {item.label}
              </Text>
            ) : (
              <>
                {renderLink ? (
                  renderLink(
                    <Text
                      component="a"
                      size="sm"
                      sx={(theme) => ({
                        textDecoration: 'none',
                        color: isStudent ? notActiveColor : 'inherit'
                      })}
                    >
                      {item.label}
                    </Text>,
                    item.path
                  )
                ) : (
                  <Text
                    component="a"
                    href={item.path}
                    size="sm"
                    sx={(theme) => ({
                      textDecoration: 'none',
                      color: isStudent ? notActiveColor : 'inherit'
                    })}
                  >
                    {item.label}
                  </Text>
                )}
              </>
            )}
            {index !== items.length - 1 && (
              <Icon
                icon={isStudent ? ChevronRightIcon : ArrowNarrowRightIcon}
                color={isStudent ? notActiveColor : 'inherit'}
              />
            )}
          </Group>
        );
      })}
    </Group>
  );

  return (
    <Group spacing="sm">
      {roleType !== 'MAHASISWA' && (
        <Badge
          component="button"
          size="xl"
          px={8}
          py="sm"
          sx={{ cursor: 'pointer' }}
          type="button"
          onClick={() => {
            backAction ? backAction() : back();
          }}
        >
          <Icon icon={ChevronLeftIcon} />
        </Badge>
      )}

      {isStudent ? <>{content}</> : <Badge size="xl">{content}</Badge>}
    </Group>
  );
};

export default Breadcrumbs;

import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { ButtonAction } from './index';

export default {
  title: 'Example/ButtonAction',
  component: ButtonAction
} as ComponentMeta<typeof ButtonAction>;

const Template: ComponentStory<typeof ButtonAction> = (args) => (
  <ButtonAction {...args} />
);

const IconOnly = Template.bind({});

IconOnly.args = {
  actionType: 'add',
  iconOnly: true
};

const WithLabel = Template.bind({});

WithLabel.args = {
  actionType: 'add',
  children: 'Tambah Data'
};

export { IconOnly, WithLabel };

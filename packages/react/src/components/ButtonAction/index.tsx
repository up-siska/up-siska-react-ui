import React, { useCallback, useMemo } from 'react';
import {
  ActionIcon,
  Button,
  ActionIconProps,
  ButtonProps
} from '@mantine/core';
import {
  PlusIcon,
  PencilAltIcon,
  TrashIcon,
  EyeIcon,
  DownloadIcon,
  AdjustmentsIcon,
  CheckIcon,
  UploadIcon
} from '@heroicons/react/outline';
import { Icon, IconType } from '../Icon';

export type ButtonActionProps = {
  actionType:
    | 'add'
    | 'edit'
    | 'delete'
    | 'detail'
    | 'custom'
    | 'import'
    | 'filter'
    | 'custom'
    | 'check'
    | 'download';
  icon?: IconType;
  iconPosition?: 'left' | 'right';
  colorScheme?: string;
  compactButton?: boolean;
  withIcon?: boolean;
  children?: React.ReactNode;
  iconOnly?: boolean;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  ButtonProps?: ButtonProps<'button'>;
  ActionIconProps?: ActionIconProps<'button'>;
};

export const ButtonAction: React.FC<ButtonActionProps> = ({
  actionType,
  colorScheme,
  compactButton = true,
  withIcon = true,
  iconPosition = 'left',
  onClick,
  children,
  iconOnly = false,
  icon,
  ButtonProps,
  ActionIconProps
}) => {
  const getIcon = useCallback(() => {
    const iconData = {
      add: PlusIcon,
      edit: PencilAltIcon,
      delete: TrashIcon,
      detail: EyeIcon,
      import: UploadIcon,
      filter: AdjustmentsIcon,
      check: CheckIcon,
      download: DownloadIcon,
      custom: icon
    };

    return icon || iconData[actionType];
  }, [actionType, icon]);

  const getButtonColor = useCallback(() => {
    const colors = {
      add: 'primary',
      edit: 'primary',
      delete: 'red',
      detail: 'dark-blue',
      import: 'dark-green',
      filter: '',
      check: 'green',
      download: 'gray',
      custom: colorScheme
    };

    return colorScheme || colors[actionType];
  }, [actionType, colorScheme]);

  const actionIcon = useMemo(() => {
    return getIcon();
  }, [getIcon]);

  const buttonColor = useMemo(() => {
    return getButtonColor();
  }, [getButtonColor]);

  return (
    <>
      {iconOnly ? (
        <ActionIcon
          onClick={onClick}
          color={buttonColor}
          variant="filled"
          {...ActionIconProps}
        >
          {actionIcon && <Icon icon={actionIcon} />}
        </ActionIcon>
      ) : (
        <Button
          onClick={onClick}
          color={buttonColor}
          compact={compactButton}
          leftIcon={
            iconPosition === 'left'
              ? withIcon && actionIcon && <Icon icon={actionIcon} size={18} />
              : undefined
          }
          rightIcon={
            iconPosition === 'right'
              ? withIcon && actionIcon && <Icon icon={actionIcon} size={18} />
              : undefined
          }
          {...ButtonProps}
        >
          {children}
        </Button>
      )}
    </>
  );
};

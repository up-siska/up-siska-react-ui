import React from 'react';
import dayjs from 'dayjs';
import { Button, Box, Group, Tooltip } from '@mantine/core';
import {
  Calendar as MantineCalendar,
  CalendarProps as MantineCalendarProps
} from '@mantine/dates';
import { useId } from '@mantine/hooks';
import useStyles from './styles';

export type CalendarProps = {
  onReset: () => void;
  getDateEvents: (date: Date) => any[];
} & MantineCalendarProps;

export const Calendar: React.FC<CalendarProps> = ({
  onReset,
  getDateEvents,
  value,
  ...props
}) => {
  const { classes } = useStyles();
  const calendarId = useId();

  return (
    <>
      <Group position="right" mb="-40px">
        <Button variant="light" onClick={onReset}>
          Hari Ini
        </Button>
      </Group>
      <MantineCalendar
        value={value}
        fullWidth
        firstDayOfWeek="sunday"
        size="xl"
        locale="id"
        id={calendarId}
        weekdayLabelFormat="dddd"
        renderDay={(date) => {
          const formatDate = (date: Date) => dayjs(date).format('YYYY-MM-DD');
          const currentDate = formatDate(date);
          const isDateSelected =
            currentDate === formatDate(value || new Date());
          const eventCount = getDateEvents(date).length;

          return (
            <Tooltip
              position="bottom"
              placement="center"
              label={`${dayjs(date).format(
                'DD MMM YYYY'
              )}: ${eventCount} Event`}
            >
              {date.getDate()}
              <Group position="center" spacing={2} mt={6}>
                {[...Array(eventCount > 6 ? 6 : eventCount)].map((num) => (
                  <Box
                    key={num}
                    sx={(theme) => ({
                      width: 6,
                      height: 6,
                      background: isDateSelected
                        ? theme.colors.blue[7]
                        : theme.colorScheme === 'dark'
                        ? theme.colors.dark[3]
                        : theme.colors.gray[3],
                      borderRadius: '50%'
                    })}
                  />
                ))}
              </Group>
            </Tooltip>
          );
        }}
        classNames={{
          calendarHeader: classes.calendarHeader,
          calendarHeaderLevel: classes.calendarHeaderLevel,
          calendarHeaderControl: classes.calendarHeaderControl,
          cell: classes.cell,
          day: classes.day,
          selected: classes.selected,
          weekdayCell: classes.weekdayCell,
          monthPickerControl: classes.monthPickerControl,
          monthPickerControlActive: classes.monthPickerControlActive,
          yearPickerControl: classes.yearPickerControl,
          yearPickerControlActive: classes.yearPickerControlActive,
          month: classes.month,
          yearPickerControls: classes.yearPickerControls,
          monthPickerControls: classes.monthPickerControls
        }}
        {...props}
      />
    </>
  );
};

export default Calendar;

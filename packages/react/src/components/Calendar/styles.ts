import { createStyles } from '@mantine/core';

const useStyles = createStyles((theme) => ({
  calendarHeader: {
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  calendarHeaderLevel: {
    flex: '0 auto',
    order: 3,
    marginLeft: theme.spacing.md,
    padding: 0,
    fontWeight: 'bold',
    fontSize: 26,
    gap: theme.spacing.xs,
    '&:hover': {
      backgroundColor: 'transparent'
    }
  },
  calendarHeaderControl: {
    width: 30,
    height: 25,
    backgroundColor: theme.colors.blue[6],
    color: theme.white,
    marginRight: 4,
    '&:hover': {
      backgroundColor: theme.colors.blue[8]
    }
  },
  cell: {
    textAlign: 'center',
    padding: theme.spacing.xs,
    verticalAlign: 'bottom'
  },
  day: {
    width: 65,
    paddingLeft: theme.spacing.lg,
    paddingRight: theme.spacing.lg,
    paddingTop: theme.spacing.xs,
    fontWeight: 600,
    paddingBottom: 30,
    lineHeight: 1,
    '&:after': {
      content: "''",
      position: 'absolute'
    }
  },
  selected: {
    backgroundColor: `${theme.colors.blue[1]} !important`,
    color: `${theme.colors.blue[7]} !important`
  },
  weekdayCell: {
    borderBottom: `2px solid ${
      theme.colorScheme === 'dark' ? theme.colors.dark[3] : theme.colors.gray[3]
    }`,
    height: 60
  },
  monthPickerControl: {
    fontWeight: 600
  },
  monthPickerControlActive: {
    backgroundColor: `${theme.colors.blue[1]} !important`,
    color: `${theme.colors.blue[7]} !important`
  },
  yearPickerControl: {
    fontWeight: 600
  },
  yearPickerControlActive: {
    backgroundColor: `${theme.colors.blue[1]} !important`,
    color: `${theme.colors.blue[7]} !important`
  },
  month: {
    order: 3
  },
  yearPickerControls: {
    order: 3
  },
  monthPickerControls: {
    order: 3
  }
}));

export default useStyles;

import React from 'react';
import { ContainerCard, ContainerCardProps } from '../index';

export type CardProps = ContainerCardProps;

const Card: React.FC<ContainerCardProps> = ({ children, ...props }) => {
  return <ContainerCard {...props}>{children}</ContainerCard>;
};

export default Card;

import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { ContainerCard } from './index';
import { Group } from '@mantine/core';
import { ButtonAction } from '../ButtonAction';
import { TableSearch } from '../DataTable';

export default {
  title: 'Example/ContainerCard',
  component: ContainerCard
} as ComponentMeta<typeof ContainerCard>;

const Template: ComponentStory<typeof ContainerCard> = (args) => {
  return <ContainerCard {...args} />;
};

const Card = Template.bind({}) as typeof Template;

Card.args = {
  title: 'Persetujuan KP',
  titleRightSection: (
    <Group>
      <TableSearch globalFilter="" setGlobalFilter={() => {}} />
      <ButtonAction actionType="add">Tambah Data</ButtonAction>
    </Group>
  ),
  children: <Group p="md">hello</Group>
};

export { Card };

import React, { useMemo } from 'react';
import {
  Group,
  Paper,
  Sx,
  Text,
  Divider,
  useMantineTheme
} from '@mantine/core';
import { useSiskaTheme } from '../../hooks';

export type ContainerCardProps = {
  sx?: Sx;
  title?: string;
  titleRightSection?: React.ReactNode;
};

export const ContainerCard: React.FC<ContainerCardProps> = ({
  children,
  sx,
  title,
  titleRightSection
}) => {
  const theme = useMantineTheme();
  const { isStudent } = useSiskaTheme();
  const borderColor = useMemo(() => {
    return theme.colorScheme === 'dark'
      ? theme.colors.gray[8]
      : theme.colors.gray[2];
  }, [theme.colorScheme, theme.colors.gray]);

  return (
    <Paper
      radius="md"
      p="md"
      pt={title || titleRightSection ? 'xs' : 'md'}
      sx={(theme) => ({
        background: theme.colors.paper[0],
        boxShadow: !isStudent ? theme.shadows.sm : undefined,
        border: isStudent ? `1px solid ${borderColor}` : undefined,
        ...(sx instanceof Function ? sx(theme) : sx)
      })}
    >
      {(title || titleRightSection) && (
        <>
          <Group position="apart">
            {title && <Text weight="bold">{title}</Text>}
            {titleRightSection && titleRightSection}
          </Group>
          <Divider
            my="xs"
            mx={-theme.spacing.md}
            sx={{ borderTopColor: borderColor }}
          />
        </>
      )}
      {children}
    </Paper>
  );
};

export default ContainerCard;

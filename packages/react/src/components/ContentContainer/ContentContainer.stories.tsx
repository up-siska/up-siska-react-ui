import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { ContentContainer } from './index';

export default {
  title: 'Example/ContentContainer',
  component: ContentContainer
} as ComponentMeta<typeof ContentContainer>;

const Template: ComponentStory<typeof ContentContainer> = (args) => {
  return <ContentContainer {...args} />;
};

const Container = Template.bind({});

export { Container };

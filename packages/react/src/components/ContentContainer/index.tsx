import React from 'react';
import { Title, Group, GroupProps, Box } from '@mantine/core';
import { ContainerCard, ContainerCardProps } from '../ContainerCard';
import { BreadcrumbItem, Breadcrumbs } from '../Breadcrumbs';
import { useSiskaTheme } from '../../hooks';

export type ContentContainerProps = {
  children: React.ReactNode;
  headerGroupProps?: GroupProps;
  title?: string;
  titleRightSection?: React.ReactNode;
  withCard?: boolean;
  breadcrumbItems?: BreadcrumbItem[];
  cardProps?: ContainerCardProps;
};

export const ContentContainer: React.FC<ContentContainerProps> = ({
  children,
  headerGroupProps,
  title,
  titleRightSection,
  withCard = true,
  breadcrumbItems
}) => {
  const { isStudent } = useSiskaTheme();

  return (
    <>
      <Box mb="xl">
        <Group position="apart" mb="xs" {...headerGroupProps}>
          <Title
            sx={(theme) => ({
              color: theme.colors.text[2]
            })}
            order={2}
          >
            {title}
          </Title>
          {titleRightSection}
        </Group>
        {breadcrumbItems && <Breadcrumbs items={breadcrumbItems} />}
      </Box>
      {withCard ? <ContainerCard>{children}</ContainerCard> : <>{children}</>}
    </>
  );
};

export default ContentContainer;

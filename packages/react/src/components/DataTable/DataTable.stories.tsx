import React, { useCallback, useMemo, useRef, useState } from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Group, MultiSelect } from '@mantine/core';
import { TableColumn } from '../../types/data-table';
import {
  DataTable,
  TableSearch,
  TableSearchProps,
  ChangeCheckboxes,
  CheckboxOption
} from './index';
import { ButtonAction } from '../ButtonAction';
import { renderTableColumn } from '../../utils';
import { dataDummy } from '../../../table-dummy';
import {
  useCurrentTablePage,
  useDataTableCheckbox,
  useMultiSelectTable
} from '../../';

export default {
  title: 'Example/DataTable',
  component: DataTable
} as ComponentMeta<typeof DataTable>;

interface IAccount {
  id: number;
  account_code: string;
  account_name: string;
  account_type: string;
  created_by: string | null;
  updated_by: string | null;
  created_at: string | null;
  updated_at: string | null;
  deleted_at: string | null;
}

interface IAccountSelect
  extends Pick<IAccount, 'account_code' | 'account_name' | 'id'> {}

const nestedCols: TableColumn<IAccount>[] = [
  {
    Header: 'SO',
    Footer: 'SO FOOTER',
    cellStyle: (theme) => ({
      ['& > div']: {
        justifyContent: 'center'
      }
    }),
    columns: [
      {
        Header: 'Method',
        cellStyle: (theme) => ({
          ['& > div']: {
            justifyContent: 'center'
          }
        }),
        columns: [
          {
            Header: 'CO',
            columns: [
              {
                Header: 'Name',
                accessor: 'account_name',
                cellStyle: {
                  textAlign: 'center',
                  ['& > div']: {
                    justifyContent: 'center'
                  }
                }
              } as TableColumn<IAccount>,
              {
                Header: 'NIM',
                accessor: 'account_code',
                Footer: 'Hello'
              }
            ]
          }
        ]
      } as TableColumn<IAccount>
    ]
  },
  {
    Header: 'SO2',
    columns: [
      {
        Header: 'Mantap',
        columns: [
          {
            Header: 'Hahaha',
            columns: [
              {
                Header: 'Name',
                accessor: () => 'hello'
              },
              {
                Header: 'NIM',
                accessor: () => 'world'
              }
            ]
          }
        ]
      }
    ]
  }
];

const tableColumns = (hello: string): TableColumn<IAccount>[] => [
  {
    Header: 'Nama - Kode',
    Footer: 'TOTAL',
    // accessor: (row) => `${row.account_name} - ${row.account_code}`,
    Cell: function Render({ row }) {
      return 'heloo';
    },
    id: 'name_code'
  },
  {
    Header: 'Tipe',
    accessor: 'account_type',
    Cell: function Render({ row }) {
      return (
        <div>
          {hello} - {row.original.account_type}
        </div>
      );
    }
  }
];

// const tableColumns: TableColumn<IAccount>[] = [
//   {
//     Header: 'Nama - Kode',
//     accessor: (row) => `${row.account_name} - ${row.account_code}`,
//     id: 'name_code'
//   },
//   {
//     Header: 'Tipe',
//     accessor: 'account_type',
//     Cell: function Render({ row }) {
//       return <div>{row.original.account_type}</div>;
//     }
//   }
// ];

const checkboxOptions: CheckboxOption[] = [
  { label: 'Semua', value: '*' },
  { label: 'Tak Satupun', value: '-' },
  { label: 'Direct Costs', value: 'direct costs', key: 'account_type' },
  { label: 'Bank', value: 'bank', key: 'account_type' }
];

const tableActions = renderTableColumn<IAccount>(function Action(row) {
  return (
    <>
      <Group noWrap>
        <ButtonAction actionType="edit" iconOnly />
        <ButtonAction actionType="delete" iconOnly />
      </Group>
    </>
  );
});

const Template: ComponentStory<typeof DataTable> = (args) => {
  const [selectedData, setSelectedData] = useState<IAccountSelect[]>([]);

  const {
    getSelectedData,
    tableCheckbox: { onChangeCheckboxes, selectedIndex }
  } = useMultiSelectTable({
    data: dataDummy,
    selected: selectedData,
    matcher: {
      data: 'id',
      selected: 'id',
      row: ({ original }) => original.id
    },
    converter: ({ id, account_code, account_name }) => ({
      id,
      account_code,
      account_name
    })
  });

  const getData = useCallback(() => {
    // eslint-disable-next-line
    console.log(getSelectedData());
  }, [getSelectedData]);

  // const callbackedColumn = useCallback((hello: string) => {
  //   return tableColumns(hello);
  // }, []);

  const message = 'hello';

  const fixedColumns = useMemo(() => {
    return tableColumns(message);
  }, []);

  return (
    <>
      <DataTable
        tableData={dataDummy}
        tableColumns={fixedColumns}
        onChangeCheckboxes={onChangeCheckboxes}
        initialSelectedIds={selectedIndex}
        withCheckbox
        withVerticalBorder
        tableActions={tableActions}
        withFooter
      />

      <button onClick={getData}>GET DATA</button>
      {/* <pre>{JSON.stringify(pageData, null, 4)}</pre> */}
    </>
  );
};

const Table: typeof Template = Template.bind({});

Table.args = {
  tableActions,
  tableSubRow: renderTableColumn<IAccount>((row) => {
    return <pre>{JSON.stringify(row.original, null, 2)}</pre>;
  }),
  tableSubRowOffset: 2,
  // checkboxOptions,
  withSort: false,
  // defaultSortBy: [{ id: 'name_code', desc: false }],
  tableActionsPositionIndex: 3,
  // checkboxActions: (ids) => {
  //   return (
  //     <Group spacing="xs">
  //       <ButtonAction actionType="edit" iconOnly />
  //       <ButtonAction actionType="delete" iconOnly />
  //     </Group>
  //   );
  // },
  compactCellWidth: true
};

export { Table };

import React, { useState, useCallback, useEffect } from 'react';
import axios from 'axios';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Group } from '@mantine/core';
import { TableColumn } from '../../../types/data-table';
import {
  ChangeCheckboxes,
  CheckboxOption,
  TableSearch,
  TableSearchProps
} from '../index';
import { ServerDataTable, TableQueries } from './index';
import { ButtonAction } from '../../ButtonAction';
import { renderTableColumn, generateTableQueryParams } from '../../../utils';
import { useDataTableCheckbox, useTableQueries } from '../../../hooks';

export default {
  title: 'Example/ServerDataTable',
  component: ServerDataTable
} as ComponentMeta<typeof ServerDataTable>;

const nestedCols: TableColumn<any>[] = [
  {
    Header: 'SO',
    Footer: 'SO FOOTER',
    cellStyle: (theme) => ({
      ['& > div']: {
        justifyContent: 'center'
      }
    }),
    columns: [
      {
        Header: 'Method',
        cellStyle: (theme) => ({
          ['& > div']: {
            justifyContent: 'center'
          }
        }),
        columns: [
          {
            Header: 'CO',
            columns: [
              {
                Header: 'Name',
                accessor: 'account_name',
                cellStyle: {
                  textAlign: 'center',
                  ['& > div']: {
                    justifyContent: 'center'
                  }
                }
              } as TableColumn<any>,
              {
                Header: 'NIM',
                accessor: 'account_code',
                Footer: 'Hello'
              }
            ]
          }
        ]
      } as TableColumn<any>
    ]
  },
  {
    Header: 'SO2',
    columns: [
      {
        Header: 'Mantap',
        columns: [
          {
            Header: 'Hahaha',
            columns: [
              {
                Header: 'Name',
                accessor: () => 'hello'
              },
              {
                Header: 'NIM',
                accessor: () => 'world'
              }
            ]
          }
        ]
      }
    ]
  }
];

const tableColumns: TableColumn<any>[] = [
  {
    Header: 'Nama',
    accessor: 'name',
    sortName: 'name'
  },
  {
    Header: 'Username',
    accessor: 'username',
    sortName: 'username'
  },
  {
    Header: 'Role',
    accessor: 'role',
    sortName: 'role'
  }
];

const checkboxOptions: CheckboxOption[] = [
  { label: 'Semua', value: '*' },
  { label: 'Tak Satupun', value: '-' },
  { label: 'Direct Costs', value: 'direct costs', key: 'account_type' },
  { label: 'Bank', value: 'bank', key: 'account_type' }
];

const tableActions = renderTableColumn(function Action(row) {
  return (
    <>
      <Group noWrap>
        <ButtonAction actionType="edit" iconOnly />
        <ButtonAction actionType="delete" iconOnly />
      </Group>
    </>
  );
});

const initialTableQueries: TableQueries = {
  page: 1,
  perPage: 10,
  sortData: [
    {
      field: 'name',
      type: 'asc'
    }
  ],
  search: [
    {
      field: 'name',
      value: ''
    },
    {
      field: 'username',
      value: ''
    },
    {
      field: 'role',
      value: ''
    }
  ]
};

const Template: ComponentStory<typeof ServerDataTable> = (args) => {
  const { selectedIndex, onChangeCheckboxes } = useDataTableCheckbox();
  const { tableQueries, handleRefetch, queryParams } =
    useTableQueries(initialTableQueries);
  const [isLoading, setIsLoading] = useState(false);
  const [tableData, setTableData] = useState<any>({
    data: [],
    pageInfo: {}
  });
  const [tableSearchProps, setTableSearchProps] =
    React.useState<TableSearchProps>({
      globalFilter: '',
      setGlobalFilter: () => {}
    });

  const getTableSearchProps = React.useCallback((props: TableSearchProps) => {
    setTableSearchProps(props);
  }, []);

  const getData = useCallback(async () => {
    setIsLoading(true);
    try {
      const { data } = await axios.get(
        `http://ganeshcomdemo.com:8012/api/users?${queryParams}`
      );
      setTableData({
        data: data.data.nodes,
        pageInfo: { ...data.pageInfo, totalRows: data.totalCount }
      });
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      //eslint-disable-next-line
      console.error(error);
    }
  }, [queryParams]);

  useEffect(() => {
    getData();
  }, [getData]);

  return (
    <>
      <Group position="apart" mb="-12px">
        <TableSearch {...tableSearchProps} />
        <ButtonAction actionType="add">Tambah Data</ButtonAction>
      </Group>
      <ServerDataTable
        {...args}
        tableData={tableData.data}
        onRefetch={handleRefetch}
        totalRows={tableData.pageInfo.totalRows}
        tableQueries={tableQueries}
        // onChangeCheckboxes={onChangeCheckboxes}
        // initialSelectedIds={selectedIndex}
        isLoading={isLoading}
        getTableSearchProps={getTableSearchProps}
        tableSubRowOffset={2}
        withVerticalBorder
        withFooter
        tableSubRow={() => {
          return <>Test</>;
        }}
      />
      <pre style={{ marginBottom: 30 }}>
        {JSON.stringify(tableData.pageInfo, null, 2)}
      </pre>
      <pre style={{ marginBottom: 30 }}>
        {JSON.stringify(selectedIndex, null, 2)}
      </pre>
    </>
  );
};

const Table: typeof Template = Template.bind({});

Table.args = {
  tableColumns: nestedCols
  // tableActions
  // withCheckbox: true
};

export { Table };

import React, {
  useMemo,
  useEffect,
  useCallback,
  useState,
  Fragment
} from 'react';
import {
  Table,
  Text,
  Box,
  Divider,
  Group,
  Menu,
  LoadingOverlay,
  ActionIcon
} from '@mantine/core';
import { useDidUpdate, useElementSize } from '@mantine/hooks';
import { useTable, useExpanded, useRowSelect } from 'react-table';
import {
  ArrowSmUpIcon,
  DatabaseIcon,
  ChevronDownIcon
} from '@heroicons/react/solid';
import { ExclamationIcon } from '@heroicons/react/outline';
import { Loading } from '../../Loading';
import { Icon } from '../../Icon';
import {
  TableSearch,
  PaginationTable,
  TableCheckbox,
  DataTableProps,
  generateTableColumns,
  getColumnLength,
  CheckboxOption
} from '../index';
import useStyles from '../styles';
import { getErrorMessage } from '@siska-up-ui/fetcher';

export type TableQueries<T = string> = {
  page: number;
  perPage: number;
  sortData?: {
    field: T;
    type: 'asc' | 'desc';
  }[];
  search?: {
    field: T;
    value: string;
  }[];
  fields?: string[];
  filter?: {
    field: T;
    value: string;
  }[];
};

export type ServerDataTableProps<D extends object> = {
  tableQueries: TableQueries;
  onRefetch: (queries: TableQueries) => void;
} & Omit<
  DataTableProps<D>,
  'totalRows' | 'defaultSortBy' | 'getCurrentPageData'
> & {
    totalRows: number;
  };

export type ServerDataTableComponent = <D extends object>(
  props: ServerDataTableProps<D>
) => JSX.Element;

export const ServerDataTable: ServerDataTableComponent = <D extends object>({
  tableData,
  tableColumns,
  withPagination = true,
  withNumber = true,
  withSearch = true,
  withCheckbox,
  withSort = true,
  withVerticalBorder,
  withFooter,
  onChangeCheckboxes,
  checkboxOptions,
  checkboxActions,
  checkboxProps,
  tableSubRow,
  tableSubRowOffset,
  tableActions,
  tableActionsPositionIndex,
  tableFilters,
  actionButton,
  initialSelectedIds,
  isLoading,
  isRefetching,
  errorMessage,
  error,
  tableId,
  totalRows = 0,
  compactCellWidth,
  getTableSearchProps,
  onRefetch,
  tableQueries
}: ServerDataTableProps<D>): JSX.Element => {
  const { classes } = useStyles();
  const data = useMemo(() => tableData, [tableData]);
  const columns = useMemo(() => {
    return generateTableColumns({
      tableColumns,
      tableActions,
      withNumber,
      withCheckbox,
      tableSubRow,
      tableActionsPositionIndex,
      compactCellWidth,
      checkboxProps
    });
  }, [
    tableColumns,
    tableActions,
    withNumber,
    withCheckbox,
    tableSubRow,
    tableActionsPositionIndex,
    compactCellWidth,
    checkboxProps
  ]);
  const [dataTableQuery, setDataTableQuery] = useState(tableQueries);
  const tableDimensions = useElementSize();

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    footerGroups,
    prepareRow,
    visibleColumns,
    rows,
    selectedFlatRows,
    toggleRowSelected,
    toggleAllRowsSelected,
    getToggleAllRowsSelectedProps,
    state: { selectedRowIds }
  } = useTable(
    {
      columns,
      data,
      initialState: {
        selectedRowIds: initialSelectedIds || {}
      }
    },
    useExpanded,
    useRowSelect
  );

  const handleChangeCheckboxOpt = useCallback(
    ({ value, key }: Omit<CheckboxOption, 'label'>) => {
      if (value === '*') {
        rows.forEach((row) => {
          toggleRowSelected(row.index.toString(), true);
        });
      } else if (value === '-') {
        rows.forEach((row) => {
          toggleRowSelected(row.index.toString(), false);
        });
      } else {
        rows.forEach((row) => {
          const anyOriginal = { ...row.original } as any;

          if (anyOriginal[(key || '') as any] === value) {
            toggleRowSelected(row.index.toString(), true);
          } else {
            toggleRowSelected(row.index.toString(), false);
          }
        });
      }
    },
    [rows, toggleRowSelected]
  );

  const handleSearch = useCallback((value: string) => {
    const updatedData = (prev: typeof dataTableQuery) => {
      const searchData = prev.search?.map((data) => {
        return {
          ...data,
          value
        };
      });
      return { ...prev, page: 1, search: searchData };
    };
    setDataTableQuery(updatedData);
  }, []);

  const handleGoToPage = useCallback(
    (page: number | ((pageIndex: number) => number)) => {
      const updatedData = (prev: typeof dataTableQuery) => {
        const _page = page instanceof Function ? page(prev.page - 1) : page;
        return { ...prev, page: _page };
      };
      setDataTableQuery(updatedData);
    },
    []
  );

  const handleSort = useCallback((fieldName: string, type: 'asc' | 'desc') => {
    const updatedData = (prev: typeof dataTableQuery) => {
      return {
        ...prev,
        page: 1,
        sortData: [
          {
            field: fieldName,
            type
          }
        ]
      };
    };
    setDataTableQuery(updatedData);
  }, []);

  const handleChangePerPage = useCallback((value: number) => {
    const updatedData = (prev: typeof dataTableQuery) => {
      return {
        ...prev,
        page: 1,
        perPage: value
      };
    };

    setDataTableQuery(updatedData);
  }, []);

  const getNextSortStatus = useCallback(
    (
      fieldName: string,
      sortData?: {
        field: string;
        type: 'asc' | 'desc';
      }
    ) => {
      if (sortData && sortData.field === fieldName) {
        if (sortData.type === 'asc') {
          return 'desc';
        } else {
          return 'asc';
        }
      } else {
        return 'asc';
      }
    },
    []
  );

  const arrayToMap = useMemo(() => rows, [rows]);

  const columnLength = useMemo(
    () =>
      getColumnLength({
        tableActions,
        tableColumns,
        withNumber,
        withCheckbox,
        tableSubRow
      }),
    [tableActions, tableColumns, withNumber, withCheckbox, tableSubRow]
  );

  const pageCount = useMemo(
    () => Math.ceil(totalRows / dataTableQuery.perPage),
    [totalRows, dataTableQuery.perPage]
  );

  const selectedCount = useMemo(() => {
    return Object.values(selectedRowIds).filter((val) => val === true).length;
  }, [selectedRowIds]);

  useEffect(() => {
    Object.entries(initialSelectedIds || {}).forEach((id) => {
      toggleRowSelected(id[0], id[1]);
    });
  }, [initialSelectedIds, toggleRowSelected]);

  useEffect(() => {
    onRefetch(dataTableQuery);
  }, [onRefetch, dataTableQuery]);

  useDidUpdate(() => {
    toggleAllRowsSelected(false);
  }, [dataTableQuery.page, toggleAllRowsSelected]);

  useEffect(() => {
    if (withCheckbox && onChangeCheckboxes) {
      onChangeCheckboxes({ index: selectedRowIds, rows: selectedFlatRows });
    }
  }, [withCheckbox, selectedRowIds, selectedFlatRows, onChangeCheckboxes]);

  useEffect(() => {
    if (getTableSearchProps) {
      getTableSearchProps({
        globalFilter: dataTableQuery?.search?.[0]?.value || '',
        setGlobalFilter: handleSearch
      });
    }
  }, [dataTableQuery?.search, handleSearch, getTableSearchProps]);

  useEffect(() => {
    setDataTableQuery(tableQueries);
  }, [tableQueries]);

  return (
    <Box className={classes.tableOuter}>
      {[Boolean(actionButton), withCheckbox].includes(true) && (
        <>
          <Box
            className={classes.tableOuterHeader}
            sx={(theme) => ({
              paddingBottom: [withSearch, tableFilters].includes(true)
                ? 0
                : theme.spacing.md
            })}
          >
            {withCheckbox ? (
              <Group spacing="xs" className={classes.checkboxHeader}>
                <TableCheckbox
                  {...getToggleAllRowsSelectedProps()}
                  {...(checkboxProps?.() || {})}
                />
                {checkboxOptions && (
                  <Menu
                    control={
                      <ActionIcon
                        size="xs"
                        color="primary"
                        variant="transparent"
                      >
                        <Icon icon={ChevronDownIcon} />
                      </ActionIcon>
                    }
                  >
                    {checkboxOptions.map((opt, index) => (
                      <Menu.Item
                        onClick={() =>
                          handleChangeCheckboxOpt({
                            value: opt.value,
                            key: opt.key
                          })
                        }
                        key={index}
                      >
                        {opt.label}
                      </Menu.Item>
                    ))}
                  </Menu>
                )}
                {checkboxActions && checkboxActions(selectedRowIds)}
                <Text size="xs">{selectedCount} dipilih</Text>
              </Group>
            ) : (
              <div />
            )}

            {actionButton && <>{actionButton()}</>}
          </Box>
        </>
      )}
      {[withSearch, tableFilters].includes(true) && (
        <>
          <Box className={classes.tableOuterHeader}>
            {tableFilters && tableFilters()}
            {withSearch && !getTableSearchProps && (
              <Box sx={{ marginLeft: 'auto' }}>
                <TableSearch
                  globalFilter={dataTableQuery?.search?.[0]?.value || ''}
                  setGlobalFilter={handleSearch}
                />
              </Box>
            )}
          </Box>
          <Divider />
        </>
      )}

      <Box className={classes.tableWrapper}>
        <LoadingOverlay
          sx={{ zIndex: 99 }}
          visible={Boolean(isRefetching)}
          loader={<Loading size={50} />}
        />
        <Table
          ref={tableDimensions.ref}
          {...getTableProps()}
          id={tableId || ''}
          striped
          sx={(theme) => ({
            borderCollapse: 'collapse',
            border: withVerticalBorder
              ? `1px solid ${
                  theme.colors.gray[theme.colorScheme === 'dark' ? 7 : 5]
                }`
              : '0'
          })}
        >
          <Box component="thead">
            {headerGroups.map((headerGroup, index) => {
              return (
                <Box
                  component="tr"
                  {...headerGroup.getHeaderGroupProps()}
                  key={index}
                >
                  {headerGroup.headers.map((column, index) => {
                    const matchedField = dataTableQuery.sortData?.find(
                      (sort) => column.sortName === sort.field
                    );
                    const currentSortStatus = matchedField
                      ? matchedField.type
                      : null;
                    const nextSortStatus = getNextSortStatus(
                      column?.sortName || '',
                      matchedField
                    );

                    const onSort = () => {
                      if (column.sortName) {
                        handleSort(column.sortName, nextSortStatus);
                      }
                    };

                    return (
                      <Fragment key={index}>
                        {!column.isHide && (
                          <Box
                            component="th"
                            onClick={withSort ? onSort : () => {}}
                            title={
                              withSort && column.sortName
                                ? `Sort ${column.Header}`
                                : undefined
                            }
                            key={index}
                            sx={(theme) => ({
                              border: withVerticalBorder
                                ? `1px solid ${
                                    theme.colors.gray[
                                      theme.colorScheme === 'dark' ? 7 : 4
                                    ]
                                  }`
                                : '0',
                              cursor:
                                withSort && column.sortName
                                  ? 'pointer'
                                  : 'default',
                              width: [
                                'table-number',
                                'table-checkbox',
                                'table-expander'
                              ].includes(column.id)
                                ? 20
                                : 'auto',
                              ...(column.cellStyle instanceof Function
                                ? column.cellStyle?.(theme)
                                : column.cellStyle)
                            })}
                          >
                            <Box className={classes.tableHeadCell}>
                              {column.render('Header')}
                              {withSort && (
                                <Box
                                  className={classes.sortLabel}
                                  style={{
                                    opacity: currentSortStatus ? 1 : 0,
                                    transform: `rotate(${
                                      currentSortStatus === 'desc'
                                        ? '180deg'
                                        : '0deg'
                                    })`
                                  }}
                                >
                                  <Icon icon={ArrowSmUpIcon} />
                                </Box>
                              )}
                            </Box>
                          </Box>
                        )}
                      </Fragment>
                    );
                  })}
                </Box>
              );
            })}
          </Box>
          <Box component="tbody" {...getTableBodyProps()}>
            {isLoading ? (
              <Box component="tr">
                <Box
                  component="td"
                  colSpan={columnLength}
                  sx={(theme) => ({
                    border: withVerticalBorder
                      ? `1px solid ${
                          theme.colors.gray[
                            theme.colorScheme === 'dark' ? 7 : 4
                          ]
                        }`
                      : '0'
                  })}
                >
                  <Box className={classes.alertContainer}>
                    <Loading />
                  </Box>
                </Box>
              </Box>
            ) : !isLoading && (errorMessage || error) ? (
              <Box component="tr">
                <Box
                  component="td"
                  colSpan={columnLength}
                  sx={(theme) => ({
                    border: withVerticalBorder
                      ? `1px solid ${
                          theme.colors.gray[
                            theme.colorScheme === 'dark' ? 7 : 4
                          ]
                        }`
                      : '0'
                  })}
                >
                  <Box className={classes.alertContainer}>
                    <Box>
                      <Box className={classes.alertIcon}>
                        <Icon icon={ExclamationIcon} size={40} />
                      </Box>
                      <Text size="lg" className={classes.alertText}>
                        {error ? (
                          <>{getErrorMessage(error)}</>
                        ) : errorMessage ? (
                          <>
                            {typeof errorMessage === 'boolean'
                              ? 'Terjadi Kesalahan'
                              : errorMessage}
                          </>
                        ) : null}
                      </Text>
                    </Box>
                  </Box>
                </Box>
              </Box>
            ) : !isLoading && arrayToMap.length === 0 ? (
              <Box component="tr">
                <Box
                  component="td"
                  colSpan={columnLength}
                  sx={(theme) => ({
                    border: withVerticalBorder
                      ? `1px solid ${
                          theme.colors.gray[
                            theme.colorScheme === 'dark' ? 7 : 4
                          ]
                        }`
                      : '0'
                  })}
                >
                  <Box className={classes.alertContainer}>
                    <Box>
                      <Box className={classes.alertIcon}>
                        <Icon icon={DatabaseIcon} size={40} />
                      </Box>
                      <Text className={classes.alertText}>Belum ada data.</Text>
                    </Box>
                  </Box>
                </Box>
              </Box>
            ) : (
              arrayToMap.map((row, rowIndex) => {
                prepareRow(row);

                return (
                  <Fragment key={rowIndex}>
                    <Box {...row.getRowProps()} component="tr">
                      {row.cells.map((cell, cellIndex) => {
                        const { cellStyle, isHide } = cell.column;

                        if (isHide) {
                          return null;
                        }

                        return (
                          <Box
                            component="td"
                            className={classes.cellText}
                            {...cell.getCellProps()}
                            key={cellIndex}
                            sx={(theme) => ({
                              border: withVerticalBorder
                                ? `1px solid ${
                                    theme.colors.gray[
                                      theme.colorScheme === 'dark' ? 7 : 4
                                    ]
                                  }`
                                : '0',
                              verticalAlign: 'middle',
                              ...(cellStyle instanceof Function
                                ? cellStyle?.(theme)
                                : cellStyle)
                            })}
                          >
                            {cell.column.id === 'table-number' ? (
                              <>{rowIndex + 1}</>
                            ) : (
                              <>{cell.render('Cell')}</>
                            )}
                          </Box>
                        );
                      })}
                    </Box>
                    {tableSubRow && row.isExpanded ? (
                      <Box component="tr">
                        {tableSubRowOffset && (
                          <>
                            {[...Array(tableSubRowOffset).keys()].map(
                              (_, idx) => {
                                return (
                                  <Box
                                    component="td"
                                    sx={(theme) => ({
                                      border: withVerticalBorder
                                        ? `1px solid ${
                                            theme.colors.gray[
                                              theme.colorScheme === 'dark'
                                                ? 7
                                                : 4
                                            ]
                                          }`
                                        : '0'
                                    })}
                                    key={idx}
                                  ></Box>
                                );
                              }
                            )}
                          </>
                        )}
                        <Box
                          component="td"
                          className={classes.cellText}
                          sx={(theme) => ({
                            border: withVerticalBorder
                              ? `1px solid ${
                                  theme.colors.gray[
                                    theme.colorScheme === 'dark' ? 7 : 4
                                  ]
                                }`
                              : '0'
                          })}
                          colSpan={
                            tableSubRowOffset
                              ? visibleColumns.length - tableSubRowOffset
                              : visibleColumns.length
                          }
                        >
                          {tableSubRow({ row })}
                        </Box>
                      </Box>
                    ) : null}
                  </Fragment>
                );
              })
            )}
          </Box>
          {withFooter ? (
            <Box component="tfoot">
              {footerGroups.map((footerGroup, index) => {
                const isCanCreateRow = footerGroup.headers.map((footer) => {
                  if (
                    ![
                      footer.id.includes('table-checkbox'),
                      footer.id.includes('table-expander'),
                      footer.id.includes('table-actions'),
                      footer.id.includes('table-number')
                    ].includes(true)
                  ) {
                    return Boolean(footer.Footer);
                  }
                  return false;
                });

                return (
                  <>
                    {isCanCreateRow.includes(true) ? (
                      <Box
                        component="tr"
                        {...footerGroup.getFooterGroupProps()}
                        key={index}
                      >
                        {footerGroup.headers.map((column, index) => {
                          return (
                            !column.isHide &&
                            column.Footer && (
                              <Box
                                component="td"
                                {...column.getFooterProps()}
                                key={index}
                                sx={(theme) => ({
                                  border: withVerticalBorder
                                    ? `1px solid ${
                                        theme.colors.gray[
                                          theme.colorScheme === 'dark' ? 7 : 4
                                        ]
                                      }`
                                    : '0',
                                  width: [
                                    'table-number',
                                    'table-checkbox',
                                    'table-expander'
                                  ].includes(column.id)
                                    ? 20
                                    : 'auto',
                                  ...(column.cellStyle instanceof Function
                                    ? column.cellStyle?.(theme)
                                    : column.cellStyle)
                                })}
                              >
                                <Box className={classes.tableHeadCell}>
                                  {column.render('Footer')}
                                </Box>
                              </Box>
                            )
                          );
                        })}
                      </Box>
                    ) : null}
                  </>
                );
              })}
            </Box>
          ) : null}
        </Table>
      </Box>

      {withPagination && (
        <Box>
          <PaginationTable
            pageSize={dataTableQuery.perPage}
            gotoPage={handleGoToPage}
            setPageSize={handleChangePerPage}
            pageIndex={dataTableQuery.page}
            pageCount={pageCount}
            totalRows={totalRows || tableData.length}
            indexFromZero={false}
          />
        </Box>
      )}
    </Box>
  );
};

import React, {
  useMemo,
  useCallback,
  useState,
  useEffect,
  forwardRef,
  Fragment
} from 'react';
import {
  Table,
  Input,
  Select,
  Text,
  Box,
  Divider,
  Checkbox,
  Group,
  Pagination,
  Menu,
  LoadingOverlay,
  ActionIcon,
  MantineTheme,
  CheckboxProps
} from '@mantine/core';
import { useElementSize, useDebouncedValue } from '@mantine/hooks';
import { AxiosError } from 'axios';
import {
  useTable,
  useSortBy,
  usePagination,
  useExpanded,
  useGlobalFilter,
  useRowSelect,
  TableColumn,
  RenderParams,
  Row,
  SortingRule,
  CellProps
} from 'react-table';
import {
  ArrowSmUpIcon,
  DatabaseIcon,
  ChevronRightIcon,
  ChevronDownIcon
} from '@heroicons/react/solid';
import { ApiResponseError, getErrorMessage } from '@siska-up-ui/fetcher';
import { ExclamationIcon, SearchIcon } from '@heroicons/react/outline';
import { Loading } from '../Loading';
import { Icon } from '../Icon';
import useStyles from './styles';
import { useSiskaTheme } from '../../hooks';

export type TableSearchProps = {
  globalFilter: string;
  setGlobalFilter: (value: any) => void;
  withShadow?: boolean;
  fullWidth?: boolean;
};

export const TableSearch: React.FC<TableSearchProps> = ({
  globalFilter,
  setGlobalFilter,
  withShadow = true,
  fullWidth
}) => {
  const [searchValue, setSearchValue] = useState(globalFilter);
  const [debouncedValue] = useDebouncedValue(searchValue, 300);
  const { classes, cx } = useStyles();
  const { roleType } = useSiskaTheme();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;

    setSearchValue(value);
  };

  useEffect(() => {
    setGlobalFilter(debouncedValue);
  }, [debouncedValue, setGlobalFilter]);

  return (
    <Input
      placeholder="Cari..."
      value={searchValue || ''}
      onChange={handleChange}
      icon={<Icon icon={SearchIcon} />}
      classNames={{
        wrapper: fullWidth
          ? classes.inputSearchWrapperFull
          : classes.inputSearchWrapper,
        input: cx(classes.inputSearch, {
          [classes.inputSearchShadow]: withShadow && roleType !== 'MAHASISWA',
          [classes.inputSearchBorder]: roleType === 'MAHASISWA'
        })
      }}
    />
  );
};

export type PaginationTableProps = {
  pageSize: number;
  gotoPage: (updater: number | ((pageIndex: number) => number)) => void;
  pageIndex: number;
  pageCount: number;
  setPageSize: (pageSize: number) => void;
  totalRows: number;
  indexFromZero?: boolean;
};

export const PaginationTable: React.FC<PaginationTableProps> = ({
  pageSize,
  gotoPage,
  pageIndex,
  pageCount,
  setPageSize,
  totalRows,
  indexFromZero = true
}) => {
  const { classes } = useStyles();
  const [activePage, setActivePage] = useState(
    indexFromZero ? pageIndex + 1 : pageIndex
  );

  useEffect(() => {
    gotoPage(indexFromZero ? activePage - 1 : activePage);
  }, [activePage, gotoPage, indexFromZero]);

  useEffect(() => {
    setActivePage(indexFromZero ? pageIndex + 1 : pageIndex);
  }, [pageIndex, indexFromZero]);

  return (
    <Box className={classes.paginatorContainer}>
      <Group spacing={2}>
        <Text size="sm" className={classes.rowText}>
          Show
        </Text>
        <Select
          value={pageSize.toString()}
          searchable={false}
          onChange={(value) => setPageSize(Number(value))}
          data={[10, 20, 30, 40, 50].map((val) => ({
            value: val.toString(),
            label: val.toString()
          }))}
          className={classes.selectPerPage}
        />
        <Text size="sm" className={classes.rowText}>
          of {totalRows} entries
        </Text>
      </Group>

      <Pagination
        page={activePage}
        onChange={setActivePage}
        total={pageCount}
      />
    </Box>
  );
};

export const TableCheckbox = forwardRef<HTMLInputElement, CheckboxProps>(
  ({ indeterminate, ...props }, ref) => {
    return (
      <Checkbox
        indeterminate={indeterminate}
        styles={{
          input: {
            cursor: 'pointer'
          }
        }}
        size="xs"
        ref={ref}
        {...props}
      />
    );
  }
);

export type ChangeCheckboxes<T extends object = any> = {
  index: Record<string, boolean>;
  rows?: Row<T>[];
};

export type CheckboxOption = {
  label: string;
  value: string | number | boolean;
  key?: string;
};

export type DefaultColumnKey = {
  'table-number'?: any;
  'table-checkbox'?: any;
  'table-actions'?: any;
};

export type DataTableProps<D extends object> = {
  // array of object contains data
  tableData: D[];

  // constants column for the data table
  tableColumns: TableColumn<D>[];

  // if true will be display pagination in the bottom of table
  withPagination?: boolean;

  // if true will be display the number of row
  withNumber?: boolean;

  // if true will be display search input
  withSearch?: boolean;

  // if true will be display checkbox for selection
  withCheckbox?: boolean;

  // if true will allow to sort column
  withSort?: boolean;

  // bordered table
  withVerticalBorder?: boolean;

  // make footer rendered
  withFooter?: boolean;

  // default sort by
  defaultSortBy?: SortingRule<any>[];

  // a callback for handle change when selection is changed
  onChangeCheckboxes?: (params: ChangeCheckboxes) => void;

  // checkbox options for selection
  checkboxOptions?: CheckboxOption[];

  // for render checkbox actions
  checkboxActions?: (
    selectedIndexes: Record<string, boolean>
  ) => React.ReactNode;

  // extra prop for checkbox
  checkboxProps?: (
    row?: Row<D>
  ) => CheckboxProps & React.RefAttributes<HTMLInputElement>;

  // for render table sub
  tableSubRow?: (params: RenderParams<any>) => React.ReactNode;

  // for render table actions
  tableActions?: (params: RenderParams<any>) => React.ReactNode;

  // index where to display the table action, default in last index
  tableActionsPositionIndex?: number;

  // table sub row offset cell
  tableSubRowOffset?: number;

  //for render filters in the top
  tableFilters?: () => React.ReactNode;

  // if true will be display add button
  actionButton?: () => React.ReactNode;

  // loading state if data is loading
  isLoading?: boolean;

  // is refetching state
  isRefetching?: boolean;

  // error message if data failed to get
  errorMessage?: string | boolean | null;

  // error to generate error message
  error?: AxiosError<ApiResponseError<any> | null> | Error | null;

  // id for table tag
  tableId?: string;

  // total data in all rows
  totalRows?: number;

  // initial object for selected ids
  initialSelectedIds?: Record<string, boolean>;

  // width will fit from content inside cell
  compactCellWidth?: boolean;

  // ref for render search component outside of datatable component
  getTableSearchProps?: (props: TableSearchProps) => void;

  //
  getCurrentPageData?: (data: D[]) => void;
};

export const generateTableColumns = <D extends object>(
  options: Pick<
    DataTableProps<D>,
    | 'tableColumns'
    | 'tableActions'
    | 'withNumber'
    | 'withCheckbox'
    | 'tableSubRow'
    | 'tableActionsPositionIndex'
    | 'compactCellWidth'
    | 'checkboxProps'
  >
): TableColumn<D>[] => {
  const {
    tableColumns,
    tableActions,
    withNumber,
    withCheckbox,
    tableSubRow,
    tableActionsPositionIndex,
    compactCellWidth,
    checkboxProps
  } = options;

  let fixedTableColumns = tableColumns as TableColumn<any>[];

  if (withNumber) {
    fixedTableColumns = [
      {
        Header: 'No',
        accessor: 'table-number',
        id: 'table-number'
      },
      ...fixedTableColumns
    ];
  }

  if (withCheckbox) {
    fixedTableColumns = [
      {
        id: 'table-checkbox',
        accessor: 'table-checkbox',
        Header: '',
        Cell: ({ row }: React.PropsWithChildren<CellProps<D>>) => (
          <TableCheckbox
            sx={{ transform: 'translateY(1px)' }}
            {...row.getToggleRowSelectedProps()}
            {...(checkboxProps?.(row) || {})}
          />
        ),
        disableSortBy: true
      },
      ...fixedTableColumns
    ];
  }

  if (tableSubRow) {
    fixedTableColumns = [
      {
        id: 'table-expander',
        Header: '',
        Cell: ({ row }: React.PropsWithChildren<CellProps<D>>) => (
          <ActionIcon
            color={row.isExpanded ? 'primary' : 'gray'}
            variant="transparent"
            {...row.getToggleRowExpandedProps()}
          >
            <Box
              sx={{
                transform: row.isExpanded ? 'rotate(90deg)' : 'rotate(0deg)',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                transition: 'transform 0.2s'
              }}
            >
              <Icon icon={ChevronRightIcon} size={16} />
            </Box>
          </ActionIcon>
        )
      },
      ...fixedTableColumns
    ];
  }

  if (tableActions) {
    const actionsColumn: any = {
      Header: 'Aksi',
      accessor: 'table-actions',
      id: 'table-actions',
      Cell: function Render({ row }: React.PropsWithChildren<CellProps<D>>) {
        return <>{tableActions({ row })}</>;
      },
      disableSortBy: true
    };

    if (tableActionsPositionIndex || tableActionsPositionIndex === 0) {
      fixedTableColumns.splice(
        tableActionsPositionIndex,
        0,
        actionsColumn as TableColumn<any>
      );
    } else {
      fixedTableColumns = [
        ...fixedTableColumns,
        actionsColumn as TableColumn<any>
      ];
    }
  }

  if (compactCellWidth) {
    return fixedTableColumns.map((col, index) => ({
      ...col,
      cellStyle: (theme: MantineTheme) => ({
        ...(col.cellStyle instanceof Function
          ? col.cellStyle?.(theme)
          : col.cellStyle),
        width: index === fixedTableColumns.length - 1 ? '1%' : '0%'
      })
    })) as any;
  }

  return fixedTableColumns as TableColumn<D>[];
};

export const getColumnLength = <D extends object>(
  options: Pick<
    DataTableProps<D>,
    | 'tableActions'
    | 'tableColumns'
    | 'withNumber'
    | 'withCheckbox'
    | 'tableSubRow'
  >
) => {
  const { tableActions, tableColumns, withNumber, withCheckbox, tableSubRow } =
    options;

  let colLength = 0;

  const loopHandlers = (col: any) => {
    if (
      [Boolean(col.accessor), Boolean(col.Cell)].includes(true) &&
      !col.isHide
    ) {
      colLength += 1;
      return;
    }

    if (col.columns) {
      col.columns.forEach(loopHandlers);
    }
  };

  tableColumns.forEach(loopHandlers);

  if (tableSubRow) {
    colLength += 1;
  }

  if (withNumber) {
    colLength += 1;
  }

  if (tableActions) {
    colLength += 1;
  }

  if (withCheckbox) {
    colLength += 1;
  }

  return colLength;
};

export type DataTableComponent = <D extends object>(
  props: DataTableProps<D>
) => JSX.Element;

export const DataTable: DataTableComponent = <D extends object>({
  tableData,
  tableColumns,
  withPagination = true,
  withNumber = true,
  withSearch = true,
  withCheckbox,
  withSort = true,
  withVerticalBorder,
  withFooter,
  defaultSortBy,
  onChangeCheckboxes,
  checkboxOptions,
  checkboxActions,
  checkboxProps,
  tableSubRow,
  tableSubRowOffset,
  tableActions,
  tableActionsPositionIndex,
  tableFilters,
  actionButton,
  initialSelectedIds,
  isLoading,
  isRefetching,
  errorMessage,
  error,
  tableId,
  compactCellWidth,
  getTableSearchProps,
  getCurrentPageData
}: DataTableProps<D>): JSX.Element => {
  const { classes } = useStyles();
  const data = useMemo(() => tableData, [tableData]);
  const columns = useMemo(() => {
    return generateTableColumns({
      tableColumns,
      tableActions,
      withNumber,
      withCheckbox,
      tableSubRow,
      tableActionsPositionIndex,
      compactCellWidth,
      checkboxProps
    });
    // eslint-disable-next-line
  }, [
    tableColumns,
    withNumber,
    withCheckbox,
    tableActionsPositionIndex,
    compactCellWidth
  ]);
  const tableDimensions = useElementSize();

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    pageCount,
    gotoPage,
    setPageSize,
    visibleColumns,
    rows,
    setGlobalFilter,
    selectedFlatRows,
    toggleRowSelected,
    getToggleAllPageRowsSelectedProps,
    state: { pageIndex, pageSize, globalFilter, selectedRowIds },
    footerGroups
  } = useTable(
    {
      columns,
      data,
      initialState: {
        selectedRowIds: initialSelectedIds || {},
        sortBy: defaultSortBy || []
      },
      disableSortRemove: true
    },
    useGlobalFilter,
    useSortBy,
    useExpanded,
    usePagination,
    useRowSelect
  );

  const handleChangeCheckboxOpt = useCallback(
    ({ value, key }: Omit<CheckboxOption, 'label'>) => {
      const _rows = withPagination ? page : rows;

      if (value === '*') {
        _rows.forEach((row) => {
          toggleRowSelected(row.index.toString(), true);
        });
      } else if (value === '-') {
        _rows.forEach((row) => {
          toggleRowSelected(row.index.toString(), false);
        });
      } else {
        _rows.forEach((row) => {
          if (row.values[key || ''] === value) {
            toggleRowSelected(row.index.toString(), true);
          } else {
            toggleRowSelected(row.index.toString(), false);
          }
        });
      }
    },
    [rows, page, toggleRowSelected, withPagination]
  );

  const arrayToMap = useMemo(
    () => (withPagination ? page : rows),
    [withPagination, page, rows]
  );

  const selectedCount = useMemo(() => {
    return Object.values(selectedRowIds).filter((val) => val === true).length;
  }, [selectedRowIds]);

  const columnLength = useMemo(
    () =>
      getColumnLength<D>({
        tableActions,
        tableColumns,
        withNumber,
        withCheckbox,
        tableSubRow
      }),
    [tableActions, tableColumns, withNumber, withCheckbox, tableSubRow]
  );

  useEffect(() => {
    if (withCheckbox && onChangeCheckboxes) {
      onChangeCheckboxes({ index: selectedRowIds, rows: selectedFlatRows });
    }
  }, [withCheckbox, selectedRowIds, selectedFlatRows, onChangeCheckboxes]);

  useEffect(() => {
    Object.entries(initialSelectedIds || {}).forEach((id) => {
      toggleRowSelected(id[0], id[1]);
    });
  }, [initialSelectedIds, toggleRowSelected]);

  useEffect(() => {
    if (getTableSearchProps) {
      getTableSearchProps({ globalFilter, setGlobalFilter });
    }
  }, [globalFilter, setGlobalFilter, getTableSearchProps]);

  useEffect(() => {
    if (getCurrentPageData) {
      getCurrentPageData(arrayToMap.map((d) => d.original));
    }
  }, [arrayToMap, getCurrentPageData]);

  return (
    <Box className={classes.tableOuter}>
      {[Boolean(actionButton), withCheckbox].includes(true) && (
        <>
          <Box
            className={classes.tableOuterHeader}
            sx={(theme) => ({
              paddingBottom: [withSearch, tableFilters].includes(true)
                ? 0
                : theme.spacing.md
            })}
          >
            {withCheckbox ? (
              <Group spacing="xs" className={classes.checkboxHeader}>
                <TableCheckbox
                  {...getToggleAllPageRowsSelectedProps()}
                  {...(checkboxProps?.() || {})}
                />
                {checkboxOptions && (
                  <Menu
                    control={
                      <ActionIcon
                        size="xs"
                        color="primary"
                        variant="transparent"
                      >
                        <Icon icon={ChevronDownIcon} />
                      </ActionIcon>
                    }
                  >
                    {checkboxOptions.map((opt, index) => (
                      <Menu.Item
                        onClick={() =>
                          handleChangeCheckboxOpt({
                            value: opt.value,
                            key: opt.key
                          })
                        }
                        key={index}
                      >
                        {opt.label}
                      </Menu.Item>
                    ))}
                  </Menu>
                )}
                {checkboxActions && checkboxActions(selectedRowIds)}
                <Text size="xs">{selectedCount} dipilih</Text>
              </Group>
            ) : (
              <div />
            )}

            {actionButton && <>{actionButton()}</>}
          </Box>
        </>
      )}
      {[withSearch, tableFilters].includes(true) && (
        <>
          <Box className={classes.tableOuterHeader}>
            {tableFilters && tableFilters()}
            {withSearch && !getTableSearchProps && (
              <Box sx={{ marginLeft: 'auto' }}>
                <TableSearch
                  globalFilter={globalFilter}
                  setGlobalFilter={setGlobalFilter}
                />
              </Box>
            )}
          </Box>
          <Divider />
        </>
      )}

      <Divider />

      <Box className={classes.tableWrapper}>
        <LoadingOverlay
          sx={{ zIndex: 99 }}
          visible={Boolean(isRefetching)}
          loader={<Loading size={50} />}
        />
        <Table
          ref={tableDimensions.ref}
          {...getTableProps()}
          id={tableId || ''}
          striped
          sx={(theme) => ({
            borderCollapse: 'collapse',
            border: withVerticalBorder
              ? `1px solid ${
                  theme.colors.gray[theme.colorScheme === 'dark' ? 7 : 5]
                }`
              : '0'
          })}
        >
          <Box component="thead">
            {headerGroups.map((headerGroup, index) => {
              return (
                <Box
                  component="tr"
                  {...headerGroup.getHeaderGroupProps()}
                  key={index}
                >
                  {headerGroup.headers.map((column, index) => {
                    return (
                      !column.isHide && (
                        <Box
                          component="th"
                          {...column.getHeaderProps(
                            withSort ? column.getSortByToggleProps() : undefined
                          )}
                          key={index}
                          sx={(theme) => ({
                            border: withVerticalBorder
                              ? `1px solid ${
                                  theme.colors.gray[
                                    theme.colorScheme === 'dark' ? 7 : 4
                                  ]
                                }`
                              : '0',
                            width: [
                              'table-number',
                              'table-checkbox',
                              'table-expander'
                            ].includes(column.id)
                              ? 20
                              : 'auto',
                            ...(column.cellStyle instanceof Function
                              ? column.cellStyle?.(theme)
                              : column.cellStyle)
                          })}
                          title={withSort ? `Sort ${column.Header}` : undefined}
                        >
                          <Box className={classes.tableHeadCell}>
                            {column.render('Header')}
                            {withSort && (
                              <Box
                                className={classes.sortLabel}
                                style={{
                                  opacity: `${column.isSorted ? '1' : '0'}`,
                                  transform: `rotate(${
                                    column.isSortedDesc ? '180deg' : '0deg'
                                  })`
                                }}
                              >
                                <Icon icon={ArrowSmUpIcon} />
                              </Box>
                            )}
                          </Box>
                        </Box>
                      )
                    );
                  })}
                </Box>
              );
            })}
          </Box>
          <Box component="tbody" {...getTableBodyProps()}>
            {isLoading ? (
              <Box component="tr">
                <Box
                  component="td"
                  colSpan={columnLength}
                  sx={(theme) => ({
                    border: withVerticalBorder
                      ? `1px solid ${
                          theme.colors.gray[
                            theme.colorScheme === 'dark' ? 7 : 4
                          ]
                        }`
                      : '0'
                  })}
                >
                  <Box className={classes.alertContainer}>
                    <Loading />
                  </Box>
                </Box>
              </Box>
            ) : !isLoading && (errorMessage || error) ? (
              <Box component="tr">
                <Box
                  component="td"
                  colSpan={columnLength}
                  sx={(theme) => ({
                    border: withVerticalBorder
                      ? `1px solid ${
                          theme.colors.gray[
                            theme.colorScheme === 'dark' ? 7 : 4
                          ]
                        }`
                      : '0'
                  })}
                >
                  <Box className={classes.alertContainer}>
                    <Box>
                      <Box className={classes.alertIcon}>
                        <Icon icon={ExclamationIcon} size={40} />
                      </Box>
                      <Text size="lg" className={classes.alertText}>
                        {error ? (
                          <>{getErrorMessage(error as any, 'string')}</>
                        ) : errorMessage ? (
                          <>
                            {typeof errorMessage === 'boolean'
                              ? 'Terjadi Kesalahan'
                              : errorMessage}
                          </>
                        ) : null}
                      </Text>
                    </Box>
                  </Box>
                </Box>
              </Box>
            ) : !isLoading && arrayToMap.length === 0 ? (
              <Box component="tr">
                <Box
                  component="td"
                  colSpan={columnLength}
                  sx={(theme) => ({
                    border: withVerticalBorder
                      ? `1px solid ${
                          theme.colors.gray[
                            theme.colorScheme === 'dark' ? 7 : 4
                          ]
                        }`
                      : '0'
                  })}
                >
                  <Box className={classes.alertContainer}>
                    <Box>
                      <Box className={classes.alertIcon}>
                        <Icon icon={DatabaseIcon} size={40} />
                      </Box>
                      <Text className={classes.alertText}>Belum ada data.</Text>
                    </Box>
                  </Box>
                </Box>
              </Box>
            ) : (
              arrayToMap.map((row, rowIndex) => {
                prepareRow(row);

                return (
                  <Fragment key={rowIndex}>
                    <Box {...row.getRowProps()} component="tr">
                      {row.cells.map((cell, cellIndex) => {
                        const { cellStyle, isHide } = cell.column;

                        if (isHide) {
                          return null;
                        }

                        return (
                          <Box
                            component="td"
                            className={classes.cellText}
                            {...cell.getCellProps()}
                            key={cellIndex}
                            sx={(theme) => ({
                              border: withVerticalBorder
                                ? `1px solid ${
                                    theme.colors.gray[
                                      theme.colorScheme === 'dark' ? 7 : 4
                                    ]
                                  }`
                                : '0',
                              verticalAlign: 'middle',
                              ...(cellStyle instanceof Function
                                ? cellStyle?.(theme)
                                : cellStyle)
                            })}
                          >
                            {cell.column.id === 'table-number' ? (
                              <>{rowIndex + 1}</>
                            ) : (
                              <>{cell.render('Cell')}</>
                            )}
                          </Box>
                        );
                      })}
                    </Box>
                    {tableSubRow && row.isExpanded ? (
                      <Box component="tr">
                        {tableSubRowOffset && (
                          <>
                            {[...Array(tableSubRowOffset).keys()].map(
                              (_, idx) => {
                                return (
                                  <Box
                                    component="td"
                                    sx={(theme) => ({
                                      border: withVerticalBorder
                                        ? `1px solid ${
                                            theme.colors.gray[
                                              theme.colorScheme === 'dark'
                                                ? 7
                                                : 5
                                            ]
                                          }`
                                        : '0'
                                    })}
                                    key={idx}
                                  ></Box>
                                );
                              }
                            )}
                          </>
                        )}
                        <Box
                          component="td"
                          className={classes.cellText}
                          sx={(theme) => ({
                            border: withVerticalBorder
                              ? `1px solid ${
                                  theme.colors.gray[
                                    theme.colorScheme === 'dark' ? 7 : 4
                                  ]
                                }`
                              : '0'
                          })}
                          colSpan={
                            tableSubRowOffset
                              ? visibleColumns.length - tableSubRowOffset
                              : visibleColumns.length
                          }
                        >
                          {tableSubRow({ row })}
                        </Box>
                      </Box>
                    ) : null}
                  </Fragment>
                );
              })
            )}
          </Box>
          {withFooter ? (
            <Box component="tfoot">
              {footerGroups.map((footerGroup, index) => {
                const isCanCreateRow = footerGroup.headers.map((footer) => {
                  if (
                    ![
                      footer.id.includes('table-checkbox'),
                      footer.id.includes('table-expander'),
                      footer.id.includes('table-actions'),
                      footer.id.includes('table-number')
                    ].includes(true)
                  ) {
                    return Boolean(footer.Footer);
                  }
                  return false;
                });

                return (
                  <>
                    {isCanCreateRow.includes(true) ? (
                      <Box
                        component="tr"
                        {...footerGroup.getFooterGroupProps()}
                        key={index}
                      >
                        {footerGroup.headers.map((column, index) => {
                          return (
                            !column.isHide &&
                            column.Footer && (
                              <Box
                                component="td"
                                {...column.getFooterProps()}
                                key={index}
                                sx={(theme) => ({
                                  border: withVerticalBorder
                                    ? `1px solid ${
                                        theme.colors.gray[
                                          theme.colorScheme === 'dark' ? 7 : 4
                                        ]
                                      }`
                                    : '0',
                                  width: [
                                    'table-number',
                                    'table-checkbox',
                                    'table-expander'
                                  ].includes(column.id)
                                    ? 20
                                    : 'auto',
                                  ...(column.cellStyle instanceof Function
                                    ? column.cellStyle?.(theme)
                                    : column.cellStyle)
                                })}
                              >
                                <Box className={classes.tableHeadCell}>
                                  {column.render('Footer')}
                                </Box>
                              </Box>
                            )
                          );
                        })}
                      </Box>
                    ) : null}
                  </>
                );
              })}
            </Box>
          ) : null}
        </Table>
      </Box>

      {withPagination && (
        <Box>
          <PaginationTable
            pageSize={pageSize}
            gotoPage={gotoPage}
            setPageSize={setPageSize}
            pageIndex={pageIndex}
            pageCount={pageCount}
            totalRows={rows.length}
          />
        </Box>
      )}
    </Box>
  );
};

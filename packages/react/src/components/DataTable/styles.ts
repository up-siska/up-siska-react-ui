import { createStyles } from '@mantine/core';

const useStyles = createStyles((theme) => ({
  tableOuter: {
    '& *:not(svg)': {
      fontSize: 14
    },
    [`@media (max-width: ${theme.breakpoints.md}px)`]: {
      '& *:not(svg)': {
        fontSize: 12
      }
    }
  },
  tableOuterHeader: {
    paddingTop: theme.spacing.md,
    paddingBottom: theme.spacing.md,
    display: 'flex',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    alignItems: 'center'
  },
  tableWrapper: {
    overflow: 'auto',
    whiteSpace: 'nowrap',
    position: 'relative',
    '&::-webkit-scrollbar': {
      width: '4px',
      borderRadius: '8px',
      backgroundColor: `rgba(0, 0, 0, 0.10)`
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: `rgba(0, 0, 0, 0.10)`,
      borderRadius: '8px'
    }
  },
  checkboxHeader: {
    padding: `7px ${theme.spacing.xs}px`
  },
  paginatorContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: theme.spacing.md,
    flexWrap: 'wrap',
    rowGap: theme.spacing.md
  },
  tableHeadCell: {
    display: 'flex',
    alignItems: 'center',
    transition: 'all 0.2s'
  },
  sortLabel: {
    transition: 'all 0.2s',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  alertContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: `${theme.spacing.xl}px 0px`
  },
  alertIcon: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: theme.spacing.md,
    color: theme.colors.gray[6]
  },
  alertText: {
    color: theme.colors.gray[6]
  },
  cellText: {
    fontSize: 14
  },
  linkText: {
    textDecoration: 'underline',
    cursor: 'pointer'
  },
  inputSearchWrapper: {
    maxWidth: 300
  },
  inputSearchWrapperFull: {
    width: '100%'
  },
  inputSearch: {
    borderRadius: theme.spacing.xs
  },
  inputSearchShadow: {
    boxShadow: `inset ${theme.shadows.md}`,
    background: theme.colors.body[0],
    border: 0
  },
  inputSearchBorder: {
    background: theme.colors.paper[1],
    borderRadius: 4
  },
  selectRowField: {
    '& > div': {
      padding: 10,
      fontSize: 14
    }
  },
  sortIcon: {
    fontSize: 12,
    marginLeft: theme.spacing.xs,
    marginRight: theme.spacing.xs
  },
  selectPerPage: {
    width: 80
  },
  pageText: {
    margin: '0px 4px',
    fontSize: 14
  },
  rowText: {
    marginLeft: 4,
    marginRight: theme.spacing.sm
  },
  expander: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    transition: 'transform 0.2s'
  }
}));

export default useStyles;

import React from 'react';
import { MantineStyleSystemProps, Text } from '@mantine/core';

export type FormSectionProps = {
  children: React.ReactNode;
  title?: string;
  titleGutter?: MantineStyleSystemProps['mb'];
};

export const FormSection: React.FC<FormSectionProps> = ({
  title,
  children,
  titleGutter = 'sm'
}) => {
  return (
    <>
      {title && (
        <Text
          weight="bold"
          sx={(theme) => ({ color: theme.colors.text[2] })}
          mb={titleGutter}
        >
          {title}
        </Text>
      )}
      {children}
    </>
  );
};

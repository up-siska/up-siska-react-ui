import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { ArrowDownIcon } from '@heroicons/react/outline';
import { Icon } from './index';

export default {
  title: 'Example/Icon',
  component: Icon
} as ComponentMeta<typeof Icon>;

const Template: ComponentStory<typeof Icon> = ({ icon, ...args }) => {
  return <Icon icon={ArrowDownIcon} {...args} />;
};

const IconWrapper = Template.bind({});
IconWrapper.args = {
  ...IconWrapper.args
};

export { IconWrapper };

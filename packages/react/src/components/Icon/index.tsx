import React from 'react';
import { Box } from '@mantine/core';

export type IconTypeProps = React.ComponentProps<'svg'>;

export type IconType = (props: IconTypeProps) => JSX.Element;

export type IconProps = {
  icon: IconType;
  size?: number;
  color?: string;
};

export const Icon: React.FC<IconProps> = ({ icon, size = 16, color }) => {
  const HeroIcon = icon as React.ElementType<IconTypeProps>;

  return (
    <Box
      sx={{
        width: size,
        height: size,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        color,
        ['& svg']: {
          width: '100%',
          height: '100%'
        }
      }}
    >
      <HeroIcon />
    </Box>
  );
};

import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { IconButton } from './index';
import { MantineProvider } from '@mantine/core';
import { darkTheme, lightTheme } from '../../theme';

export default {
  title: 'Example/IconButton',
  component: IconButton,
  argTypes: {
    children: {
      options: ['Add', 'Photo', 'Adjustment'],
      control: { type: 'radio' },
      mapping: {
        Add: <></>,
        Photo: <></>,
        Adjustment: <></>
      }
    },
    onClick: { action: 'clicked' }
  },
  args: {
    children: 'Add'
  }
} as ComponentMeta<typeof IconButton>;

const LightTemplate: ComponentStory<typeof IconButton> = (args) => {
  return (
    <MantineProvider
      withGlobalStyles
      withNormalizeCSS
      theme={{
        ...lightTheme
      }}
    >
      <IconButton {...args} />
    </MantineProvider>
  );
};

const DarkTemplate = (args) => {
  return (
    <MantineProvider
      withGlobalStyles
      withNormalizeCSS
      theme={{
        ...darkTheme
      }}
    >
      <IconButton {...args} />
    </MantineProvider>
  );
};

const LightIconButton = LightTemplate.bind({});
LightIconButton.args = {
  ...LightIconButton.args
};
const DarkIconButton = DarkTemplate.bind({});
DarkIconButton.args = {
  ...DarkIconButton.args
};

export { LightIconButton, DarkIconButton };

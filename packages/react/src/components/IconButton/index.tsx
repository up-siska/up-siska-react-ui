import React from 'react';
import { ActionIcon, ActionIconProps } from '@mantine/core';
import useStyles from './styles';

export type IconButtonProps = {
  children: React.ReactNode;
} & ActionIconProps<'button'>;

export const IconButton: React.FC<IconButtonProps> = ({
  children,
  className,
  ...props
}) => {
  const { classes, cx } = useStyles();

  return (
    <ActionIcon {...props} className={cx(classes.iconButton, className)}>
      {children}
    </ActionIcon>
  );
};

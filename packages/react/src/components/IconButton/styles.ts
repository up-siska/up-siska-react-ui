import { createStyles } from '@mantine/core';

const useStyles = createStyles((theme) => ({
  iconButton: {
    borderRadius: '50%',
    background: theme.colors.paper[0],
    fontSize: 24,
    boxShadow: theme.shadows.sm,
    width: 'fit-content',
    height: 'fit-content',
    padding: 6,
    '&:focus:not(:focus-visible)': {
      boxShadow: theme.shadows.sm
    },
    color: theme.colors.text[2]
  }
}));

export default useStyles;

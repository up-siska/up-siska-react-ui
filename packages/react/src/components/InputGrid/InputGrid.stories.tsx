import React from 'react';
import { TextInput } from '@mantine/core';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { InputGrid } from './index';

export default {
  title: 'Example/InputGrid',
  component: InputGrid
} as ComponentMeta<typeof InputGrid>;

const Template: ComponentStory<typeof InputGrid> = (args) => {
  return (
    <div style={{ width: 400 }}>
      <InputGrid {...args} gridLabelProps={{ span: 12, md: 2 }}>
        <TextInput />
      </InputGrid>
    </div>
  );
};

export const GridField = Template.bind({}) as typeof Template;

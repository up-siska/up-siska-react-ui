import React from 'react';
import {
  Grid,
  Text,
  Group,
  ColProps,
  InputWrapper,
  MediaQuery,
  useMantineTheme
} from '@mantine/core';
import { useMediaQuery } from '@mantine/hooks';

export type InputGridProps = {
  label?: string;
  children: React.ReactNode;
  gridLabelProps?: ColProps;
  gridFieldProps?: ColProps;
};

export const InputGrid: React.FC<InputGridProps> = ({
  children,
  label,
  gridLabelProps,
  gridFieldProps
}) => {
  const theme = useMantineTheme();
  const isInputWrapperDisplayed = useMediaQuery(
    `(max-width: ${theme.breakpoints.md}px)`,
    false
  );

  return (
    <Grid gutter="sm">
      <MediaQuery smallerThan="md" styles={{ display: 'none' }}>
        {label ? (
          <Grid.Col xs={12} md={4} {...gridLabelProps}>
            <Group position="left" sx={{ height: '100%' }}>
              <Text component="label" size="sm">
                {label}
              </Text>
            </Group>
          </Grid.Col>
        ) : null}
      </MediaQuery>
      <Grid.Col xs={12} md={label ? 8 : 12} {...gridFieldProps}>
        {isInputWrapperDisplayed ? (
          <InputWrapper label={label}>{children}</InputWrapper>
        ) : (
          <>{children}</>
        )}
      </Grid.Col>
    </Grid>
  );
};

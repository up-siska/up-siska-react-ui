import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Loading } from './index';

export default {
  title: 'Example/Loading',
  component: Loading
} as ComponentMeta<typeof Loading>;

const Template: ComponentStory<typeof Loading> = (props) => (
  <Loading {...props} />
);

const DefaultSize = Template.bind({});

DefaultSize.args = {
  size: undefined
};

const CustomSize = Template.bind({});

CustomSize.args = {
  size: 100
};

export { DefaultSize, CustomSize };

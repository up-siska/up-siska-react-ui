import React from 'react';
import { Text } from '@mantine/core';

export type ModalTitleProps = {
  children: React.ReactNode;
};

export const ModalTitle: React.FC<ModalTitleProps> = ({ children }) => {
  return (
    <Text
      size="xl"
      weight={'bold'}
      sx={(theme) => ({
        color: theme.colors.text[2]
      })}
    >
      {children}
    </Text>
  );
};

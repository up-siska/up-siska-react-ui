import React from 'react';
import { useMantineTheme } from '@mantine/core';

export const NProgressCSS = () => {
  const theme = useMantineTheme();

  return (
    <style>
      {`
        #nprogress {
          pointer-events: none;
        }

        #nprogress .bar {
          background: ${theme.colors[theme.primaryColor][5]};
          position: fixed;
          z-index: 9999;
          top: 0;
          left: 0;
          width: 100%;
          height: 3px;
        }

        .nprogress-custom-parent {
          overflow: hidden;
          position: relative;
        }

        .nprogress-custom-parent #nprogress .spinner,
        .nprogress-custom-parent #nprogress .bar {
          position: absolute;
        }
    `}
    </style>
  );
};

import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { Navbar } from './index';
import { MantineProvider } from '@mantine/core';
import { darkTheme, lightTheme } from '../../theme';

export default {
  title: 'Example/Navbar',
  component: Navbar
} as ComponentMeta<typeof Navbar>;

const LightTemplate: ComponentStory<typeof Navbar> = (args) => {
  return (
    <MantineProvider
      withGlobalStyles
      withNormalizeCSS
      theme={{
        ...lightTheme
      }}
    >
      <Navbar {...args} />
    </MantineProvider>
  );
};

const DarkTemplate: ComponentStory<typeof Navbar> = (args) => {
  return (
    <MantineProvider
      withGlobalStyles
      withNormalizeCSS
      theme={{
        ...darkTheme
      }}
    >
      <Navbar {...args} />
    </MantineProvider>
  );
};

const LightNavbar = LightTemplate.bind({});
LightNavbar.args = {
  ...LightNavbar.args,
  children: () => <a href="/">{/* <AiOutlineHome /> Home */}</a>,
  isSidebarOpen: true,
  renderLogo: () => (
    <a href="/" style={{ textDecoration: 'none' }}>
      {/* <GrSystem style={{ height: 20, width: 20 }} /> */}
    </a>
  )
};

const DarkNavbar = DarkTemplate.bind({});
DarkNavbar.args = {
  ...DarkNavbar.args,
  children: () => <a href="/">{/* <AiOutlineHome /> Home */}</a>,
  isSidebarOpen: true,
  renderLogo: () => (
    <a href="/" style={{ textDecoration: 'none' }}>
      {/* <GrSystem style={{ color: '#fff', height: 20, width: 20 }} /> */}
    </a>
  )
};

export { LightNavbar, DarkNavbar };

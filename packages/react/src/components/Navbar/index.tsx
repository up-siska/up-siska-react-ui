import React, { forwardRef } from 'react';
import { Box, ActionIcon } from '@mantine/core';
import { MenuIcon } from '@heroicons/react/outline';
import useStyles from './styles';
import { Icon, IconType } from '../Icon';

export type NavbarProps = {
  isSidebarOpen?: boolean;
  onToggleSidebar: () => void;
  children?: React.ReactNode;
  renderLogo?: () => React.ReactNode;
  icon: IconType;
  isSidebarIcon?: boolean;
  appText?: () => React.ReactNode;
  withAppText?: boolean;
};

export const Navbar = forwardRef<HTMLDivElement, NavbarProps>(
  (
    {
      isSidebarOpen,
      onToggleSidebar,
      renderLogo,
      children,
      icon,
      isSidebarIcon,
      withAppText,
      appText
    },
    ref
  ) => {
    const { classes } = useStyles();

    return (
      <Box component="nav" className={classes.navbarContainer} ref={ref}>
        <Box className={classes.innerNavbar}>
          <Box className={classes.navbarTitle}>
            {renderLogo?.()}
            {withAppText && appText?.()}
            {isSidebarIcon && (
              <ActionIcon
                className={classes.toggleButton}
                onClick={onToggleSidebar}
              >
                <Icon icon={icon} size={24} />
              </ActionIcon>
            )}
          </Box>
          {children}
        </Box>
      </Box>
    );
  }
);

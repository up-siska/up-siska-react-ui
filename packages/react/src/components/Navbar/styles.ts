import { createStyles } from '@mantine/core';

const useStyles = createStyles((theme) => ({
  navbarContainer: {
    backgroundColor: theme.colors.paper[0],
    position: 'fixed',
    top: 0,
    right: 0,
    left: 0,
    zIndex: 110,
    borderBottom: '1px solid',
    borderBottomColor: theme.colors.gray[theme.colorScheme === 'dark' ? 8 : 2]
  },
  innerNavbar: {
    padding: `${theme.spacing.xs}px ${theme.spacing.xl}px`,
    display: 'flex',
    width: '100%',
    height: '100%',
    justifyContent: 'space-between'
  },
  navbarTitle: {
    display: 'flex',
    alignItems: 'center'
  },
  toggleButton: {
    marginRight: theme.spacing.md
  }
}));

export default useStyles;

import React from 'react';
import { useMantineTheme } from '@mantine/core';

export const ReactDatePickerCSS = () => {
  const theme = useMantineTheme();

  return (
    <style>
      {`.react-datepicker {
  font-family: "Inter", sans-serif;
  background: ${theme.colors.body[0]};
  border: 1px solid ${theme.colors.paper[1]};
}

.react-datepicker__day,
.react-datepicker__day-name,
.react-datepicker__year-text {
  margin: 1px;
  padding: 6px 8px;
  width: 35px;
  height: 35px;
  color: ${theme.colors.text[0]};
}

.react-datepicker__year-text--disabled {
  color: ${
    theme.colorScheme === 'dark' ? theme.colors.gray[6] : theme.colors.gray[5]
  } !important;
}

.react-datepicker__year--container {
  width: 220px;
}

.react-datepicker__year-wrapper {
    max-width: 100%;
}

.react-datepicker__day:hover,
.react-datepicker__year-text:hover {
  background-color: ${theme.colors.paper[1]};
}

.react-datepicker__header {
  border-bottom: 1px solid ${theme.colors.paper[1]};
}

.react-datepicker__navigation {
  top: 12px;
}

.react-datepicker__day--selected,
.react-datepicker__day--keyboard-selected,
.react-datepicker__year-text--selected,
.react-datepicker__year-text--keyboard-selected {
  background-color: ${theme.colors.blue[6]};
  color: ${theme.white};
}

.react-datepicker__day--selected:hover,
.react-datepicker__day--keyboard-selected:hover,
.react-datepicker__year-text--selected:hover,
.react-datepicker__year-text--keyboard-selected:hover {
  background-color: ${theme.colors.blue[7]};
}

.react-datepicker__current-month,
.react-datepicker-year-header {
  font-size: 14px;
  margin: 8px 0px;
  color: ${theme.colors.text[0]};
}

.react-datepicker__day--outside-month {
  color: ${theme.colors.paper[2]};
}

.react-datepicker__header {
  background-color: ${theme.colors.body[0]};
}

.react-datepicker__triangle {
  display: none;
}

.react-datepicker__triangle::before {
  border-top-color: ${theme.colors.body[0]} !important;
  border-bottom-color: ${theme.colors.body[0]} !important;
}

.react-datepicker__triangle::after {
  border-bottom-color: ${theme.colors.body[0]} !important;
  border-top-color: ${theme.colors.body[0]} !important;
}
`}
    </style>
  );
};

export default ReactDatePickerCSS;

import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { SelectModal } from './index';

export default {
  title: 'Example/SelectModal',
  component: SelectModal
} as ComponentMeta<typeof SelectModal>;

const Template: ComponentStory<typeof SelectModal> = (args) => (
  <SelectModal {...args} />
);

export const InputSelectModal = Template.bind({}) as typeof Template;

InputSelectModal.args = {
  error: 'Mantap'
};

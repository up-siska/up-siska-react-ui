import React from 'react';
import {
  InputWrapper,
  Group,
  Input,
  InputWrapperProps,
  InputProps,
  ActionIcon
} from '@mantine/core';
import { SelectorIcon } from '@heroicons/react/solid';
import { Icon } from '../Icon';

export type SelectModalProps = {
  onOpenModal: () => void;
  inputWrapperProps?: Omit<InputWrapperProps, 'label' | 'error'>;
  inputProps?: Omit<InputProps<'input'>, 'placeholder'>;
  label?: string;
  error?: React.ReactNode;
  placeholder?: string;
  fullWidth?: boolean;
};

export const SelectModal: React.FC<SelectModalProps> = ({
  onOpenModal,
  label,
  error,
  inputProps,
  inputWrapperProps,
  placeholder,
  fullWidth = true
}) => {
  return (
    <InputWrapper label={label} error={error} {...inputWrapperProps}>
      <Group sx={{ width: fullWidth ? '100%' : 'auto' }} spacing="xs">
        <Input
          sx={{ flex: 1 }}
          placeholder={placeholder}
          readOnly
          onClick={onOpenModal}
          {...inputProps}
        />
        <ActionIcon variant="filled" color="blue" onClick={onOpenModal}>
          <Icon icon={SelectorIcon} />
        </ActionIcon>
      </Group>
    </InputWrapper>
  );
};

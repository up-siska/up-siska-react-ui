import React, { useState } from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { AcademicCapIcon } from '@heroicons/react/outline';
import { Sidebar } from './index';
import { SidebarMenu } from '../../types';

const menus: SidebarMenu[] = [
  {
    label: 'Persiapan Perkuliahan',
    icon: AcademicCapIcon,
    withTooltip: true,
    pathnames: ['/']
  }
];

const sidebarMenu: Pick<SidebarMenu, 'label' | 'menus'>[] = [
  {
    menus
  }
];

export default {
  title: 'Example/Sidebar',
  component: Sidebar
} as ComponentMeta<typeof Sidebar>;

const Template: ComponentStory<typeof Sidebar> = (props) => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <button onClick={() => setOpen((prev) => !prev)}>toggle</button>
      <Sidebar {...props} isOpen={open} onClose={() => setOpen(false)} />
    </>
  );
};

const OpenedSidebar = Template.bind({}) as typeof Template;

OpenedSidebar.args = {
  navbarHeight: 60,
  sidebarMenuItems: sidebarMenu,
  isOpen: true,
  onOpen: () => {},
  onClose: () => {},
  isOpenPermanent: true,
  getIsActive: (paths) => paths.includes('/'),
  renderLink: (path, child) => <>{child}</>,
  renderCustomContent: (defaultContent) => (
    <>
      Hello World
      <br />
      {defaultContent}
    </>
  )
};

const ClosedSidebar = Template.bind({});

ClosedSidebar.args = {
  ...OpenedSidebar.args,
  isOpen: false,
  isOpenPermanent: false
};

export { OpenedSidebar, ClosedSidebar };

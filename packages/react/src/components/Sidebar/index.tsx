import React, { forwardRef } from 'react';
import { Box, Text, ScrollArea } from '@mantine/core';
import { SidebarItem } from '../SidebarItem';
import { SidebarMenu } from '../../types';
import useStyles from './styles';
import { useSiskaTheme } from '../../hooks';

export type SidebarProps = {
  isOpen: boolean;
  navbarHeight: number;
  isOpenPermanent: boolean;
  onOpen: () => void;
  onClose: () => void;
  sidebarMenuItems: Pick<SidebarMenu, 'label' | 'menus'>[];
  getIsActive: (pathnames: string[]) => boolean;
  renderLink?: (path: string, children: React.ReactNode) => React.ReactNode;
  renderCustomContent?: (defaultContent: React.ReactNode) => React.ReactNode;
};

export type SidebarContentProps = Omit<
  SidebarProps,
  'onOpen' | 'onClose' | 'isOpenPermanent' | 'navbarHeight'
> & {
  onOpenWidth: number;
  onCloseWidth: number;
};

export const SidebarContent: React.FC<SidebarContentProps> = ({
  sidebarMenuItems,
  isOpen,
  onOpenWidth,
  onCloseWidth,
  getIsActive,
  renderLink,
  renderCustomContent
}) => {
  const { classes } = useStyles();

  const content = (
    <>
      {sidebarMenuItems.map(({ menus, label }, index) => {
        return (
          <React.Fragment key={index}>
            {label && (
              <Text
                className={classes.titleText}
                sx={{
                  opacity: isOpen ? 1 : 0
                }}
                size="sm"
                weight={700}
              >
                {label}
              </Text>
            )}
            {menus?.map((menu, index) => {
              return (
                <SidebarItem
                  item={menu}
                  key={index}
                  isSidebarOpen={isOpen}
                  getIsActive={getIsActive}
                  renderLink={renderLink}
                />
              );
            })}
          </React.Fragment>
        );
      })}
    </>
  );

  return (
    <Box
      className={classes.innerSidebar}
      sx={{ width: isOpen ? onOpenWidth : onCloseWidth }}
    >
      {renderCustomContent ? renderCustomContent(content) : content}
    </Box>
  );
};

export const Sidebar = forwardRef<HTMLDivElement, SidebarProps>(
  function SidebarComp(
    {
      isOpen,
      isOpenPermanent,
      navbarHeight,
      onOpen,
      onClose,
      sidebarMenuItems,
      getIsActive,
      renderLink,
      renderCustomContent
    },
    ref
  ) {
    const { classes } = useStyles();
    const { isStudent } = useSiskaTheme();

    return (
      <Box
        className={classes.root}
        onMouseEnter={!isOpenPermanent ? onOpen : () => {}}
        onMouseLeave={!isOpenPermanent ? onClose : () => {}}
        sx={(theme) => ({
          width: isOpen ? 245 : 65,
          top: `${navbarHeight.toFixed(1)}px`,
          whiteSpace: isOpen ? 'normal' : 'nowrap',
          borderRight: isStudent
            ? `1px solid ${
                theme.colorScheme === 'dark'
                  ? theme.colors.gray[8]
                  : theme.colors.gray[2]
              }`
            : 0,
          boxShadow: isStudent ? 'none' : theme.shadows.xs
        })}
        ref={ref}
      >
        <ScrollArea
          sx={{
            height: `calc(100vh - ${navbarHeight}px)`
          }}
          scrollbarSize={8}
          scrollHideDelay={500}
        >
          <SidebarContent
            isOpen={isOpen}
            onOpenWidth={isStudent ? 244 : 245}
            onCloseWidth={65}
            sidebarMenuItems={sidebarMenuItems}
            getIsActive={getIsActive}
            renderLink={renderLink}
            renderCustomContent={renderCustomContent}
          />
        </ScrollArea>
      </Box>
    );
  }
);

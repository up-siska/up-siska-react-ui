import { createStyles } from '@mantine/core';

const useStyles = createStyles((theme) => ({
  root: {
    backgroundColor: theme.colors.paper[0],
    position: 'fixed',
    left: 0,
    bottom: 0,
    zIndex: 105
  },
  innerSidebar: {
    padding: `${theme.spacing.lg}px ${theme.spacing.xs}px`
  },
  titleText: {
    padding: `6px ${theme.spacing.xs}px`,
    transition: 'opacity 0.2s',
    overflow: 'hidden'
  }
}));

export default useStyles;

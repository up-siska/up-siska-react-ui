import React, { useMemo } from 'react';
import {
  ThemeIcon,
  Group,
  Text,
  Box,
  Collapse,
  UnstyledButton,
  Tooltip,
  useMantineTheme
} from '@mantine/core';
import { useDisclosure, useElementSize } from '@mantine/hooks';
import { ChevronLeftIcon } from '@heroicons/react/solid';
import { Icon } from '../Icon';
import { SidebarMenu } from '../../types';
import useStyles from './styles';
import { useSiskaTheme } from '../../hooks';

type CollapseItemProps = {
  isSidebarOpen?: boolean;
  menus: SidebarMenu[];
  isOpen: boolean;
  getIsActive: (pathnames: string[]) => boolean;
  renderLink?: (path: string, children: React.ReactNode) => React.ReactNode;
  iconWidth?: number;
};

const CollapseItem: React.FC<CollapseItemProps> = ({
  isSidebarOpen,
  menus,
  isOpen,
  renderLink,
  getIsActive,
  iconWidth
}) => {
  return (
    <Collapse in={isOpen} sx={{ display: isSidebarOpen ? 'block' : 'none' }}>
      {menus.map(function Item(menu, index) {
        return (
          <SidebarItemCollapse
            menu={menu}
            key={index}
            isSidebarOpen={isSidebarOpen || false}
            getIsActive={getIsActive}
            iconWidth={iconWidth}
            renderLink={renderLink}
          />
        );
      })}
    </Collapse>
  );
};

export type SidebarItemCollapseProps = {
  menu: SidebarMenu;
  iconWidth?: number;
  getIsActive: (pathnames: string[]) => boolean;
  offset?: number;
  isSidebarOpen?: boolean;
  renderLink?: (path: string, children: React.ReactNode) => React.ReactNode;
};

export const SidebarItemCollapse: React.FC<SidebarItemCollapseProps> = ({
  menu,
  iconWidth = 0,
  getIsActive,
  renderLink,
  isSidebarOpen
}) => {
  const { pathnames, path, menus } = menu;
  const { classes } = useStyles();
  const [isSubmenuOpen, submenuHandlers] = useDisclosure(false);

  const isActive = useMemo(() => {
    return !menus ? getIsActive(pathnames || []) : false;
  }, [getIsActive, menus, pathnames]);

  const linkChild = (
    <Text component="a" size="sm" weight={isActive ? 'bold' : 'normal'}>
      <Box
        component="p"
        className={classes.submenuText}
        sx={(theme) => ({
          marginLeft: `calc(${iconWidth}px + ${theme.spacing.xl}px)`,
          color: isActive ? theme.colors.text[2] : theme.colors.text[0],
          fontWeight: isActive ? 'bold' : 'normal',
          '&:before': {
            display: isActive ? 'block' : 'none'
          }
        })}
      >
        {menu.label}
      </Box>
    </Text>
  );

  return (
    <>
      {renderLink && !menus ? (
        renderLink(path || '/', linkChild)
      ) : menus ? (
        <>
          <Group
            position="apart"
            align="center"
            mr="sm"
            onClick={submenuHandlers.toggle}
          >
            {linkChild}
            <Box
              className={classes.toggleIcon}
              sx={{
                transform: `rotate(${isSubmenuOpen ? '-90' : '0'}deg)`
              }}
            >
              <Icon size={12} icon={ChevronLeftIcon} />
            </Box>
          </Group>
          <CollapseItem
            menus={menus}
            isSidebarOpen={isSidebarOpen}
            isOpen={isSubmenuOpen}
            renderLink={renderLink}
            getIsActive={getIsActive}
            iconWidth={iconWidth * 2}
          />
        </>
      ) : (
        linkChild
      )}
    </>
  );
};

export type SidebarItemProps = {
  item: SidebarMenu;
  isSidebarOpen: boolean;
  getIsActive: (pathnames: string[]) => boolean;
  renderLink?: (path: string, children: React.ReactNode) => React.ReactNode;
};

export const SidebarItem: React.FC<SidebarItemProps> = ({
  item,
  isSidebarOpen,
  getIsActive,
  renderLink
}) => {
  const { label, icon, colorScheme, menus, path, pathnames, withTooltip } =
    item;
  const theme = useMantineTheme();
  const { classes } = useStyles();
  const { isStudent } = useSiskaTheme();
  const [isSubmenuOpen, handlers] = useDisclosure(false);
  const iconDims = useElementSize();
  const isActive = useMemo(() => {
    return getIsActive(pathnames || []);
  }, [pathnames, getIsActive]);

  const isActiveSub = useMemo(() => {
    let _isActive = false;

    // const getActiveSub = (menu: SidebarMenu) => {
    //   if (menu.menus) {
    //     menus?.forEach(getActiveSub);
    //   } else {
    //     const isActiveSubMenu = getIsActive(menu.pathnames || []);

    //     if (isActiveSubMenu === true) {
    //       _isActive = isActiveSubMenu;
    //     }
    //   }
    // };

    item.menus?.forEach((menu) => {
      const isActiveSubMenu = getIsActive(menu.pathnames || []);
      if (isActiveSubMenu === true) {
        _isActive = isActiveSubMenu;
      }
    });

    return _isActive;
  }, [item.menus, getIsActive]);

  const renderContent = () => {
    const studentTextColor = isActive
      ? theme.colors.text[3]
      : theme.colorScheme === 'dark'
      ? theme.colors.gray[4]
      : theme.colors.dark[5];

    return (
      <Group spacing="xs" align="center" noWrap>
        {isStudent ? (
          <Box ref={iconDims.ref}>
            {icon && <Icon color={studentTextColor} icon={icon} size={18} />}
          </Box>
        ) : (
          <ThemeIcon ref={iconDims.ref} color={colorScheme} variant="light">
            {icon && <Icon icon={icon} />}
          </ThemeIcon>
        )}

        <Text
          size="sm"
          weight={isStudent ? 600 : 'normal'}
          className={classes.labelText}
          sx={(theme) => ({
            color: isStudent ? studentTextColor : theme.colors.text[0],
            opacity: isSidebarOpen ? '1' : '0'
          })}
        >
          {label}
        </Text>
        {menus && (
          <Box
            className={classes.toggleIcon}
            sx={{
              transform: `rotate(${isSubmenuOpen ? '-90' : '0'}deg)`
            }}
          >
            <Icon icon={ChevronLeftIcon} />
          </Box>
        )}
      </Group>
    );
  };

  return (
    <>
      {path && renderLink ? (
        renderLink(
          path,
          <Box
            sx={(theme) => ({
              backgroundColor: !isStudent
                ? isActive
                  ? theme.colors.paper[2]
                  : 'transparent'
                : 'transparent'
            })}
            component="a"
            className={classes.root}
          >
            {withTooltip && isSidebarOpen ? (
              <Tooltip label={label} sx={{ display: 'block' }} position="right">
                {renderContent()}
              </Tooltip>
            ) : (
              <>{renderContent()}</>
            )}
          </Box>
        )
      ) : (
        <UnstyledButton
          onClick={() => menus && handlers.toggle()}
          className={classes.root}
          sx={(theme) => ({
            backgroundColor: isActiveSub
              ? theme.colors.paper[2]
              : isSubmenuOpen
              ? theme.colorScheme === 'dark'
                ? theme.colors.dark[6]
                : theme.colors.gray[0]
              : 'transparent'
          })}
        >
          {withTooltip && isSidebarOpen ? (
            <Tooltip label={label} sx={{ display: 'block' }} position="right">
              {renderContent()}
            </Tooltip>
          ) : (
            <>{renderContent()}</>
          )}
        </UnstyledButton>
      )}
      {menus && (
        <Collapse
          in={isSubmenuOpen}
          sx={{ display: isSidebarOpen ? 'block' : 'none' }}
        >
          {menus.map(function Item(menu, index) {
            return (
              <SidebarItemCollapse
                menu={menu}
                key={index}
                iconWidth={iconDims.width}
                getIsActive={getIsActive}
                renderLink={renderLink}
                isSidebarOpen={isSidebarOpen}
                offset={index}
              />
            );
          })}
        </Collapse>
      )}
      {/* {menus && <CollapseItem menus={menus} isSidebarOpen={isSidebarOpen} />} */}
    </>
  );
};

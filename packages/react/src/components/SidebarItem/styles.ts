import { createStyles } from '@mantine/core';

const useStyles = createStyles((theme) => ({
  root: {
    display: 'block',
    width: '100%',
    margin: '4px 0px',
    padding: theme.spacing.xs,
    borderRadius: theme.radius.sm,
    color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black,
    '&:hover': {
      backgroundColor:
        theme.colorScheme === 'dark'
          ? theme.colors.dark[6]
          : theme.colors.gray[0]
    },
    cursor: 'pointer',
    textDecoration: 'none',
    overflow: 'hidden'
  },
  labelContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  labelText: {
    flex: 1,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  toggleIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    transition: 'transform 0.2s'
  },
  submenuText: {
    padding: `4px ${theme.spacing.xs}px`,
    textDecoration: 'none',
    cursor: 'pointer',
    position: 'relative',
    '&:hover': {
      color: theme.colors.text[2]
    },
    '&:before': {
      content: '""',
      width: 10,
      height: 10,
      backgroundColor: theme.colors.text[2],
      position: 'absolute',
      left: '-20px',
      borderRadius: '50%',
      top: '50%',
      transform: 'translateY(-50%)'
    }
  }
}));

export default useStyles;

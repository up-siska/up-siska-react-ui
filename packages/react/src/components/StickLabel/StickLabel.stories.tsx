import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { StickLabel } from './index';

export default {
  title: 'Example/StickLabel',
  component: StickLabel
} as ComponentMeta<typeof StickLabel>;

const Template: ComponentStory<typeof StickLabel> = (args) => (
  <StickLabel {...args} />
);

const Label = Template.bind({}) as typeof Template;

Label.args = {
  label: 'Kurikulum Teknik Penambangan 2020',
  badgeLabel: 'Active',
  color: 'blue',
  isGradient: true
};

export { Label };

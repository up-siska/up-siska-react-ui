import React, { useCallback } from 'react';
import {
  Group,
  Box,
  Text,
  Center,
  MantineTheme,
  useMantineTheme,
  GroupProps,
  Sx
} from '@mantine/core';

export type StickLabelProps = {
  color: string;
  label: React.ReactNode;
  badgeLabel: React.ReactNode;
  isGradient?: boolean;
  gradient?: (theme: MantineTheme) => [number, ...string[]];
} & GroupProps;

export const StickLabel: React.FC<StickLabelProps> = ({
  color,
  label,
  badgeLabel,
  isGradient,
  gradient,
  sx,
  ...props
}) => {
  const theme = useMantineTheme();
  const generateColors = useCallback(() => {
    if (isGradient) {
      if (gradient) {
        const customGradient = gradient(theme);
        return theme.fn.linearGradient(...customGradient);
      } else {
        return theme.fn.linearGradient(
          90,
          theme.colors[color]?.[6],
          theme.colors[color]?.[5]
        );
      }
    } else {
      return theme.colors[color]?.[6];
    }
  }, [color, gradient, isGradient, theme]);

  return (
    <Group
      sx={(theme) => ({
        borderRadius: theme.radius.md,
        backgroundColor: theme.colors.body[1],
        width: 'fit-content',
        ...((sx instanceof Function ? sx(theme) : sx) as Sx)
      })}
      spacing={0}
      align="stretch"
      {...props}
    >
      <Box sx={{ flex: 1 }} py={6} px="sm">
        <Text
          sx={(theme) => ({ color: theme.colors.text[2], fontWeight: 600 })}
          size="sm"
        >
          {label}
        </Text>
      </Box>
      <Center
        py={6}
        px="sm"
        sx={(theme) => ({
          background: generateColors(),
          borderTopRightRadius: theme.radius.md,
          borderBottomRightRadius: theme.radius.md
        })}
      >
        <Text
          size="sm"
          sx={(theme) => ({
            color: theme.white
          })}
        >
          {badgeLabel}
        </Text>
      </Center>
    </Group>
  );
};

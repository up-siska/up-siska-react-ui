import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { ToggleTheme } from './index';

export default {
  title: 'Example/ToggleTheme',
  component: ToggleTheme
} as ComponentMeta<typeof ToggleTheme>;

const Template: ComponentStory<typeof ToggleTheme> = (args) => (
  <ToggleTheme {...args} />
);

const ButtonToggle = Template.bind({});

export { ButtonToggle };

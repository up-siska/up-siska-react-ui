import React from 'react';
import { Button } from '@mantine/core';

export const ToggleTheme = () => {
  const toggle = () => {
    const key = 'storybook-siska-ui-theme';
    const currentColor = localStorage.getItem(key) || 'light';

    localStorage.setItem(
      'storybook-siska-ui-theme',
      currentColor === 'light' ? 'dark' : 'light'
    );
  };

  return <Button onClick={toggle}>ToggleTheme</Button>;
};

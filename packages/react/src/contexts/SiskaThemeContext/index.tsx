import React, { createContext, useCallback } from 'react';
import {
  ColorScheme,
  Global,
  MantineProvider,
  MantineProviderProps
} from '@mantine/core';
import {
  defaultGlobalStyles,
  defaultProps,
  defaultStyles,
  defaultStudentTheme,
  defaultTheme,
  darkTheme,
  lightTheme,
  darkStudentTheme,
  lightStudentTheme
} from '../../theme';
import { NProgressCSS, ReactDatePickerCSS } from '../../components';

export type RoleType = 'MAHASISWA' | 'ADMIN';

export type SiskaThemeCtx = {
  roleType: RoleType;
};

export const SiskaThemeContext = createContext<SiskaThemeCtx>({
  roleType: 'ADMIN'
});

export type SiskaThemeProviderProps = {
  children: React.ReactNode;
  roleType: RoleType;
  colorScheme: ColorScheme;
  mantineProviderProps?: MantineProviderProps;
};

export const SiskaThemeProvider: React.FC<SiskaThemeProviderProps> = ({
  children,
  roleType,
  colorScheme,
  mantineProviderProps
}) => {
  const getDefaultTheme = useCallback(() => {
    if (roleType === 'MAHASISWA') {
      return defaultStudentTheme;
    } else {
      return defaultTheme;
    }
  }, [roleType]);

  const getColorSchemeTheme = useCallback(
    (color: ColorScheme) => {
      const themeData = {
        dark: roleType === 'MAHASISWA' ? darkStudentTheme : darkTheme,
        light: roleType === 'MAHASISWA' ? lightStudentTheme : lightTheme
      };

      return themeData[color];
    },
    [roleType]
  );

  return (
    <SiskaThemeContext.Provider value={{ roleType }}>
      <MantineProvider
        {...mantineProviderProps}
        withGlobalStyles
        withNormalizeCSS
        styles={{ ...defaultStyles, ...mantineProviderProps?.styles }}
        defaultProps={{
          ...defaultProps,
          ...mantineProviderProps?.defaultProps
        }}
        theme={{
          ...getDefaultTheme(),
          ...getColorSchemeTheme(colorScheme),
          ...mantineProviderProps?.theme
        }}
      >
        <Global styles={defaultGlobalStyles} />
        <ReactDatePickerCSS />
        <NProgressCSS />
        {children}
      </MantineProvider>
    </SiskaThemeContext.Provider>
  );
};

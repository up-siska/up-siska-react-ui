import React, { useCallback } from 'react';
import { useId } from '@mantine/hooks';
import { useModals } from '@mantine/modals';
import { AlertDialog, AlertDialogProps, ModalTitle } from '../components';

export type AlertDialogOptions = Omit<
  AlertDialogProps,
  'onClose' | 'children'
> & { title?: string; message: string };

export type UseAlertDialogOptions = {
  isConfirmLoading?: boolean;
};

export const useAlertDialog = () => {
  const modals = useModals();
  const id = useId();

  const showAlertDialog = useCallback(
    ({ title = 'Perhatian', message, ...options }: AlertDialogOptions) =>
      modals.openModal({
        id,
        title: <ModalTitle>{title}</ModalTitle>,
        children: (
          <AlertDialog onClose={() => modals.closeModal(id)} {...options}>
            {message}
          </AlertDialog>
        )
      }),
    [id, modals]
  );

  return { show: showAlertDialog, onClose: () => modals.closeModal(id) };
};

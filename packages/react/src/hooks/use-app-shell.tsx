import { useState } from 'react';
import { useDisclosure, useElementSize } from '@mantine/hooks';

export const useAppShell = () => {
  const [isSidebarOpen, sidebarHandler] = useDisclosure(true);
  const [isOpenPermanent, setIsOpenPermanent] = useState(isSidebarOpen);
  const navbarDims = useElementSize();
  const sidebarDims = useElementSize();

  const onToggleSidebar = () => {
    sidebarHandler.toggle();
    setIsOpenPermanent((prev) => !prev);
  };

  const onOpenSidebar = () => {
    sidebarHandler.open();
    setIsOpenPermanent(true);
  };

  const onCloseSidebar = () => {
    sidebarHandler.close();
    setIsOpenPermanent(false);
  };

  return {
    isSidebarOpen,
    isOpenPermanent,
    sidebarHandler,
    onToggleSidebar,
    onOpenSidebar,
    onCloseSidebar,
    navbarDims,
    sidebarDims
  };
};

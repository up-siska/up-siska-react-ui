import { useCallback, useState } from 'react';

export const useCurrentTablePage = <T extends object = any>() => {
  const [pageData, setPageData] = useState<T[]>([]);

  const getCurrentPageData = useCallback((data: T[]) => {
    setPageData(data);
  }, []);

  return {
    pageData,
    getCurrentPageData
  };
};

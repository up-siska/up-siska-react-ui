import { useState, useCallback } from 'react';
import { ChangeCheckboxes } from '../components/DataTable';

export const useDataTableCheckbox = <T extends object = any>() => {
  const [selectedIndex, setSelectedIndex] = useState<ChangeCheckboxes['index']>(
    {}
  );
  const [selectedFlatRows, setSelectedFlatRows] = useState<
    ChangeCheckboxes<T>['rows']
  >([]);

  const onChangeCheckboxes = useCallback(
    ({ index, rows }: ChangeCheckboxes<T>) => {
      setSelectedIndex(index);
      setSelectedFlatRows(rows);
    },
    []
  );

  return {
    selectedIndex,
    selectedFlatRows,
    onChangeCheckboxes,
    stateActions: {
      setSelectedIndex,
      setSelectedFlatRows
    }
  };
};

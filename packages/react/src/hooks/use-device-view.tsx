import { useCallback } from 'react';
import { useMantineTheme } from '@mantine/core';
import { useViewportSize } from '@mantine/hooks';

export type Devices = 'mobile' | 'tablet' | 'desktop';

export const useDeviceView = (device: Devices) => {
  const theme = useMantineTheme();
  const { width } = useViewportSize();

  const getDeviceBreakpoint = useCallback(() => {
    switch (device) {
      case 'mobile':
        return width <= theme.breakpoints.xs;

      case 'tablet':
        return width <= theme.breakpoints.md;

      case 'desktop':
        return width <= theme.breakpoints.lg;

      default:
        return false;
    }
  }, [device, theme, width]);

  return getDeviceBreakpoint();
};

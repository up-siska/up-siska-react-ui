import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Stepper, Group, Button, Box } from '@mantine/core';
import { Dropzone } from '@mantine/dropzone';
import { useDropzone } from '../hooks';

export default {
  title: 'Example/Dropzone',
  component: Dropzone
} as ComponentMeta<typeof Dropzone>;

const Template: ComponentStory<typeof Dropzone> = (props) => {
  const { onDropFile, dropzoneChildren } = useDropzone((file) => {}, {
    allowedFiles: ['jpg', 'png', 'gif']
  });

  return (
    <Box sx={{ width: 500 }} mx="auto">
      <Dropzone onDrop={onDropFile}>{dropzoneChildren}</Dropzone>
    </Box>
  );
};

const Content = Template.bind({});

export { Content };

import React, { useCallback, useState } from 'react';
import {
  Button,
  Center,
  Group,
  MantineTheme,
  Text,
  useMantineTheme
} from '@mantine/core';
import { DropzoneStatus } from '@mantine/dropzone';
import { UploadIcon } from '@heroicons/react/solid';
import { IconType } from '../components';

export type UseDropzoneOptions = {
  multiple?: boolean;
  allowedFiles?: string[];
};

const ImageUploadIcon = ({
  status,
  ...props
}: React.ComponentProps<IconType> & { status: DropzoneStatus }) => {
  return <UploadIcon {...props} height={48} width={48} />;
};

const getIconColor = (status: DropzoneStatus, theme: MantineTheme) => {
  return status.accepted
    ? theme.colors[theme.primaryColor][theme.colorScheme === 'dark' ? 4 : 6]
    : status.rejected
    ? theme.colors.red[theme.colorScheme === 'dark' ? 4 : 6]
    : theme.colorScheme === 'dark'
    ? theme.colors.gray[3]
    : theme.colors.gray[7];
};

export const useDropzone = (
  onAfterDrop: (files: File | File[]) => void,
  options?: UseDropzoneOptions
) => {
  const [previewUrl, setPreviewUrl] = useState<string | string[] | null>(null);
  const [uploadedFiles, setUploadedFiles] = useState<File[]>([]);
  const theme = useMantineTheme();

  const onDropFile = useCallback(
    (files: File[]) => {
      setUploadedFiles(files);
      if (options?.multiple) {
        const objUrls = files.map((file) => URL.createObjectURL(file));
        setPreviewUrl(objUrls);
        onAfterDrop(files);
        return;
      }

      const objUrl = URL.createObjectURL(files[0]);
      setPreviewUrl(objUrl);
      onAfterDrop(files[0]);
    },
    [onAfterDrop, options?.multiple]
  );

  const isImage = useCallback(() => {
    const imageExtensions = ['png', 'jpg', 'jpeg', 'gif', 'svg'];
    const getExtension = (file: File) => {
      const fileNameArray = file.name.split('.');
      return fileNameArray[fileNameArray.length - 1];
    };

    if (options?.multiple) {
      return uploadedFiles.map((file) => {
        const fileExtensions = getExtension(file);
        return imageExtensions.includes(fileExtensions);
      });
    }

    const fileExtensions = getExtension(uploadedFiles[0]);
    return imageExtensions.includes(fileExtensions);
  }, [options?.multiple, uploadedFiles]);

  const dropzoneChildren = useCallback(
    (status: DropzoneStatus) => (
      <Group
        position="center"
        spacing={0}
        direction="column"
        style={{ pointerEvents: 'none' }}
      >
        <Center mb="sm">
          <ImageUploadIcon
            status={status}
            style={{ color: getIconColor(status, theme) }}
          />
        </Center>
        <Text weight="700">Upload file disini.</Text>
        <Text
          size="sm"
          sx={(theme) => ({
            color: theme.colors.gray[theme.colorScheme === 'dark' ? 5 : 6]
          })}
        >
          Drag dan drop, atau klik untuk memilih file.
        </Text>

        {options?.allowedFiles ? (
          <Text
            size="xs"
            sx={(theme) => ({
              color: theme.colors.gray[theme.colorScheme === 'dark' ? 5 : 6]
            })}
          >
            {options.allowedFiles.map((mimeType, index) => {
              const isLast = index === (options.allowedFiles?.length || 0) - 1;
              return (
                <React.Fragment key={index}>
                  {`${isLast ? 'atau ' : ''}${mimeType}${isLast ? '' : ', '}`}
                </React.Fragment>
              );
            })}
          </Text>
        ) : null}
      </Group>
    ),
    [options?.allowedFiles, theme]
  );

  return {
    previewUrl,
    isImage,
    onDropFile,
    dropzoneChildren
  };
};

import { useEffect } from 'react';

export type UseIntersectionObserverContext<T, I> = {
  root?: React.MutableRefObject<T>;
  target?: React.MutableRefObject<T>;
  onIntersect: I;
  threshold?: number;
  rootMargin?: string;
  enabled?: boolean;
};

export const useIntersectionObserver = <
  T extends HTMLElement | null = HTMLElement,
  I extends any = any
>({
  root,
  target,
  onIntersect,
  threshold = 1.0,
  rootMargin = '0px',
  enabled = true
}: UseIntersectionObserverContext<T, I>) => {
  useEffect(() => {
    if (!enabled) {
      return;
    }

    const observer = new IntersectionObserver(
      (entries) =>
        entries.forEach(
          (entry) =>
            entry.isIntersecting &&
            onIntersect instanceof Function &&
            onIntersect()
        ),
      {
        root: root && root.current,
        rootMargin,
        threshold
      }
    );

    const el = target && target.current;

    if (!el) {
      return;
    }

    observer.observe(el);

    return () => {
      observer.unobserve(el);
    };

    //eslint-disable-next-line
  }, [target?.current, enabled]);
};

import { useState, useCallback } from 'react';

export type EditData<T> = {
  values: T | null;
  id: string | number | null;
};

export const useModalForm = <T extends any = any>(opener: () => void) => {
  const [editData, setEditData] = useState<EditData<T>>({
    values: null,
    id: null
  });

  const onAddClick = useCallback(() => {
    setEditData({
      values: null,
      id: null
    });
    opener();
  }, [opener]);

  const onEditClick = useCallback(
    (values: T, id: string | number) => {
      setEditData({
        values,
        id
      });
      opener();
    },
    [opener]
  );

  return {
    editData,
    onAddClick,
    onEditClick
  };
};

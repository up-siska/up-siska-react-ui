import { useMemo, useEffect, useCallback } from 'react';
import { Row } from 'react-table';
import { ChangeCheckboxes } from '../components/DataTable';
import { useDataTableCheckbox } from './use-data-table-checkbox';

export type MultiSelectTableMatcher<T, D extends object> = {
  selected: keyof T | ((el: T) => any);
  data: keyof D | ((el: D) => any);
  row: keyof Row<D> | ((el: Row<D>) => any);
};

export type UseMultiSelectTableOptions<T, D extends object> = {
  // default selected (usually send to backend)
  selected: T[];
  // data from api
  data: D[];
  // matcher for matching data when loop
  matcher: MultiSelectTableMatcher<T, D>;
  // convert data from backend to selected from type D to type T
  converter: (defaultData: D, index: number, self: D[]) => T;
  // auto merge with previous selected data
  mergeWithPrev?: boolean;
};

export const useMultiSelectTable = <
  T extends object = any,
  D extends object = any
>({
  selected,
  data,
  matcher,
  converter,
  mergeWithPrev = true
}: UseMultiSelectTableOptions<T, D>) => {
  const initialSelectedIndex = useMemo(() => {
    const _index: ChangeCheckboxes['index'] = {};

    selected.forEach((sel) => {
      data.forEach((d, i) => {
        const selectedMatcher =
          matcher.selected instanceof Function
            ? matcher.selected(sel)
            : sel[matcher.selected];
        const dataMatcher =
          matcher.data instanceof Function ? matcher.data(d) : d[matcher.data];

        if (dataMatcher === selectedMatcher) {
          _index[i] = true;
        }
      });
    });

    return _index;

    // eslint-disable-next-line
  }, [data, selected]);

  const {
    selectedIndex,
    selectedFlatRows,
    onChangeCheckboxes,
    stateActions: { setSelectedIndex, setSelectedFlatRows }
  } = useDataTableCheckbox<D>();

  const getSelectedData = useCallback(() => {
    const _data: typeof data = [];

    data.forEach((d, index) =>
      Object.entries(selectedIndex).forEach(([key, value]) => {
        if (index.toString() === key && value === true) {
          _data.push(d);
        }
      })
    );

    const convertedData: typeof selected = _data.map(converter);

    let restData: T[] = [];

    if (mergeWithPrev) {
      restData = selected?.filter((sel) => {
        const selectedMatcher =
          matcher.selected instanceof Function
            ? matcher.selected(sel)
            : sel[matcher.selected];

        const isNotExsist = selectedFlatRows?.some((row) => {
          const rowMatcher =
            matcher.row instanceof Function
              ? matcher.row(row)
              : row[matcher.row];
          return selectedMatcher !== rowMatcher;
        });
        return isNotExsist;
      });
    }

    const removedDuplicate = [...restData, ...convertedData].filter(
      (value, index, self) =>
        index ===
        self.findIndex((t) => {
          const selfMatcher =
            matcher.selected instanceof Function
              ? matcher.selected(t)
              : t[matcher.selected];
          const selectedMatcher =
            matcher.selected instanceof Function
              ? matcher.selected(value)
              : value[matcher.selected];

          return selfMatcher === selectedMatcher;
        })
    );

    return removedDuplicate;
  }, [
    data,
    converter,
    mergeWithPrev,
    selectedIndex,
    selected,
    matcher,
    selectedFlatRows
  ]);

  useEffect(() => {
    setSelectedIndex(initialSelectedIndex);
    // eslint-disable-next-line
  }, [initialSelectedIndex]);

  return {
    getSelectedData,
    tableCheckbox: {
      selectedIndex,
      selectedFlatRows,
      onChangeCheckboxes,
      stateActions: { setSelectedIndex, setSelectedFlatRows }
    }
  };
};

export default useMultiSelectTable;

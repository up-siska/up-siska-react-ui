import { useContext, useMemo } from 'react';
import { SiskaThemeContext } from '../contexts';

export const useSiskaTheme = () => {
  const { roleType } = useContext(SiskaThemeContext);

  const isStudent = useMemo(() => {
    return roleType === 'MAHASISWA';
  }, [roleType]);

  return {
    roleType,
    isStudent
  };
};

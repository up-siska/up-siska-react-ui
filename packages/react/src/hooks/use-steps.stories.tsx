import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Stepper, Group, Button } from '@mantine/core';
import { useSteps } from '../hooks';

export default {
  title: 'Example/Stepper',
  component: Stepper
} as ComponentMeta<typeof Stepper>;

const Template: ComponentStory<typeof Stepper> = (props) => {
  const { activeStep, gotoStep, prevStep, nextStep, reset } = useSteps({
    stepCount: 3,
    defaultStep: 1
  });

  return (
    <>
      <Stepper active={activeStep} onStepClick={gotoStep} breakpoint="sm">
        <Stepper.Step label="First step" description="Create an account">
          Step 1 content: Create an account
        </Stepper.Step>
        <Stepper.Step label="Second step" description="Verify email">
          Step 2 content: Verify email
        </Stepper.Step>
        <Stepper.Step label="Final step" description="Get full access">
          Step 3 content: Get full access
        </Stepper.Step>
        <Stepper.Completed>
          Completed, click back button to get to previous step
        </Stepper.Completed>
      </Stepper>

      <Group position="center" mt="xl">
        <Button variant="default" onClick={prevStep}>
          Back
        </Button>
        <Button onClick={nextStep}>Next step</Button>
        <Button onClick={reset}>Reset</Button>
      </Group>
    </>
  );
};

const Content = Template.bind({});

export { Content };

import { useState, useCallback } from 'react';

export type UseStepOptions = {
  defaultStep?: number;
  stepCount: number;
};

export const useSteps = ({ defaultStep, stepCount }: UseStepOptions) => {
  const [activeStep, setActiveStep] = useState(defaultStep || 0);

  const nextStep = useCallback(() => {
    setActiveStep((current) =>
      current < stepCount - 1 ? current + 1 : current
    );
  }, [stepCount]);

  const prevStep = useCallback(() => {
    setActiveStep((current) => (current > 0 ? current - 1 : current));
  }, []);

  const reset = useCallback(() => {
    setActiveStep(defaultStep || 0);
  }, [defaultStep]);

  const gotoStep = useCallback((stepIndex: number) => {
    setActiveStep(stepIndex);
  }, []);

  return {
    activeStep,
    nextStep,
    prevStep,
    gotoStep,
    reset
  };
};

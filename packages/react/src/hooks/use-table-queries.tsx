import { useState, useCallback, useMemo, useEffect } from 'react';
import { useDidUpdate } from '@mantine/hooks';
import { TableQueries } from '../components/DataTable/ServerSide';
import { generateTableQueryParams } from '../utils';

type UseTableQueriesOptions = {
  resetPage?: any[];
};

export const useTableQueries = <T extends string = string>(
  initialQueries: TableQueries<T>,
  options?: UseTableQueriesOptions
) => {
  const [tableQueries, setTableQueries] = useState(initialQueries);

  const handleRefetch = useCallback((queryData: TableQueries<T>) => {
    setTableQueries(queryData);
  }, []);

  const queryParams = useMemo(
    () => generateTableQueryParams(tableQueries),
    [tableQueries]
  );

  const resetPageDeps = useMemo(
    () => options?.resetPage || [],
    [options?.resetPage]
  );

  useDidUpdate(() => {
    setTableQueries((prev) => ({
      ...prev,
      page: 1
    }));
  }, resetPageDeps);

  return {
    tableQueries,
    handleRefetch,
    queryParams,
    stateActions: {
      setTableQueries
    }
  };
};

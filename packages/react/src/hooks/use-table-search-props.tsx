import { useState, useCallback } from 'react';
import { TableSearchProps } from '../components/DataTable';

export const useTableSearchProps = () => {
  const [tableSearchProps, setTableSearchProps] = useState<TableSearchProps>({
    globalFilter: '',
    setGlobalFilter: () => {}
  });

  const getTableSearchProps = useCallback((props: TableSearchProps) => {
    setTableSearchProps(props);
  }, []);

  return { tableSearchProps, getTableSearchProps };
};

export default useTableSearchProps;

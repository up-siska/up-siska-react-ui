import { useEffect, useState } from 'react';

export const useWindowDimensions = () => {
  const [dimensions, setDimensions] = useState({
    height: window.innerHeight,
    width: window.innerWidth
  });

  useEffect(() => {
    window.addEventListener('resize', () => {
      setDimensions({
        height: window.innerHeight,
        width: window.innerWidth
      });
    });

    return () => {
      window.removeEventListener('resize', () => {
        setDimensions({
          height: window.innerHeight,
          width: window.innerWidth
        });
      });
    };
  }, []);

  return dimensions;
};

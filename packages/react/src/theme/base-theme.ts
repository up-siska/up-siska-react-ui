import { MantineThemeOverride } from '@mantine/core';

export const baseTheme: MantineThemeOverride = {
  fontFamily: "'Inter', sans-serif",
  headings: {
    fontFamily: "'Inter', sans-serif"
  },
  primaryColor: 'blue'
};

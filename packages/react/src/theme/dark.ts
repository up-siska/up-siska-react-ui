import { MantineThemeOverride } from '@mantine/core';

export const darkTheme: MantineThemeOverride = {
  colorScheme: 'dark',
  colors: {
    blue: [
      '#CEE9FF',
      '#ABDAFF',
      '#8CCBFF',
      '#72BEFD',
      '#5AB1F9',
      '#45A5F5',
      '#339AF0',
      '#1990F3',
      '#0885EE',
      '#037BDF'
    ],
    'dark-blue': [
      '#A8C4FF',
      '#6191FF',
      '#2F6AF5',
      '#1751DB',
      '#1B47AC',
      '#1D3D88',
      '#122E71',
      '#0A235D',
      '#041A4E',
      '#001341'
    ],
    'dark-green': [
      '#A3FFB5',
      '#5FFF7D',
      '#49F065',
      '#3FCA56',
      '#469B54',
      '#289139',
      '#0B8C21',
      '#008D09',
      '#007F00',
      '#006800'
    ],
    paper: ['#141517', '#212529', '#29304a'],
    text: ['#F8F9FA', '#A6A7AB', '#0067D4'],
    body: ['#1A1B1E', '#363c42']
  }
};

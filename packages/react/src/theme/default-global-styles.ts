import { MantineTheme } from '@mantine/core';

export const defaultGlobalStyles = (theme: MantineTheme): any => ({
  '*, *::before, *::after': {
    boxSizing: 'border-box',
    fontFamily: "'Inter', sans-serif !important"
  },
  body: {
    backgroundColor: theme.colors.body[0],
    overflowX: 'hidden'
  }
});

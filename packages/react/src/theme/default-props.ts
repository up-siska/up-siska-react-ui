import { MantineProviderProps } from '@mantine/core';

export const defaultProps: MantineProviderProps['defaultProps'] = {
  Button: {
    compact: true
  },
  Select: {
    searchable: true
  },
  MultiSelect: {
    searchable: true
  },
  DatePicker: {
    locale: 'id'
  },
  LoadingOverlay: {
    zIndex: 99
  }
};

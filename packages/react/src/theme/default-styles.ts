import { MantineProviderProps } from '@mantine/core';

const compactButtonHeight: { [key: string]: number } = {
  xs: 22,
  sm: 26,
  md: 30,
  lg: 34,
  xl: 40
};

export const defaultStyles: MantineProviderProps['styles'] = {
  Badge: {
    root: {
      textTransform: 'capitalize'
    }
  },
  Text: (theme) => ({
    root: {
      color: theme.colors.text[0]
    }
  }),
  Paper: (theme) => ({
    root: {
      backgroundColor: theme.colors.paper[0]
    }
  }),
  Button: (theme, params) => ({
    root: {
      height: params.compact ? 'auto' : undefined,
      padding: params.compact ? `2px ${theme.spacing.md}px` : undefined
    },
    inner: {
      height: params.compact ? compactButtonHeight[params.size] : undefined
    }
  })
};

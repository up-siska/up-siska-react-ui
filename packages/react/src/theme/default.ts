import { MantineThemeOverride } from '@mantine/core';
import { baseTheme } from './base-theme';

export const defaultTheme: MantineThemeOverride = {
  ...baseTheme,
  shadows: {
    xs: '0px 0px 8px 0px rgba(0,103,212,0.25)',
    sm: '0px 0px 12px 0px rgba(0,103,212,0.25)',
    md: '0px 0px 18px 0px rgba(0,103,212,0.25)',
    lg: '0px 0px 26px 0px rgba(0,103,212,0.25)',
    xl: '0px 0px 36px 0px rgba(0,103,212,0.25)'
  }
};

import { MantineThemeOverride } from '@mantine/core';

export const lightTheme: MantineThemeOverride = {
  colorScheme: 'light',
  colors: {
    blue: [
      '#CEE9FF',
      '#ABDAFF',
      '#8CCBFF',
      '#72BEFD',
      '#5AB1F9',
      '#45A5F5',
      '#339AF0',
      '#1990F3',
      '#0885EE',
      '#037BDF'
    ],
    'dark-blue': [
      '#A8C4FF',
      '#6191FF',
      '#2F6AF5',
      '#1751DB',
      '#1B47AC',
      '#1D3D88',
      '#122E71',
      '#0A235D',
      '#041A4E',
      '#001341'
    ],
    'dark-green': [
      '#A3FFB5',
      '#5FFF7D',
      '#49F065',
      '#3FCA56',
      '#469B54',
      '#289139',
      '#0B8C21',
      '#008D09',
      '#007F00',
      '#006800'
    ],
    paper: ['#ffffff', '#F1F3F5', '#EFF2FF'],
    text: ['#101113', '#373A40', '#0067D4'],
    body: ['#F9FAFF', '#E5F3FD']
  }
};

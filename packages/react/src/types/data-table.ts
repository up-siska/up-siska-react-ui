import { Column } from 'react-table';
import { CSSObject, MantineTheme } from '@mantine/core';

export type TableColumn<D extends object = {}> = Column<D> & {
  cellStyle?: CSSObject | ((theme: MantineTheme) => CSSObject);
  sortName?: string;
};

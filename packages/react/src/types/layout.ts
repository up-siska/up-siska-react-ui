import React from 'react';
import { IconType } from '../components/Icon';

export interface Menu {
  label: string;
  path?: string;
  icon?: IconType;
  pathnames?: string[];
  permissions?: string[];
}

export interface SidebarMenu extends Omit<Menu, 'label'> {
  label?: string;
  colorScheme?: string;
  pathnames?: string[];
  withTooltip?: boolean;
  menus?: SidebarMenu[];
}

export interface TabMenu extends Menu {}

export interface ProfileInfoMenu {
  action: (e?: React.MouseEvent<HTMLButtonElement>) => void;
  icon?: React.ReactNode;
  label: string;
}

import React from 'react';
import { Row, RenderParams } from 'react-table';
import { TableQueries } from '../components/DataTable/ServerSide';

export const renderTableColumn = <T extends object = {}>(
  callback: (row: Row<T>) => React.ReactNode
) => {
  const getRow = (params: RenderParams<T>) => {
    return callback(params.row);
  };

  return getRow;
};

export const generateTableQueryParams = (tableQuery: TableQueries) => {
  const urlParams = new URLSearchParams('');

  const fields = tableQuery.fields?.join(',');
  const filters = tableQuery.filter
    ?.map((filterData) => {
      return `${filterData.field}:${filterData.value}`;
    })
    .join(',');
  const sortData = tableQuery.sortData
    ?.map((sort) => {
      return `${sort.field}:${sort.type}`;
    })
    .join(',');
  const searchData = tableQuery.search
    ?.map((search) => {
      return `${search.field}:${search.value}`;
    })
    .join(',');

  urlParams.append('page', tableQuery.page.toString());
  urlParams.append('perPage', tableQuery.perPage.toString());
  if (fields) {
    urlParams.append('fields', fields);
  }
  if (sortData) {
    urlParams.append('sort', sortData);
  }
  if (filters) {
    urlParams.append('filter', filters);
  }
  if (searchData) {
    urlParams.append('search', searchData);
  }

  return urlParams.toString();
};

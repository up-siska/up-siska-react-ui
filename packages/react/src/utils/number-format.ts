import dayjs from 'dayjs';

export const currencyFormat = (
  value = 0,
  currency = 'IDR',
  localeString = 'id-ID'
) => {
  const numberedValue = Number(value);

  return numberedValue
    .toLocaleString(localeString, {
      style: 'currency',
      currency
    })
    .slice(0, -3);
};

export const compactNumber = (value: string | number) => {
  const newValue = Number(value);
  if (Number(value) >= 1000) {
    const suffixes = ['', 'rb', 'jt', 'm', 't'];
    const suffixNum = Math.floor(('' + value).length / 3);
    let shortValue = 0;
    for (let precision = 2; precision >= 1; precision--) {
      shortValue = parseFloat(
        (suffixNum !== 0 ? newValue / Math.pow(1000, suffixNum) : newValue)
          .toPrecision(precision)
          .toString()
      );
      const dotLessShortValue = (shortValue + '').replace(
        /[^a-zA-Z 0-9]+/g,
        ''
      );
      if (dotLessShortValue.length <= 2) {
        break;
      }
    }
    if (shortValue % 1 !== 0) {
      return shortValue.toFixed(1);
    }
    return shortValue + suffixes[suffixNum];
  }
  return newValue;
};

export const thousandsFormat = (value: number | string) => {
  return value?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

export type Formatter<T extends object = any> = {
  label: keyof T | ((item: T) => string);
  value: keyof T | ((item: T) => string);
};

type Option = {
  label: string;
  value: string;
};

export const generateOptions = <T extends object = {}>(
  values: T[],
  format: Formatter<T>
) => {
  return values.map((item) => {
    return {
      label:
        format.label instanceof Function
          ? format.label(item)
          : item[format.label],
      value:
        format.value instanceof Function
          ? format.value(item)
          : item[format.value]
    } as Option;
  });
};

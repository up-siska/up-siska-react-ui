import React from 'react';
import dayjs from 'dayjs';
import { Button } from '@mantine/core';
import { useDisclosure } from '@mantine/hooks';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { ImportExcel } from './index';
import { PreviewImport } from './PreviewImport';
import { useReadExcel } from '../../hooks';

export interface IImportHasilItem {
  email_patient?: string;
  nama_patient?: string;
  no_identitas?: string;
  jenis_identitas?: string;
  alamat_patient?: string;
  notelp_patient?: string;
  jenis_kelamin?: string;
  tanggal_lahir?: number;
  nationality?: string;
  cara_pelaksanaan_tes?: 'walk-in' | 'drive-thru';
  tanggal_ambil_sampel?: number;
  tanggal_keluar_hasil?: number;
  hasil?: 'positif' | 'negatif';
  ct_value?: string;
  no_specimen?: string;
  no_lab?: string;
}

const previewImportTableCols = [
  {
    Header: 'Nama Pasien',
    accessor: 'nama_patient',
    id: 'nama_patient'
  },
  {
    Header: 'Jenis ID',
    accessor: 'jenis_identitas',
    id: 'jenis_identitas'
  },
  {
    Header: 'Nomor Identitas',
    accessor: 'no_identitas',
    id: 'no_identitas'
  },
  {
    Header: 'Alamat',
    accessor: 'alamat_patient',
    id: 'alamat_patient'
  },
  {
    Header: 'Nomor Telepon',
    accessor: 'notelp_patient',
    id: 'notelp_patient'
  },
  {
    Header: 'Email',
    accessor: 'email_patient',
    id: 'email_patient'
  },
  {
    Header: 'Jenis Kelamin',
    accessor: 'jenis_kelamin',
    id: 'jenis_kelamin'
  },
  {
    Header: 'Tanggal Lahir',
    accessor: (row: any) =>
      row?.patient?.tanggal_lahir
        ? dayjs(row?.patient?.tanggal_lahir).format('DD/MM/YYYY')
        : '',
    id: 'tanggal_lahir'
  },
  {
    Header: 'Kewarganegaraan',
    accessor: 'nationality',
    id: 'nationality'
  },
  {
    Header: 'Tanggal Test',
    accessor: (row: any) =>
      row?.tanggal_ambil_sampel
        ? dayjs(row?.tanggal_ambil_sampel).format('DD/MM/YYYY H.mm')
        : '',
    id: 'tanggal_ambil_sampel'
  },
  {
    Header: 'Tanggal Keluar Hasil',
    accessor: (row: any) =>
      row?.tanggal_keluar_hasil
        ? dayjs(row?.tanggal_keluar_hasil).format('DD/MM/YYYY H.mm')
        : '',
    id: 'tanggal_keluar_hasil'
  },
  {
    Header: 'Hasil Test',
    accessor: 'hasil',
    id: 'hasil'
  }
];

export default {
  title: 'Example/ImportExcel',
  component: ImportExcel
} as ComponentMeta<typeof ImportExcel>;

const Template: ComponentStory<typeof ImportExcel> = (args) => {
  const [isOpen, handlers] = useDisclosure(false);
  const [isPreviewOpen, previewHandlers] = useDisclosure(false);
  const { onReadFile, convertedRows, renderImportErrors, isError, fileName } =
    useReadExcel<IImportHasilItem>({
      previewOpener: previewHandlers.open
    });

  const handleSubmit = () => {
    handlers.close();
    previewHandlers.close();
  };

  return (
    <>
      <Button onClick={handlers.open}>Import</Button>
      <ImportExcel
        isOpen={isOpen}
        onReadFile={onReadFile}
        onClose={handlers.close}
        templateFilePath="./import-example.xlsx"
        customChildren={(defaultChild) => (
          <>
            hello
            <br />
            {defaultChild}
          </>
        )}
      />
      <PreviewImport
        fileName={fileName}
        isOpen={isPreviewOpen}
        onClose={previewHandlers.close}
        onImportSubmit={handleSubmit}
        renderErrors={renderImportErrors}
        dataTableProps={{
          tableData: convertedRows,
          tableColumns: previewImportTableCols
        }}
        isCanSave={!isError}
        confirmButtonText="Simpan"
        customChildren={(child) => (
          <>
            {child}
            <Button mt="md">Custom Child</Button>
          </>
        )}
      />
    </>
  );
};

const Import = Template.bind({}) as typeof Template;

export { Import };

import React from 'react';
import { Modal, Group, Button, Box, Text } from '@mantine/core';
import { DocumentIcon } from '@heroicons/react/outline';
import { DataTable, DataTableProps, Icon } from '@siska-up-ui/react';

export type PreviewImportTitleProps = {
  title: string;
  subtitle?: string;
};

export const PreviewImportTitle: React.FC<PreviewImportTitleProps> = ({
  title,
  subtitle
}) => {
  return (
    <Box>
      <Text size="xs" weight="bold">
        {title}
      </Text>
      <Group spacing={2} align="center">
        <Icon icon={DocumentIcon} size={12} />
        <Text size="sm">{subtitle}</Text>
      </Group>
    </Box>
  );
};

export type PreviewImportProps = {
  isOpen: boolean;
  onClose: () => void;
  isCanSave?: boolean;
  onImportSubmit: () => void;
  dataTableProps: DataTableProps<any>;
  renderErrors: () => JSX.Element;
  fileName: string;
  confirmButtonText?: string;
  confirmButtonLoading?: boolean;
  customChildren?: (defaultChild: React.ReactNode) => React.ReactNode;
};

export const PreviewImport: React.FC<PreviewImportProps> = ({
  isOpen,
  onClose,
  dataTableProps,
  isCanSave,
  onImportSubmit,
  renderErrors,
  fileName,
  confirmButtonLoading,
  confirmButtonText = 'Simpan',
  customChildren
}) => {
  const defaultChild = (
    <>
      <Box mb="lg">{renderErrors()}</Box>
      <DataTable {...dataTableProps} />
    </>
  );

  return (
    <Modal
      opened={isOpen}
      centered
      onClose={onClose}
      size="90vw"
      styles={{
        title: {
          width: '100%'
        }
      }}
      title={
        <Group position="apart">
          <PreviewImportTitle title="Preview File" subtitle={fileName} />
          <Group spacing="xs" position="right">
            <Button color="red" onClick={onClose}>
              Batal
            </Button>
            <Button
              disabled={Boolean(!isCanSave)}
              loading={confirmButtonLoading}
              onClick={onImportSubmit}
            >
              {confirmButtonText}
            </Button>
          </Group>
        </Group>
      }
    >
      {customChildren ? (
        <>{customChildren(defaultChild)}</>
      ) : (
        <>{defaultChild}</>
      )}
    </Modal>
  );
};

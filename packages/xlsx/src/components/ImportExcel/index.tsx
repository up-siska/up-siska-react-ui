import React, { useState, useEffect, useCallback } from 'react';
import {
  Modal,
  Group,
  Button,
  TextInput,
  Box,
  Select,
  Tooltip,
  Text,
  ActionIcon
} from '@mantine/core';
import XLSX, { WorkSheet } from 'xlsx';
import { useId } from '@mantine/hooks';
import { DownloadIcon } from '@heroicons/react/solid';
import { ModalTitle, Loading, Icon } from '@siska-up-ui/react';

export type ImportExcelProps = {
  isOpen: boolean;
  onClose: () => void;
  onReadFile: (data: any[], fileName?: string) => void;
  label?: string;
  isLoading?: boolean;
  templateFilePath?: string;
  templateFileName?: string;
  customChildren?: (defaultChild: React.ReactNode) => React.ReactNode;
};

export const ImportExcel: React.FC<ImportExcelProps> = ({
  isOpen,
  onClose,
  onReadFile,
  label,
  isLoading,
  templateFilePath,
  templateFileName,
  customChildren
}) => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [selectedSheet, setSelectedSheet] = useState<WorkSheet | null>(null);
  const [selectedSheetName, setSelectedSheetName] = useState('');
  const [sheetNames, setSheetNames] = useState<string[]>([]);
  const [workSheets, setWorksheets] =
    useState<{
      [sheet: string]: WorkSheet;
    } | null>(null);
  const inputId = useId();
  const [isLoadingFile, setIsLoadingFile] = useState(false);

  const handleSave = useCallback(() => {
    if (selectedSheet) {
      const rows =
        XLSX.utils.sheet_to_json<Parameters<typeof onReadFile>[0]>(
          selectedSheet
        );
      onReadFile(rows, selectedFile?.name);
    }
  }, [onReadFile, selectedSheet, selectedFile]);

  const handleChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    const reader = new FileReader();
    const file = e.target?.files?.[0];
    setWorksheets(null);
    setSelectedSheet(null);
    setSheetNames([]);
    setIsLoadingFile(true);

    reader.onload = (e) => {
      const fileData = e.target?.result;
      const workBooks = XLSX.read(fileData, { type: 'array' });
      setSheetNames(workBooks.SheetNames);
      setWorksheets(workBooks.Sheets);
      setSelectedSheet(workBooks.Sheets[workBooks.SheetNames[0]]);
      setSelectedSheetName(workBooks.SheetNames[0]);
      setSelectedFile(file || null);
      setIsLoadingFile(false);
    };
    reader.readAsArrayBuffer(file || new Blob());
  }, []);

  const handleChangeSelect = useCallback(
    (value: string | null) => {
      if (value && workSheets) {
        setSelectedSheet(workSheets[value]);
        setSelectedSheetName(value);
      }
    },
    [workSheets]
  );

  const handleReset = useCallback(
    (event: React.MouseEvent<HTMLInputElement, MouseEvent>) => {
      const element = event.target as HTMLInputElement;
      element.value = '';
    },
    []
  );

  useEffect(() => {
    if (isOpen) {
      setSelectedFile(null);
      setSelectedSheet(null);
      setSelectedSheetName('');
      setSheetNames([]);
      setWorksheets(null);
    }
  }, [isOpen]);

  const defaultChild = (
    <>
      <Group position="right">
        {templateFilePath ? (
          <Tooltip label="Download File Template">
            <ActionIcon
              component="a"
              href={templateFilePath}
              download={templateFileName}
              mb="md"
            >
              <Icon icon={DownloadIcon} size={16} />
            </ActionIcon>
          </Tooltip>
        ) : null}
      </Group>
      <Group spacing={6} align="end" mb="md">
        <Button
          component="label"
          htmlFor={inputId}
          compact={false}
          color="gray"
        >
          Pilih File
        </Button>
        <Box sx={{ flex: 1 }}>
          <TextInput
            placeholder="Belum ada file terpilih"
            value={selectedFile?.name}
            readOnly
          />
        </Box>

        <input
          type="file"
          accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
          style={{ display: 'none' }}
          id={inputId}
          onChange={handleChange}
          onClick={handleReset}
        />
      </Group>
      {isLoadingFile ? (
        <Loading />
      ) : (
        <>
          {workSheets && (
            <Select
              label="Pilih Sheet"
              placeholder="Pilih Sheet"
              data={sheetNames}
              onChange={handleChangeSelect}
              mb="md"
              searchable={false}
              value={selectedSheetName}
            />
          )}
        </>
      )}
      <Group position="right" spacing="xs">
        <Button color="red" onClick={onClose}>
          Batal
        </Button>
        <Button
          disabled={Boolean(!selectedSheet) || isLoadingFile}
          onClick={handleSave}
          loading={isLoading}
        >
          Import
        </Button>
      </Group>
    </>
  );

  return (
    <Modal
      opened={isOpen}
      centered
      onClose={onClose}
      title={<ModalTitle>Import {label || 'File'}</ModalTitle>}
    >
      {customChildren ? customChildren(defaultChild) : defaultChild}
    </Modal>
  );
};

export default ImportExcel;

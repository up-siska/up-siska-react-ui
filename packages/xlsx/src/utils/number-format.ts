import dayjs from 'dayjs';

export const excelDateFormat = (value: number, format: string) => {
  return dayjs(new Date(Math.round((value - 25569) * 86400 * 1000))).format(
    format
  );
};

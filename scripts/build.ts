/* eslint-disable no-console */
import chalk from 'chalk';
import { exec } from 'child_process';
import loading from 'loading-cli';
import { PackageName } from './types';
import { getPackageArgs, validatePackage } from './utils';

function build(name: PackageName) {
  const loadPrettier = loading(chalk.cyan('Formatting prettier')).start();
  exec(
    `cd packages/${name} && rimraf dist && prettier --write src`,
    (error, stdout, stderr) => {
      console.log('');
      console.log(stdout);
      console.log(stderr);
      if (error) {
        console.log(chalk.red(error));
        loadPrettier.stop();
        return;
      }
      console.log(chalk.green('Format prettier complete!'));
      loadPrettier.stop();
      console.log('');

      const loadBuild = loading(
        chalk.cyan(`Building @siska-up-ui/${name}`)
      ).start();

      exec(
        `cd packages/${name} && tsc && tsc --module CommonJS --outDir dist/cjs`,
        (error, stdout, stderr) => {
          if (error) {
            console.log(chalk.bgRed(stdout));
            console.log(chalk.bgRed(stderr));
            console.log(chalk.red(error));
            loadBuild.stop();
            return;
          }
          console.log(stderr);
          console.log(stdout);
          console.log(chalk.green('Build complete!'));
          loadBuild.stop();
        }
      );
    }
  );
}

function run() {
  const argv = getPackageArgs().parse();
  let isSelectBuild = false;
  validatePackage(
    argv,
    (res) => build(res.package),
    () => {
      isSelectBuild = true;
    }
  );

  if (isSelectBuild) {
    return;
  }

  build(argv.p as PackageName);
}

run();

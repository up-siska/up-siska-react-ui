import { PackageName, ReleaseType } from '../types';

export const packageNames: PackageName[] = ['react', 'pdf', 'xlsx', 'fetcher'];
export const releaseTypes: ReleaseType[] = ['major', 'minor', 'patch'];

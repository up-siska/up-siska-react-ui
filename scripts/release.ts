/* eslint-disable no-console */
import chalk from 'chalk';
import { exec } from 'child_process';
import yargs from 'yargs';
import loading from 'loading-cli';
import { releaseTypes } from './constants';
import { PackageName, ReleaseType } from './types';
import { getPackageArgs, validatePackage } from './utils';

function release(type: ReleaseType, name: PackageName) {
  const loadVer = loading(chalk.cyan('Changing version')).start();
  exec(
    `cd packages/${name} && npm version ${type}`,
    (error, stdout, stderr) => {
      console.log('');
      console.log(stdout);
      console.log(stderr);
      if (error) {
        console.log(chalk.red(error));
        loadVer.stop();
        return;
      }
      console.log(chalk.green('Change version complete!'));
      loadVer.stop();
      console.log('');

      const loadPublish = loading(`Publishing @siska-up-ui/${name}`);
      exec(
        `cd packages/${name} && npm publish --access=public`,
        (error, stdout, stderr) => {
          console.log('');
          console.log(stdout);
          console.log(stderr);
          if (error) {
            console.log(chalk.red(error));
            loadPublish.stop();
            return;
          }
          console.log(chalk.green('Publish complete!'));
          loadPublish.stop();
          exec('rimraf package-lock.json');
        }
      );
    }
  );
}

function run() {
  const argv = yargs
    .command('patch', 'Patch release', getPackageArgs)
    .command('minor', 'Minor release', getPackageArgs)
    .command('major', 'Major release', getPackageArgs).argv;

  if (!argv._[0] || !releaseTypes.includes(argv._[0] as ReleaseType)) {
    if (argv._[0]) {
      console.log(chalk.red(`Command ${argv._[0]} not found!`));
      console.log('');
    }
    yargs.showHelp();
    process.exit(1);
  }

  let isSelectBuild = false;
  validatePackage(
    argv,
    (result) => release(argv._[0] as ReleaseType, result.package),
    () => (isSelectBuild = true),
    !argv.p,
    'release'
  );

  if (isSelectBuild) {
    return;
  }

  release(argv._[0] as ReleaseType, argv.package as PackageName);
}

run();

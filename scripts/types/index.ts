export type ReleaseType = 'major' | 'minor' | 'patch';
export type PackageName = 'react' | 'pdf' | 'xlsx' | 'fetcher';

/* eslint-disable no-console */
import yargs from 'yargs';
import inquirer from 'inquirer';
import chalk from 'chalk';
import { packageNames } from '../constants';
import { PackageName } from '../types';

export const selectPackage = <T = { package: PackageName }>(
  handler: (result: T) => void,
  selectType?: 'build' | 'release'
) => {
  inquirer
    .prompt([
      {
        type: 'list',
        message: chalk.cyan(
          `Which package do you want to ${selectType || 'build'}?`
        ),
        name: 'package',
        choices: packageNames
      }
    ])
    .then((result: T) => {
      handler(result);
    });
};

export const getPackageArgs = (initialYargs?: typeof yargs) => {
  let argv = yargs.option('p', {
    alias: 'package',
    describe: 'Package name to build',
    type: 'string'
  });

  if (initialYargs) {
    argv = initialYargs.option('p', {
      alias: 'package',
      describe: 'Package name to build',
      type: 'string'
    });
  }

  return argv;
};

const parsedYargs = getPackageArgs().parse();

export const validatePackage = <T extends typeof parsedYargs>(
  argv: T,
  callback: (result: { package: PackageName }) => void,
  breaker: () => void,
  selectValidator?: boolean,
  selectType?: 'build' | 'release'
) => {
  const _selectValidator = selectValidator || (!argv._[0] && !argv.p);

  if (_selectValidator) {
    selectPackage((result) => {
      callback(result);
    }, selectType);
    breaker();
    return;
  }

  if (!argv.p) {
    yargs.showHelp();
    process.exit(1);
  }

  if (argv.p && !packageNames.includes(argv.p as PackageName)) {
    console.log(
      chalk.red(
        'Package name not valid!. Available name: "react", "pdf", "fetcher", "xlsx".'
      )
    );
    process.exit(1);
  }
};
